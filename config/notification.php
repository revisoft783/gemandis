<?php

return [

    /* Available Drivers: "none", "slack", "telegram" */
    'driver' => env('NOTIFICATION_DRIVER', 'slack'),

    'slack' => [
        'webhook_url' => env('NOTIFICATION_SLACK_WEBHOOK_URL',"https://hooks.slack.com/services/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"),
        'username' => 'Laravel Log',
        'emoji' => ':boom:',
    ],

    'telegram' => [
        /* Token for bot */
        'bot_token' => env('NOTIFICATION_TELEGRAM_BOT_TOKEN',"5250606083:AAEIIytvHj_FIK9VHUo34gnwBIgkFmYmGA4"),

        /* group / chanel destination  */
        'group_chat_id' => env('NOTIFICATION_TELEGRAM_GROUP_CHAT_ID',"-707272180"),

        /* Available Drivers: "html", "MarkdownV2", "markdown"  */
        'parse_mode' => env('NOTIFICATION_TELEGRAM_PARSE_MODE',"html"),
    ],

];
