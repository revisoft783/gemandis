<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PeranMenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('peran_menu')->delete();
        
        \DB::table('peran_menu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'peran_id' => 1,
                'menu_id' => 1,
                'created_at' => '2021-05-07 13:35:55',
                'updated_at' => '2021-05-07 13:35:55',
            ),
            1 => 
            array (
                'id' => 2,
                'peran_id' => 1,
                'menu_id' => 2,
                'created_at' => '2021-05-07 13:35:55',
                'updated_at' => '2021-05-07 13:35:55',
            ),
            2 => 
            array (
                'id' => 3,
                'peran_id' => 1,
                'menu_id' => 3,
                'created_at' => '2021-05-07 13:35:55',
                'updated_at' => '2021-05-07 13:35:55',
            ),
            3 => 
            array (
                'id' => 4,
                'peran_id' => 1,
                'menu_id' => 4,
                'created_at' => '2021-05-07 13:35:55',
                'updated_at' => '2021-05-07 13:35:55',
            ),
            4 => 
            array (
                'id' => 5,
                'peran_id' => 1,
                'menu_id' => 5,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
        ));
        
        
    }
}