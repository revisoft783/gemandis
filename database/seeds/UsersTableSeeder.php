<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'nama' => 'Admin',
                'email' => 'admin@gmail.com',
                'foto_profil' => 'public/foto_profil/xDz1OIvEwjb1PBW2SHw134y5FcwoVcWicecKY5dX.png',
                'email_verified_at' => '2021-05-07 13:37:28',
                'password' => '$2y$10$AqFVxYtTpM/HjEiT60jZJOwrEe11Mv71PyloYnP6SCM0KlIsXyn8u', //asdqwe123
                'peran_id' => 1,
                'remember_token' => NULL,
                'otp' => NULL,
                'actived' => 1,
                'created_at' => '2022-02-02 11:43:16',
                'updated_at' => '2022-02-02 11:43:19',
            ),
        ));


    }
}
