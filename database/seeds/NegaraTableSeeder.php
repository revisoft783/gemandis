<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class NegaraTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('negara')->delete();
        
        \DB::table('negara')->insert(array (
            0 => 
            array (
                'id_country' => 1,
                'negara_name' => 'Aruba',
                'code1' => 'ABW',
                'code2' => 'AW',
                'flag' => 'AW.png',
            ),
            1 => 
            array (
                'id_country' => 2,
                'negara_name' => 'Afghanistan',
                'code1' => 'AFG',
                'code2' => 'AF',
                'flag' => 'AF.png',
            ),
            2 => 
            array (
                'id_country' => 3,
                'negara_name' => 'Angola',
                'code1' => 'AGO',
                'code2' => 'AO',
                'flag' => 'AO.png',
            ),
            3 => 
            array (
                'id_country' => 4,
                'negara_name' => 'Anguilla',
                'code1' => 'AIA',
                'code2' => 'AI',
                'flag' => 'AI.png',
            ),
            4 => 
            array (
                'id_country' => 5,
                'negara_name' => 'Åland',
                'code1' => 'ALA',
                'code2' => 'AX',
                'flag' => 'AX.png',
            ),
            5 => 
            array (
                'id_country' => 6,
                'negara_name' => 'Albania',
                'code1' => 'ALB',
                'code2' => 'AL',
                'flag' => 'AL.png',
            ),
            6 => 
            array (
                'id_country' => 7,
                'negara_name' => 'Andorra',
                'code1' => 'AND',
                'code2' => 'AD',
                'flag' => 'AD.png',
            ),
            7 => 
            array (
                'id_country' => 8,
                'negara_name' => 'United Arab Emirates',
                'code1' => 'ARE',
                'code2' => 'AE',
                'flag' => 'AE.png',
            ),
            8 => 
            array (
                'id_country' => 9,
                'negara_name' => 'Argentina',
                'code1' => 'ARG',
                'code2' => 'AR',
                'flag' => 'AR.png',
            ),
            9 => 
            array (
                'id_country' => 10,
                'negara_name' => 'Armenia',
                'code1' => 'ARM',
                'code2' => 'AM',
                'flag' => 'AM.png',
            ),
            10 => 
            array (
                'id_country' => 11,
                'negara_name' => 'American Samoa',
                'code1' => 'ASM',
                'code2' => 'AS',
                'flag' => 'AS.png',
            ),
            11 => 
            array (
                'id_country' => 12,
                'negara_name' => 'Antarctica',
                'code1' => 'ATA',
                'code2' => 'AQ',
                'flag' => 'AQ.png',
            ),
            12 => 
            array (
                'id_country' => 13,
                'negara_name' => 'French Southern Territories',
                'code1' => 'ATF',
                'code2' => 'TF',
                'flag' => 'TF.png',
            ),
            13 => 
            array (
                'id_country' => 14,
                'negara_name' => 'Antigua and Barbuda',
                'code1' => 'ATG',
                'code2' => 'AG',
                'flag' => 'AG.png',
            ),
            14 => 
            array (
                'id_country' => 15,
                'negara_name' => 'Australia',
                'code1' => 'AUS',
                'code2' => 'AU',
                'flag' => 'AU.png',
            ),
            15 => 
            array (
                'id_country' => 16,
                'negara_name' => 'Austria',
                'code1' => 'AUT',
                'code2' => 'AT',
                'flag' => 'AT.png',
            ),
            16 => 
            array (
                'id_country' => 17,
                'negara_name' => 'Azerbaijan',
                'code1' => 'AZE',
                'code2' => 'AZ',
                'flag' => 'AZ.png',
            ),
            17 => 
            array (
                'id_country' => 18,
                'negara_name' => 'Burundi',
                'code1' => 'BDI',
                'code2' => 'BI',
                'flag' => 'BI.png',
            ),
            18 => 
            array (
                'id_country' => 19,
                'negara_name' => 'Belgium',
                'code1' => 'BEL',
                'code2' => 'BE',
                'flag' => 'BE.png',
            ),
            19 => 
            array (
                'id_country' => 20,
                'negara_name' => 'Benin',
                'code1' => 'BEN',
                'code2' => 'BJ',
                'flag' => 'BJ.png',
            ),
            20 => 
            array (
                'id_country' => 21,
                'negara_name' => 'Bonaire',
                'code1' => 'BES',
                'code2' => 'BQ',
                'flag' => 'BQ.png',
            ),
            21 => 
            array (
                'id_country' => 22,
                'negara_name' => 'Burkina Faso',
                'code1' => 'BFA',
                'code2' => 'BF',
                'flag' => 'BF.png',
            ),
            22 => 
            array (
                'id_country' => 23,
                'negara_name' => 'Bangladesh',
                'code1' => 'BGD',
                'code2' => 'BD',
                'flag' => 'BD.png',
            ),
            23 => 
            array (
                'id_country' => 24,
                'negara_name' => 'Bulgaria',
                'code1' => 'BGR',
                'code2' => 'BG',
                'flag' => 'BG.png',
            ),
            24 => 
            array (
                'id_country' => 25,
                'negara_name' => 'Bahrain',
                'code1' => 'BHR',
                'code2' => 'BH',
                'flag' => 'BH.png',
            ),
            25 => 
            array (
                'id_country' => 26,
                'negara_name' => 'Bahamas',
                'code1' => 'BHS',
                'code2' => 'BS',
                'flag' => 'BS.png',
            ),
            26 => 
            array (
                'id_country' => 27,
                'negara_name' => 'Bosnia and Herzegovina',
                'code1' => 'BIH',
                'code2' => 'BA',
                'flag' => 'BA.png',
            ),
            27 => 
            array (
                'id_country' => 28,
                'negara_name' => 'Saint Barthélemy',
                'code1' => 'BLM',
                'code2' => 'BL',
                'flag' => 'BL.png',
            ),
            28 => 
            array (
                'id_country' => 29,
                'negara_name' => 'Belarus',
                'code1' => 'BLR',
                'code2' => 'BY',
                'flag' => 'BY.png',
            ),
            29 => 
            array (
                'id_country' => 30,
                'negara_name' => 'Belize',
                'code1' => 'BLZ',
                'code2' => 'BZ',
                'flag' => 'BZ.png',
            ),
            30 => 
            array (
                'id_country' => 31,
                'negara_name' => 'Bermuda',
                'code1' => 'BMU',
                'code2' => 'BM',
                'flag' => 'BM.png',
            ),
            31 => 
            array (
                'id_country' => 32,
                'negara_name' => 'Bolivia',
                'code1' => 'BOL',
                'code2' => 'BO',
                'flag' => 'BO.png',
            ),
            32 => 
            array (
                'id_country' => 33,
                'negara_name' => 'Brazil',
                'code1' => 'BRA',
                'code2' => 'BR',
                'flag' => 'BR.png',
            ),
            33 => 
            array (
                'id_country' => 34,
                'negara_name' => 'Barbados',
                'code1' => 'BRB',
                'code2' => 'BB',
                'flag' => 'BB.png',
            ),
            34 => 
            array (
                'id_country' => 35,
                'negara_name' => 'Brunei',
                'code1' => 'BRN',
                'code2' => 'BN',
                'flag' => 'BN.png',
            ),
            35 => 
            array (
                'id_country' => 36,
                'negara_name' => 'Bhutan',
                'code1' => 'BTN',
                'code2' => 'BT',
                'flag' => 'BT.png',
            ),
            36 => 
            array (
                'id_country' => 37,
                'negara_name' => 'Bouvet Island',
                'code1' => 'BVT',
                'code2' => 'BV',
                'flag' => 'BV.png',
            ),
            37 => 
            array (
                'id_country' => 38,
                'negara_name' => 'Botswana',
                'code1' => 'BWA',
                'code2' => 'BW',
                'flag' => 'BW.png',
            ),
            38 => 
            array (
                'id_country' => 39,
                'negara_name' => 'Central African Republic',
                'code1' => 'CAF',
                'code2' => 'CF',
                'flag' => 'CF.png',
            ),
            39 => 
            array (
                'id_country' => 40,
                'negara_name' => 'Canada',
                'code1' => 'CAN',
                'code2' => 'CA',
                'flag' => 'CA.png',
            ),
            40 => 
            array (
                'id_country' => 41,
                'negara_name' => 'Cocos [Keeling] Islands',
                'code1' => 'CCK',
                'code2' => 'CC',
                'flag' => 'CC.png',
            ),
            41 => 
            array (
                'id_country' => 42,
                'negara_name' => 'Switzerland',
                'code1' => 'CHE',
                'code2' => 'CH',
                'flag' => 'CH.png',
            ),
            42 => 
            array (
                'id_country' => 43,
                'negara_name' => 'Chile',
                'code1' => 'CHL',
                'code2' => 'CL',
                'flag' => 'CL.png',
            ),
            43 => 
            array (
                'id_country' => 44,
                'negara_name' => 'China',
                'code1' => 'CHN',
                'code2' => 'CN',
                'flag' => 'CN.png',
            ),
            44 => 
            array (
                'id_country' => 45,
                'negara_name' => 'Ivory Coast',
                'code1' => 'CIV',
                'code2' => 'CI',
                'flag' => 'CI.png',
            ),
            45 => 
            array (
                'id_country' => 46,
                'negara_name' => 'Cameroon',
                'code1' => 'CMR',
                'code2' => 'CM',
                'flag' => 'CM.png',
            ),
            46 => 
            array (
                'id_country' => 47,
                'negara_name' => 'Democratic Republic of the Congo',
                'code1' => 'COD',
                'code2' => 'CD',
                'flag' => 'CD.png',
            ),
            47 => 
            array (
                'id_country' => 48,
                'negara_name' => 'Republic of the Congo',
                'code1' => 'COG',
                'code2' => 'CG',
                'flag' => 'CG.png',
            ),
            48 => 
            array (
                'id_country' => 49,
                'negara_name' => 'Cook Islands',
                'code1' => 'COK',
                'code2' => 'CK',
                'flag' => 'CK.png',
            ),
            49 => 
            array (
                'id_country' => 50,
                'negara_name' => 'Colombia',
                'code1' => 'COL',
                'code2' => 'CO',
                'flag' => 'CO.png',
            ),
            50 => 
            array (
                'id_country' => 51,
                'negara_name' => 'Comoros',
                'code1' => 'COM',
                'code2' => 'KM',
                'flag' => 'KM.png',
            ),
            51 => 
            array (
                'id_country' => 52,
                'negara_name' => 'Cape Verde',
                'code1' => 'CPV',
                'code2' => 'CV',
                'flag' => 'CV.png',
            ),
            52 => 
            array (
                'id_country' => 53,
                'negara_name' => 'Costa Rica',
                'code1' => 'CRI',
                'code2' => 'CR',
                'flag' => 'CR.png',
            ),
            53 => 
            array (
                'id_country' => 54,
                'negara_name' => 'Cuba',
                'code1' => 'CUB',
                'code2' => 'CU',
                'flag' => 'CU.png',
            ),
            54 => 
            array (
                'id_country' => 55,
                'negara_name' => 'Curacao',
                'code1' => 'CUW',
                'code2' => 'CW',
                'flag' => 'CW.png',
            ),
            55 => 
            array (
                'id_country' => 56,
                'negara_name' => 'Christmas Island',
                'code1' => 'CXR',
                'code2' => 'CX',
                'flag' => 'CX.png',
            ),
            56 => 
            array (
                'id_country' => 57,
                'negara_name' => 'Cayman Islands',
                'code1' => 'CYM',
                'code2' => 'KY',
                'flag' => 'KY.png',
            ),
            57 => 
            array (
                'id_country' => 58,
                'negara_name' => 'Cyprus',
                'code1' => 'CYP',
                'code2' => 'CY',
                'flag' => 'CY.png',
            ),
            58 => 
            array (
                'id_country' => 59,
                'negara_name' => 'Czech Republic',
                'code1' => 'CZE',
                'code2' => 'CZ',
                'flag' => 'CZ.png',
            ),
            59 => 
            array (
                'id_country' => 60,
                'negara_name' => 'Germany',
                'code1' => 'DEU',
                'code2' => 'DE',
                'flag' => 'DE.png',
            ),
            60 => 
            array (
                'id_country' => 61,
                'negara_name' => 'Djibouti',
                'code1' => 'DJI',
                'code2' => 'DJ',
                'flag' => 'DJ.png',
            ),
            61 => 
            array (
                'id_country' => 62,
                'negara_name' => 'Dominica',
                'code1' => 'DMA',
                'code2' => 'DM',
                'flag' => 'DM.png',
            ),
            62 => 
            array (
                'id_country' => 63,
                'negara_name' => 'Denmark',
                'code1' => 'DNK',
                'code2' => 'DK',
                'flag' => 'DK.png',
            ),
            63 => 
            array (
                'id_country' => 64,
                'negara_name' => 'Dominican Republic',
                'code1' => 'DOM',
                'code2' => 'DO',
                'flag' => 'DO.png',
            ),
            64 => 
            array (
                'id_country' => 65,
                'negara_name' => 'Algeria',
                'code1' => 'DZA',
                'code2' => 'DZ',
                'flag' => 'DZ.png',
            ),
            65 => 
            array (
                'id_country' => 66,
                'negara_name' => 'Ecuador',
                'code1' => 'ECU',
                'code2' => 'EC',
                'flag' => 'EC.png',
            ),
            66 => 
            array (
                'id_country' => 67,
                'negara_name' => 'Egypt',
                'code1' => 'EGY',
                'code2' => 'EG',
                'flag' => 'EG.png',
            ),
            67 => 
            array (
                'id_country' => 68,
                'negara_name' => 'Eritrea',
                'code1' => 'ERI',
                'code2' => 'ER',
                'flag' => 'ER.png',
            ),
            68 => 
            array (
                'id_country' => 69,
                'negara_name' => 'Western Sahara',
                'code1' => 'ESH',
                'code2' => 'EH',
                'flag' => 'EH.png',
            ),
            69 => 
            array (
                'id_country' => 70,
                'negara_name' => 'Spain',
                'code1' => 'ESP',
                'code2' => 'ES',
                'flag' => 'ES.png',
            ),
            70 => 
            array (
                'id_country' => 71,
                'negara_name' => 'Estonia',
                'code1' => 'EST',
                'code2' => 'EE',
                'flag' => 'EE.png',
            ),
            71 => 
            array (
                'id_country' => 72,
                'negara_name' => 'Ethiopia',
                'code1' => 'ETH',
                'code2' => 'ET',
                'flag' => 'ET.png',
            ),
            72 => 
            array (
                'id_country' => 73,
                'negara_name' => 'Finland',
                'code1' => 'FIN',
                'code2' => 'FI',
                'flag' => 'FI.png',
            ),
            73 => 
            array (
                'id_country' => 74,
                'negara_name' => 'Fiji',
                'code1' => 'FJI',
                'code2' => 'FJ',
                'flag' => 'FJ.png',
            ),
            74 => 
            array (
                'id_country' => 75,
                'negara_name' => 'Falkland Islands',
                'code1' => 'FLK',
                'code2' => 'FK',
                'flag' => 'FK.png',
            ),
            75 => 
            array (
                'id_country' => 76,
                'negara_name' => 'France',
                'code1' => 'FRA',
                'code2' => 'FR',
                'flag' => 'FR.png',
            ),
            76 => 
            array (
                'id_country' => 77,
                'negara_name' => 'Faroe Islands',
                'code1' => 'FRO',
                'code2' => 'FO',
                'flag' => 'FO.png',
            ),
            77 => 
            array (
                'id_country' => 78,
                'negara_name' => 'Micronesia',
                'code1' => 'FSM',
                'code2' => 'FM',
                'flag' => 'FM.png',
            ),
            78 => 
            array (
                'id_country' => 79,
                'negara_name' => 'Gabon',
                'code1' => 'GAB',
                'code2' => 'GA',
                'flag' => 'GA.png',
            ),
            79 => 
            array (
                'id_country' => 80,
                'negara_name' => 'United Kingdom',
                'code1' => 'GBR',
                'code2' => 'GB',
                'flag' => 'GB.png',
            ),
            80 => 
            array (
                'id_country' => 81,
                'negara_name' => 'Georgia',
                'code1' => 'GEO',
                'code2' => 'GE',
                'flag' => 'GE.png',
            ),
            81 => 
            array (
                'id_country' => 82,
                'negara_name' => 'Guernsey',
                'code1' => 'GGY',
                'code2' => 'GG',
                'flag' => 'GG.png',
            ),
            82 => 
            array (
                'id_country' => 83,
                'negara_name' => 'Ghana',
                'code1' => 'GHA',
                'code2' => 'GH',
                'flag' => 'GH.png',
            ),
            83 => 
            array (
                'id_country' => 84,
                'negara_name' => 'Gibraltar',
                'code1' => 'GIB',
                'code2' => 'GI',
                'flag' => 'GI.png',
            ),
            84 => 
            array (
                'id_country' => 85,
                'negara_name' => 'Guinea',
                'code1' => 'GIN',
                'code2' => 'GN',
                'flag' => 'GN.png',
            ),
            85 => 
            array (
                'id_country' => 86,
                'negara_name' => 'Guadeloupe',
                'code1' => 'GLP',
                'code2' => 'GP',
                'flag' => 'GP.png',
            ),
            86 => 
            array (
                'id_country' => 87,
                'negara_name' => 'Gambia',
                'code1' => 'GMB',
                'code2' => 'GM',
                'flag' => 'GM.png',
            ),
            87 => 
            array (
                'id_country' => 88,
                'negara_name' => 'Guinea-Bissau',
                'code1' => 'GNB',
                'code2' => 'GW',
                'flag' => 'GW.png',
            ),
            88 => 
            array (
                'id_country' => 89,
                'negara_name' => 'Equatorial Guinea',
                'code1' => 'GNQ',
                'code2' => 'GQ',
                'flag' => 'GQ.png',
            ),
            89 => 
            array (
                'id_country' => 90,
                'negara_name' => 'Greece',
                'code1' => 'GRC',
                'code2' => 'GR',
                'flag' => 'GR.png',
            ),
            90 => 
            array (
                'id_country' => 91,
                'negara_name' => 'Grenada',
                'code1' => 'GRD',
                'code2' => 'GD',
                'flag' => 'GD.png',
            ),
            91 => 
            array (
                'id_country' => 92,
                'negara_name' => 'Greenland',
                'code1' => 'GRL',
                'code2' => 'GL',
                'flag' => 'GL.png',
            ),
            92 => 
            array (
                'id_country' => 93,
                'negara_name' => 'Guatemala',
                'code1' => 'GTM',
                'code2' => 'GT',
                'flag' => 'GT.png',
            ),
            93 => 
            array (
                'id_country' => 94,
                'negara_name' => 'French Guiana',
                'code1' => 'GUF',
                'code2' => 'GF',
                'flag' => 'GF.png',
            ),
            94 => 
            array (
                'id_country' => 95,
                'negara_name' => 'Guam',
                'code1' => 'GUM',
                'code2' => 'GU',
                'flag' => 'GU.png',
            ),
            95 => 
            array (
                'id_country' => 96,
                'negara_name' => 'Guyana',
                'code1' => 'GUY',
                'code2' => 'GY',
                'flag' => 'GY.png',
            ),
            96 => 
            array (
                'id_country' => 97,
                'negara_name' => 'Hong Kong',
                'code1' => 'HKG',
                'code2' => 'HK',
                'flag' => 'HK.png',
            ),
            97 => 
            array (
                'id_country' => 98,
                'negara_name' => 'Heard Island and McDonald Islands',
                'code1' => 'HMD',
                'code2' => 'HM',
                'flag' => 'HM.png',
            ),
            98 => 
            array (
                'id_country' => 99,
                'negara_name' => 'Honduras',
                'code1' => 'HND',
                'code2' => 'HN',
                'flag' => 'HN.png',
            ),
            99 => 
            array (
                'id_country' => 100,
                'negara_name' => 'Croatia',
                'code1' => 'HRV',
                'code2' => 'HR',
                'flag' => 'HR.png',
            ),
            100 => 
            array (
                'id_country' => 101,
                'negara_name' => 'Haiti',
                'code1' => 'HTI',
                'code2' => 'HT',
                'flag' => 'HT.png',
            ),
            101 => 
            array (
                'id_country' => 102,
                'negara_name' => 'Hungary',
                'code1' => 'HUN',
                'code2' => 'HU',
                'flag' => 'HU.png',
            ),
            102 => 
            array (
                'id_country' => 103,
                'negara_name' => 'Indonesia',
                'code1' => 'IDN',
                'code2' => 'ID',
                'flag' => 'ID.png',
            ),
            103 => 
            array (
                'id_country' => 104,
                'negara_name' => 'Isle of Man',
                'code1' => 'IMN',
                'code2' => 'IM',
                'flag' => 'IM.png',
            ),
            104 => 
            array (
                'id_country' => 105,
                'negara_name' => 'India',
                'code1' => 'IND',
                'code2' => 'IN',
                'flag' => 'IN.png',
            ),
            105 => 
            array (
                'id_country' => 106,
                'negara_name' => 'British Indian Ocean Territory',
                'code1' => 'IOT',
                'code2' => 'IO',
                'flag' => 'IO.png',
            ),
            106 => 
            array (
                'id_country' => 107,
                'negara_name' => 'Ireland',
                'code1' => 'IRL',
                'code2' => 'IE',
                'flag' => 'IE.png',
            ),
            107 => 
            array (
                'id_country' => 108,
                'negara_name' => 'Iran',
                'code1' => 'IRN',
                'code2' => 'IR',
                'flag' => 'IR.png',
            ),
            108 => 
            array (
                'id_country' => 109,
                'negara_name' => 'Iraq',
                'code1' => 'IRQ',
                'code2' => 'IQ',
                'flag' => 'IQ.png',
            ),
            109 => 
            array (
                'id_country' => 110,
                'negara_name' => 'Iceland',
                'code1' => 'ISL',
                'code2' => 'IS',
                'flag' => 'IS.png',
            ),
            110 => 
            array (
                'id_country' => 111,
                'negara_name' => 'Israel',
                'code1' => 'ISR',
                'code2' => 'IL',
                'flag' => 'IL.png',
            ),
            111 => 
            array (
                'id_country' => 112,
                'negara_name' => 'Italy',
                'code1' => 'ITA',
                'code2' => 'IT',
                'flag' => 'IT.png',
            ),
            112 => 
            array (
                'id_country' => 113,
                'negara_name' => 'Jamaica',
                'code1' => 'JAM',
                'code2' => 'JM',
                'flag' => 'JM.png',
            ),
            113 => 
            array (
                'id_country' => 114,
                'negara_name' => 'Jersey',
                'code1' => 'JEY',
                'code2' => 'JE',
                'flag' => 'JE.png',
            ),
            114 => 
            array (
                'id_country' => 115,
                'negara_name' => 'Jordan',
                'code1' => 'JOR',
                'code2' => 'JO',
                'flag' => 'JO.png',
            ),
            115 => 
            array (
                'id_country' => 116,
                'negara_name' => 'Japan',
                'code1' => 'JPN',
                'code2' => 'JP',
                'flag' => 'JP.png',
            ),
            116 => 
            array (
                'id_country' => 117,
                'negara_name' => 'Kazakhstan',
                'code1' => 'KAZ',
                'code2' => 'KZ',
                'flag' => 'KZ.png',
            ),
            117 => 
            array (
                'id_country' => 118,
                'negara_name' => 'Kenya',
                'code1' => 'KEN',
                'code2' => 'KE',
                'flag' => 'KE.png',
            ),
            118 => 
            array (
                'id_country' => 119,
                'negara_name' => 'Kyrgyzstan',
                'code1' => 'KGZ',
                'code2' => 'KG',
                'flag' => 'KG.png',
            ),
            119 => 
            array (
                'id_country' => 120,
                'negara_name' => 'Cambodia',
                'code1' => 'KHM',
                'code2' => 'KH',
                'flag' => 'KH.png',
            ),
            120 => 
            array (
                'id_country' => 121,
                'negara_name' => 'Kiribati',
                'code1' => 'KIR',
                'code2' => 'KI',
                'flag' => 'KI.png',
            ),
            121 => 
            array (
                'id_country' => 122,
                'negara_name' => 'Saint Kitts and Nevis',
                'code1' => 'KNA',
                'code2' => 'KN',
                'flag' => 'KN.png',
            ),
            122 => 
            array (
                'id_country' => 123,
                'negara_name' => 'South Korea',
                'code1' => 'KOR',
                'code2' => 'KR',
                'flag' => 'KR.png',
            ),
            123 => 
            array (
                'id_country' => 124,
                'negara_name' => 'Kuwait',
                'code1' => 'KWT',
                'code2' => 'KW',
                'flag' => 'KW.png',
            ),
            124 => 
            array (
                'id_country' => 125,
                'negara_name' => 'Laos',
                'code1' => 'LAO',
                'code2' => 'LA',
                'flag' => 'LA.png',
            ),
            125 => 
            array (
                'id_country' => 126,
                'negara_name' => 'Lebanon',
                'code1' => 'LBN',
                'code2' => 'LB',
                'flag' => 'LB.png',
            ),
            126 => 
            array (
                'id_country' => 127,
                'negara_name' => 'Liberia',
                'code1' => 'LBR',
                'code2' => 'LR',
                'flag' => 'LR.png',
            ),
            127 => 
            array (
                'id_country' => 128,
                'negara_name' => 'Libya',
                'code1' => 'LBY',
                'code2' => 'LY',
                'flag' => 'LY.png',
            ),
            128 => 
            array (
                'id_country' => 129,
                'negara_name' => 'Saint Lucia',
                'code1' => 'LCA',
                'code2' => 'LC',
                'flag' => 'LC.png',
            ),
            129 => 
            array (
                'id_country' => 130,
                'negara_name' => 'Liechtenstein',
                'code1' => 'LIE',
                'code2' => 'LI',
                'flag' => 'LI.png',
            ),
            130 => 
            array (
                'id_country' => 131,
                'negara_name' => 'Sri Lanka',
                'code1' => 'LKA',
                'code2' => 'LK',
                'flag' => 'LK.png',
            ),
            131 => 
            array (
                'id_country' => 132,
                'negara_name' => 'Lesotho',
                'code1' => 'LSO',
                'code2' => 'LS',
                'flag' => 'LS.png',
            ),
            132 => 
            array (
                'id_country' => 133,
                'negara_name' => 'Lithuania',
                'code1' => 'LTU',
                'code2' => 'LT',
                'flag' => 'LT.png',
            ),
            133 => 
            array (
                'id_country' => 134,
                'negara_name' => 'Luxembourg',
                'code1' => 'LUX',
                'code2' => 'LU',
                'flag' => 'LU.png',
            ),
            134 => 
            array (
                'id_country' => 135,
                'negara_name' => 'Latvia',
                'code1' => 'LVA',
                'code2' => 'LV',
                'flag' => 'LV.png',
            ),
            135 => 
            array (
                'id_country' => 136,
                'negara_name' => 'Macao',
                'code1' => 'MAC',
                'code2' => 'MO',
                'flag' => 'MO.png',
            ),
            136 => 
            array (
                'id_country' => 137,
                'negara_name' => 'Saint Martin',
                'code1' => 'MAF',
                'code2' => 'MF',
                'flag' => 'MF.png',
            ),
            137 => 
            array (
                'id_country' => 138,
                'negara_name' => 'Morocco',
                'code1' => 'MAR',
                'code2' => 'MA',
                'flag' => 'MA.png',
            ),
            138 => 
            array (
                'id_country' => 139,
                'negara_name' => 'Monaco',
                'code1' => 'MCO',
                'code2' => 'MC',
                'flag' => 'MC.png',
            ),
            139 => 
            array (
                'id_country' => 140,
                'negara_name' => 'Moldova',
                'code1' => 'MDA',
                'code2' => 'MD',
                'flag' => 'MD.png',
            ),
            140 => 
            array (
                'id_country' => 141,
                'negara_name' => 'Madagascar',
                'code1' => 'MDG',
                'code2' => 'MG',
                'flag' => 'MG.png',
            ),
            141 => 
            array (
                'id_country' => 142,
                'negara_name' => 'Maldives',
                'code1' => 'MDV',
                'code2' => 'MV',
                'flag' => 'MV.png',
            ),
            142 => 
            array (
                'id_country' => 143,
                'negara_name' => 'Mexico',
                'code1' => 'MEX',
                'code2' => 'MX',
                'flag' => 'MX.png',
            ),
            143 => 
            array (
                'id_country' => 144,
                'negara_name' => 'Marshall Islands',
                'code1' => 'MHL',
                'code2' => 'MH',
                'flag' => 'MH.png',
            ),
            144 => 
            array (
                'id_country' => 145,
                'negara_name' => 'Macedonia',
                'code1' => 'MKD',
                'code2' => 'MK',
                'flag' => 'MK.png',
            ),
            145 => 
            array (
                'id_country' => 146,
                'negara_name' => 'Mali',
                'code1' => 'MLI',
                'code2' => 'ML',
                'flag' => 'ML.png',
            ),
            146 => 
            array (
                'id_country' => 147,
                'negara_name' => 'Malta',
                'code1' => 'MLT',
                'code2' => 'MT',
                'flag' => 'MT.png',
            ),
            147 => 
            array (
                'id_country' => 148,
                'negara_name' => 'Myanmar [Burma]',
                'code1' => 'MMR',
                'code2' => 'MM',
                'flag' => 'MM.png',
            ),
            148 => 
            array (
                'id_country' => 149,
                'negara_name' => 'Montenegro',
                'code1' => 'MNE',
                'code2' => 'ME',
                'flag' => 'ME.png',
            ),
            149 => 
            array (
                'id_country' => 150,
                'negara_name' => 'Mongolia',
                'code1' => 'MNG',
                'code2' => 'MN',
                'flag' => 'MN.png',
            ),
            150 => 
            array (
                'id_country' => 151,
                'negara_name' => 'Northern Mariana Islands',
                'code1' => 'MNP',
                'code2' => 'MP',
                'flag' => 'MP.png',
            ),
            151 => 
            array (
                'id_country' => 152,
                'negara_name' => 'Mozambique',
                'code1' => 'MOZ',
                'code2' => 'MZ',
                'flag' => 'MZ.png',
            ),
            152 => 
            array (
                'id_country' => 153,
                'negara_name' => 'Mauritania',
                'code1' => 'MRT',
                'code2' => 'MR',
                'flag' => 'MR.png',
            ),
            153 => 
            array (
                'id_country' => 154,
                'negara_name' => 'Montserrat',
                'code1' => 'MSR',
                'code2' => 'MS',
                'flag' => 'MS.png',
            ),
            154 => 
            array (
                'id_country' => 155,
                'negara_name' => 'Martinique',
                'code1' => 'MTQ',
                'code2' => 'MQ',
                'flag' => 'MQ.png',
            ),
            155 => 
            array (
                'id_country' => 156,
                'negara_name' => 'Mauritius',
                'code1' => 'MUS',
                'code2' => 'MU',
                'flag' => 'MU.png',
            ),
            156 => 
            array (
                'id_country' => 157,
                'negara_name' => 'Malawi',
                'code1' => 'MWI',
                'code2' => 'MW',
                'flag' => 'MW.png',
            ),
            157 => 
            array (
                'id_country' => 158,
                'negara_name' => 'Malaysia',
                'code1' => 'MYS',
                'code2' => 'MY',
                'flag' => 'MY.png',
            ),
            158 => 
            array (
                'id_country' => 159,
                'negara_name' => 'Mayotte',
                'code1' => 'MYT',
                'code2' => 'YT',
                'flag' => 'YT.png',
            ),
            159 => 
            array (
                'id_country' => 160,
                'negara_name' => 'Namibia',
                'code1' => 'NAM',
                'code2' => 'NA',
                'flag' => 'NA.png',
            ),
            160 => 
            array (
                'id_country' => 161,
                'negara_name' => 'New Caledonia',
                'code1' => 'NCL',
                'code2' => 'NC',
                'flag' => 'NC.png',
            ),
            161 => 
            array (
                'id_country' => 162,
                'negara_name' => 'Niger',
                'code1' => 'NER',
                'code2' => 'NE',
                'flag' => 'NE.png',
            ),
            162 => 
            array (
                'id_country' => 163,
                'negara_name' => 'Norfolk Island',
                'code1' => 'NFK',
                'code2' => 'NF',
                'flag' => 'NF.png',
            ),
            163 => 
            array (
                'id_country' => 164,
                'negara_name' => 'Nigeria',
                'code1' => 'NGA',
                'code2' => 'NG',
                'flag' => 'NG.png',
            ),
            164 => 
            array (
                'id_country' => 165,
                'negara_name' => 'Nicaragua',
                'code1' => 'NIC',
                'code2' => 'NI',
                'flag' => 'NI.png',
            ),
            165 => 
            array (
                'id_country' => 166,
                'negara_name' => 'Niue',
                'code1' => 'NIU',
                'code2' => 'NU',
                'flag' => 'NU.png',
            ),
            166 => 
            array (
                'id_country' => 167,
                'negara_name' => 'Netherlands',
                'code1' => 'NLD',
                'code2' => 'NL',
                'flag' => 'NL.png',
            ),
            167 => 
            array (
                'id_country' => 168,
                'negara_name' => 'Norway',
                'code1' => 'NOR',
                'code2' => 'NO',
                'flag' => 'NO.png',
            ),
            168 => 
            array (
                'id_country' => 169,
                'negara_name' => 'Nepal',
                'code1' => 'NPL',
                'code2' => 'NP',
                'flag' => 'NP.png',
            ),
            169 => 
            array (
                'id_country' => 170,
                'negara_name' => 'Nauru',
                'code1' => 'NRU',
                'code2' => 'NR',
                'flag' => 'NR.png',
            ),
            170 => 
            array (
                'id_country' => 171,
                'negara_name' => 'New Zealand',
                'code1' => 'NZL',
                'code2' => 'NZ',
                'flag' => 'NZ.png',
            ),
            171 => 
            array (
                'id_country' => 172,
                'negara_name' => 'Oman',
                'code1' => 'OMN',
                'code2' => 'OM',
                'flag' => 'OM.png',
            ),
            172 => 
            array (
                'id_country' => 173,
                'negara_name' => 'Pakistan',
                'code1' => 'PAK',
                'code2' => 'PK',
                'flag' => 'PK.png',
            ),
            173 => 
            array (
                'id_country' => 174,
                'negara_name' => 'Panama',
                'code1' => 'PAN',
                'code2' => 'PA',
                'flag' => 'PA.png',
            ),
            174 => 
            array (
                'id_country' => 175,
                'negara_name' => 'Pitcairn Islands',
                'code1' => 'PCN',
                'code2' => 'PN',
                'flag' => 'PN.png',
            ),
            175 => 
            array (
                'id_country' => 176,
                'negara_name' => 'Peru',
                'code1' => 'PER',
                'code2' => 'PE',
                'flag' => 'PE.png',
            ),
            176 => 
            array (
                'id_country' => 177,
                'negara_name' => 'Philippines',
                'code1' => 'PHL',
                'code2' => 'PH',
                'flag' => 'PH.png',
            ),
            177 => 
            array (
                'id_country' => 178,
                'negara_name' => 'Palau',
                'code1' => 'PLW',
                'code2' => 'PW',
                'flag' => 'PW.png',
            ),
            178 => 
            array (
                'id_country' => 179,
                'negara_name' => 'Papua New Guinea',
                'code1' => 'PNG',
                'code2' => 'PG',
                'flag' => 'PG.png',
            ),
            179 => 
            array (
                'id_country' => 180,
                'negara_name' => 'Poland',
                'code1' => 'POL',
                'code2' => 'PL',
                'flag' => 'PL.png',
            ),
            180 => 
            array (
                'id_country' => 181,
                'negara_name' => 'Puerto Rico',
                'code1' => 'PRI',
                'code2' => 'PR',
                'flag' => 'PR.png',
            ),
            181 => 
            array (
                'id_country' => 182,
                'negara_name' => 'North Korea',
                'code1' => 'PRK',
                'code2' => 'KP',
                'flag' => 'KP.png',
            ),
            182 => 
            array (
                'id_country' => 183,
                'negara_name' => 'Portugal',
                'code1' => 'PRT',
                'code2' => 'PT',
                'flag' => 'PT.png',
            ),
            183 => 
            array (
                'id_country' => 184,
                'negara_name' => 'Paraguay',
                'code1' => 'PRY',
                'code2' => 'PY',
                'flag' => 'PY.png',
            ),
            184 => 
            array (
                'id_country' => 185,
                'negara_name' => 'Palestine',
                'code1' => 'PSE',
                'code2' => 'PS',
                'flag' => 'PS.png',
            ),
            185 => 
            array (
                'id_country' => 186,
                'negara_name' => 'French Polynesia',
                'code1' => 'PYF',
                'code2' => 'PF',
                'flag' => 'PF.png',
            ),
            186 => 
            array (
                'id_country' => 187,
                'negara_name' => 'Qatar',
                'code1' => 'QAT',
                'code2' => 'QA',
                'flag' => 'QA.png',
            ),
            187 => 
            array (
                'id_country' => 188,
                'negara_name' => 'Réunion',
                'code1' => 'REU',
                'code2' => 'RE',
                'flag' => 'RE.png',
            ),
            188 => 
            array (
                'id_country' => 189,
                'negara_name' => 'Romania',
                'code1' => 'ROU',
                'code2' => 'RO',
                'flag' => 'RO.png',
            ),
            189 => 
            array (
                'id_country' => 190,
                'negara_name' => 'Russia',
                'code1' => 'RUS',
                'code2' => 'RU',
                'flag' => 'RU.png',
            ),
            190 => 
            array (
                'id_country' => 191,
                'negara_name' => 'Rwanda',
                'code1' => 'RWA',
                'code2' => 'RW',
                'flag' => 'RW.png',
            ),
            191 => 
            array (
                'id_country' => 192,
                'negara_name' => 'Saudi Arabia',
                'code1' => 'SAU',
                'code2' => 'SA',
                'flag' => 'SA.png',
            ),
            192 => 
            array (
                'id_country' => 193,
                'negara_name' => 'Sudan',
                'code1' => 'SDN',
                'code2' => 'SD',
                'flag' => 'SD.png',
            ),
            193 => 
            array (
                'id_country' => 194,
                'negara_name' => 'Senegal',
                'code1' => 'SEN',
                'code2' => 'SN',
                'flag' => 'SN.png',
            ),
            194 => 
            array (
                'id_country' => 195,
                'negara_name' => 'Singapore',
                'code1' => 'SGP',
                'code2' => 'SG',
                'flag' => 'SG.png',
            ),
            195 => 
            array (
                'id_country' => 196,
                'negara_name' => 'South Georgia and the South Sandwich Islands',
                'code1' => 'SGS',
                'code2' => 'GS',
                'flag' => 'GS.png',
            ),
            196 => 
            array (
                'id_country' => 197,
                'negara_name' => 'Saint Helena',
                'code1' => 'SHN',
                'code2' => 'SH',
                'flag' => 'SH.png',
            ),
            197 => 
            array (
                'id_country' => 198,
                'negara_name' => 'Svalbard and Jan Mayen',
                'code1' => 'SJM',
                'code2' => 'SJ',
                'flag' => 'SJ.png',
            ),
            198 => 
            array (
                'id_country' => 199,
                'negara_name' => 'Solomon Islands',
                'code1' => 'SLB',
                'code2' => 'SB',
                'flag' => 'SB.png',
            ),
            199 => 
            array (
                'id_country' => 200,
                'negara_name' => 'Sierra Leone',
                'code1' => 'SLE',
                'code2' => 'SL',
                'flag' => 'SL.png',
            ),
            200 => 
            array (
                'id_country' => 201,
                'negara_name' => 'El Salvador',
                'code1' => 'SLV',
                'code2' => 'SV',
                'flag' => 'SV.png',
            ),
            201 => 
            array (
                'id_country' => 202,
                'negara_name' => 'San Marino',
                'code1' => 'SMR',
                'code2' => 'SM',
                'flag' => 'SM.png',
            ),
            202 => 
            array (
                'id_country' => 203,
                'negara_name' => 'Somalia',
                'code1' => 'SOM',
                'code2' => 'SO',
                'flag' => 'SO.png',
            ),
            203 => 
            array (
                'id_country' => 204,
                'negara_name' => 'Saint Pierre and Miquelon',
                'code1' => 'SPM',
                'code2' => 'PM',
                'flag' => 'PM.png',
            ),
            204 => 
            array (
                'id_country' => 205,
                'negara_name' => 'Serbia',
                'code1' => 'SRB',
                'code2' => 'RS',
                'flag' => 'RS.png',
            ),
            205 => 
            array (
                'id_country' => 206,
                'negara_name' => 'South Sudan',
                'code1' => 'SSD',
                'code2' => 'SS',
                'flag' => 'SS.png',
            ),
            206 => 
            array (
                'id_country' => 207,
                'negara_name' => 'São Tomé and Príncipe',
                'code1' => 'STP',
                'code2' => 'ST',
                'flag' => 'ST.png',
            ),
            207 => 
            array (
                'id_country' => 208,
                'negara_name' => 'Suriname',
                'code1' => 'SUR',
                'code2' => 'SR',
                'flag' => 'SR.png',
            ),
            208 => 
            array (
                'id_country' => 209,
                'negara_name' => 'Slovakia',
                'code1' => 'SVK',
                'code2' => 'SK',
                'flag' => 'SK.png',
            ),
            209 => 
            array (
                'id_country' => 210,
                'negara_name' => 'Slovenia',
                'code1' => 'SVN',
                'code2' => 'SI',
                'flag' => 'SI.png',
            ),
            210 => 
            array (
                'id_country' => 211,
                'negara_name' => 'Sweden',
                'code1' => 'SWE',
                'code2' => 'SE',
                'flag' => 'SE.png',
            ),
            211 => 
            array (
                'id_country' => 212,
                'negara_name' => 'Swaziland',
                'code1' => 'SWZ',
                'code2' => 'SZ',
                'flag' => 'SZ.png',
            ),
            212 => 
            array (
                'id_country' => 213,
                'negara_name' => 'Sint Maarten',
                'code1' => 'SXM',
                'code2' => 'SX',
                'flag' => 'SX.png',
            ),
            213 => 
            array (
                'id_country' => 214,
                'negara_name' => 'Seychelles',
                'code1' => 'SYC',
                'code2' => 'SC',
                'flag' => 'SC.png',
            ),
            214 => 
            array (
                'id_country' => 215,
                'negara_name' => 'Syria',
                'code1' => 'SYR',
                'code2' => 'SY',
                'flag' => 'SY.png',
            ),
            215 => 
            array (
                'id_country' => 216,
                'negara_name' => 'Turks and Caicos Islands',
                'code1' => 'TCA',
                'code2' => 'TC',
                'flag' => 'TC.png',
            ),
            216 => 
            array (
                'id_country' => 217,
                'negara_name' => 'Chad',
                'code1' => 'TCD',
                'code2' => 'TD',
                'flag' => 'TD.png',
            ),
            217 => 
            array (
                'id_country' => 218,
                'negara_name' => 'Togo',
                'code1' => 'TGO',
                'code2' => 'TG',
                'flag' => 'TG.png',
            ),
            218 => 
            array (
                'id_country' => 219,
                'negara_name' => 'Thailand',
                'code1' => 'THA',
                'code2' => 'TH',
                'flag' => 'TH.png',
            ),
            219 => 
            array (
                'id_country' => 220,
                'negara_name' => 'Tajikistan',
                'code1' => 'TJK',
                'code2' => 'TJ',
                'flag' => 'TJ.png',
            ),
            220 => 
            array (
                'id_country' => 221,
                'negara_name' => 'Tokelau',
                'code1' => 'TKL',
                'code2' => 'TK',
                'flag' => 'TK.png',
            ),
            221 => 
            array (
                'id_country' => 222,
                'negara_name' => 'Turkmenistan',
                'code1' => 'TKM',
                'code2' => 'TM',
                'flag' => 'TM.png',
            ),
            222 => 
            array (
                'id_country' => 223,
                'negara_name' => 'East Timor',
                'code1' => 'TLS',
                'code2' => 'TL',
                'flag' => 'TL.png',
            ),
            223 => 
            array (
                'id_country' => 224,
                'negara_name' => 'Tonga',
                'code1' => 'TON',
                'code2' => 'TO',
                'flag' => 'TO.png',
            ),
            224 => 
            array (
                'id_country' => 225,
                'negara_name' => 'Trinidad and Tobago',
                'code1' => 'TTO',
                'code2' => 'TT',
                'flag' => 'TT.png',
            ),
            225 => 
            array (
                'id_country' => 226,
                'negara_name' => 'Tunisia',
                'code1' => 'TUN',
                'code2' => 'TN',
                'flag' => 'TN.png',
            ),
            226 => 
            array (
                'id_country' => 227,
                'negara_name' => 'Turkey',
                'code1' => 'TUR',
                'code2' => 'TR',
                'flag' => 'TR.png',
            ),
            227 => 
            array (
                'id_country' => 228,
                'negara_name' => 'Tuvalu',
                'code1' => 'TUV',
                'code2' => 'TV',
                'flag' => 'TV.png',
            ),
            228 => 
            array (
                'id_country' => 229,
                'negara_name' => 'Taiwan',
                'code1' => 'TWN',
                'code2' => 'TW',
                'flag' => 'TW.png',
            ),
            229 => 
            array (
                'id_country' => 230,
                'negara_name' => 'Tanzania',
                'code1' => 'TZA',
                'code2' => 'TZ',
                'flag' => 'TZ.png',
            ),
            230 => 
            array (
                'id_country' => 231,
                'negara_name' => 'Uganda',
                'code1' => 'UGA',
                'code2' => 'UG',
                'flag' => 'UG.png',
            ),
            231 => 
            array (
                'id_country' => 232,
                'negara_name' => 'Ukraine',
                'code1' => 'UKR',
                'code2' => 'UA',
                'flag' => 'UA.png',
            ),
            232 => 
            array (
                'id_country' => 233,
                'negara_name' => 'U.S. Minor Outlying Islands',
                'code1' => 'UMI',
                'code2' => 'UM',
                'flag' => 'UM.png',
            ),
            233 => 
            array (
                'id_country' => 234,
                'negara_name' => 'Uruguay',
                'code1' => 'URY',
                'code2' => 'UY',
                'flag' => 'UY.png',
            ),
            234 => 
            array (
                'id_country' => 235,
                'negara_name' => 'United States',
                'code1' => 'USA',
                'code2' => 'US',
                'flag' => 'US.png',
            ),
            235 => 
            array (
                'id_country' => 236,
                'negara_name' => 'Uzbekistan',
                'code1' => 'UZB',
                'code2' => 'UZ',
                'flag' => 'UZ.png',
            ),
            236 => 
            array (
                'id_country' => 237,
                'negara_name' => 'Vatican City',
                'code1' => 'VAT',
                'code2' => 'VA',
                'flag' => 'VA.png',
            ),
            237 => 
            array (
                'id_country' => 238,
                'negara_name' => 'Saint Vincent and the Grenadines',
                'code1' => 'VCT',
                'code2' => 'VC',
                'flag' => 'VC.png',
            ),
            238 => 
            array (
                'id_country' => 239,
                'negara_name' => 'Venezuela',
                'code1' => 'VEN',
                'code2' => 'VE',
                'flag' => 'VE.png',
            ),
            239 => 
            array (
                'id_country' => 240,
                'negara_name' => 'British Virgin Islands',
                'code1' => 'VGB',
                'code2' => 'VG',
                'flag' => 'VG.png',
            ),
            240 => 
            array (
                'id_country' => 241,
                'negara_name' => 'U.S. Virgin Islands',
                'code1' => 'VIR',
                'code2' => 'VI',
                'flag' => 'VI.png',
            ),
            241 => 
            array (
                'id_country' => 242,
                'negara_name' => 'Vietnam',
                'code1' => 'VNM',
                'code2' => 'VN',
                'flag' => 'VN.png',
            ),
            242 => 
            array (
                'id_country' => 243,
                'negara_name' => 'Vanuatu',
                'code1' => 'VUT',
                'code2' => 'VU',
                'flag' => 'VU.png',
            ),
            243 => 
            array (
                'id_country' => 244,
                'negara_name' => 'Wallis and Futuna',
                'code1' => 'WLF',
                'code2' => 'WF',
                'flag' => 'WF.png',
            ),
            244 => 
            array (
                'id_country' => 245,
                'negara_name' => 'Samoa',
                'code1' => 'WSM',
                'code2' => 'WS',
                'flag' => 'WS.png',
            ),
            245 => 
            array (
                'id_country' => 246,
                'negara_name' => 'Kosovo',
                'code1' => 'XKX',
                'code2' => 'XK',
                'flag' => 'XK.png',
            ),
            246 => 
            array (
                'id_country' => 247,
                'negara_name' => 'Yemen',
                'code1' => 'YEM',
                'code2' => 'YE',
                'flag' => 'YE.png',
            ),
            247 => 
            array (
                'id_country' => 248,
                'negara_name' => 'South Africa',
                'code1' => 'ZAF',
                'code2' => 'ZA',
                'flag' => 'ZA.png',
            ),
            248 => 
            array (
                'id_country' => 249,
                'negara_name' => 'Zambia',
                'code1' => 'ZMB',
                'code2' => 'ZM',
                'flag' => 'ZM.png',
            ),
            249 => 
            array (
                'id_country' => 250,
                'negara_name' => 'Zimbabwe',
                'code1' => 'ZWE',
                'code2' => 'ZW',
                'flag' => 'ZW.png',
            ),
        ));
        
        
    }
}