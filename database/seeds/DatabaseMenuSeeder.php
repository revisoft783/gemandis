<?php

use Database\Seeders\PeranMenuSubmenuTableSeeder;
use Database\Seeders\PeranMenuTableSeeder;
use Database\Seeders\PeranTableSeeder;
use Database\Seeders\SubmenuTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PeranTableSeeder::class);
        $this->call(MenuTable::class);
        $this->call(SubmenuTableSeeder::class);
        $this->call(PeranMenuTableSeeder::class);
        $this->call(PeranMenuSubmenuTableSeeder::class);
    }
}
