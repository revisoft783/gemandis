<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PeranMenuSubmenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('peran_menu_submenu')->delete();
        
        \DB::table('peran_menu_submenu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'peran_menu_id' => 1,
                'submenu_id' => 1,
                'created_at' => '2021-05-07 13:36:13',
                'updated_at' => '2021-05-07 13:36:13',
            ),
            1 => 
            array (
                'id' => 2,
                'peran_menu_id' => 1,
                'submenu_id' => 2,
                'created_at' => '2021-05-07 13:36:13',
                'updated_at' => '2021-05-07 13:36:13',
            ),
            2 => 
            array (
                'id' => 3,
                'peran_menu_id' => 1,
                'submenu_id' => 3,
                'created_at' => '2021-05-07 13:36:13',
                'updated_at' => '2021-05-07 13:36:13',
            ),
            3 => 
            array (
                'id' => 4,
                'peran_menu_id' => 2,
                'submenu_id' => 4,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            4 => 
            array (
                'id' => 5,
                'peran_menu_id' => 2,
                'submenu_id' => 5,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            5 => 
            array (
                'id' => 6,
                'peran_menu_id' => 2,
                'submenu_id' => 6,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            6 => 
            array (
                'id' => 7,
                'peran_menu_id' => 2,
                'submenu_id' => 7,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            7 => 
            array (
                'id' => 8,
                'peran_menu_id' => 2,
                'submenu_id' => 8,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            8 => 
            array (
                'id' => 9,
                'peran_menu_id' => 2,
                'submenu_id' => 9,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            9 => 
            array (
                'id' => 10,
                'peran_menu_id' => 3,
                'submenu_id' => 10,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            10 => 
            array (
                'id' => 11,
                'peran_menu_id' => 3,
                'submenu_id' => 11,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            11 => 
            array (
                'id' => 12,
                'peran_menu_id' => 3,
                'submenu_id' => 12,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            12 => 
            array (
                'id' => 13,
                'peran_menu_id' => 3,
                'submenu_id' => 13,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            13 => 
            array (
                'id' => 14,
                'peran_menu_id' => 3,
                'submenu_id' => 14,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            14 => 
            array (
                'id' => 15,
                'peran_menu_id' => 3,
                'submenu_id' => 15,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            15 => 
            array (
                'id' => 16,
                'peran_menu_id' => 4,
                'submenu_id' => 16,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            16 => 
            array (
                'id' => 17,
                'peran_menu_id' => 4,
                'submenu_id' => 17,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            17 => 
            array (
                'id' => 18,
                'peran_menu_id' => 4,
                'submenu_id' => 18,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            18 => 
            array (
                'id' => 19,
                'peran_menu_id' => 4,
                'submenu_id' => 19,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            19 => 
            array (
                'id' => 20,
                'peran_menu_id' => 4,
                'submenu_id' => 20,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            20 => 
            array (
                'id' => 21,
                'peran_menu_id' => 4,
                'submenu_id' => 21,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            21 => 
            array (
                'id' => 22,
                'peran_menu_id' => 5,
                'submenu_id' => 22,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            22 => 
            array (
                'id' => 23,
                'peran_menu_id' => 5,
                'submenu_id' => 23,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            23 => 
            array (
                'id' => 24,
                'peran_menu_id' => 5,
                'submenu_id' => 24,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            24 => 
            array (
                'id' => 25,
                'peran_menu_id' => 5,
                'submenu_id' => 25,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            25 => 
            array (
                'id' => 26,
                'peran_menu_id' => 5,
                'submenu_id' => 49,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            26 => 
            array (
                'id' => 27,
                'peran_menu_id' => 5,
                'submenu_id' => 26,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            27 => 
            array (
                'id' => 28,
                'peran_menu_id' => 5,
                'submenu_id' => 27,
                'created_at' => '2022-01-18 11:08:26',
                'updated_at' => '2022-01-18 11:08:26',
            ),
            28 => 
            array (
                'id' => 29,
                'peran_menu_id' => 5,
                'submenu_id' => 28,
                'created_at' => '2022-02-02 11:39:11',
                'updated_at' => '2022-02-02 11:39:11',
            ),
        ));
        
        
    }
}