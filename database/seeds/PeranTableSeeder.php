<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PeranTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('peran')->delete();
        
        \DB::table('peran')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'Admin',
                'created_at' => '2021-05-07 13:36:19',
                'updated_at' => '2021-05-07 13:36:19',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'Staff',
                'created_at' => '2021-05-07 14:00:02',
                'updated_at' => '2021-05-07 14:00:02',
            ),
            2 => 
            array (
                'id' => 3,
                'nama' => 'Operator',
                'created_at' => '2021-08-24 10:21:03',
                'updated_at' => '2021-08-24 10:21:03',
            ),
            3 => 
            array (
                'id' => 4,
                'nama' => 'KAUR TU',
                'created_at' => '2021-10-03 10:04:46',
                'updated_at' => '2021-10-29 19:46:57',
            ),
            4 => 
            array (
                'id' => 5,
                'nama' => 'KESRA',
                'created_at' => '2021-10-03 10:05:26',
                'updated_at' => '2021-10-03 10:05:26',
            ),
            5 => 
            array (
                'id' => 6,
                'nama' => 'KASI PELAYANAN',
                'created_at' => '2021-10-23 10:51:09',
                'updated_at' => '2021-10-29 19:45:48',
            ),
            6 => 
            array (
                'id' => 7,
                'nama' => 'KAUR KEUANGAN',
                'created_at' => '2021-10-29 09:07:30',
                'updated_at' => '2021-10-29 09:07:30',
            ),
            7 => 
            array (
                'id' => 8,
                'nama' => 'KAUR PERENCANAAN',
                'created_at' => '2021-10-29 09:08:32',
                'updated_at' => '2021-10-29 09:08:32',
            ),
            8 => 
            array (
                'id' => 10,
                'nama' => 'KASI PEM',
                'created_at' => '2021-10-29 09:20:14',
                'updated_at' => '2021-10-29 09:20:14',
            ),
            9 => 
            array (
                'id' => 11,
                'nama' => 'jasa 1',
                'created_at' => '2021-12-30 09:39:59',
                'updated_at' => '2021-12-30 09:41:16',
            ),
            10 => 
            array (
                'id' => 12,
                'nama' => 'jasa 2',
                'created_at' => '2021-12-30 09:40:27',
                'updated_at' => '2021-12-30 09:40:27',
            ),
            11 => 
            array (
                'id' => 13,
                'nama' => 'jasa 3',
                'created_at' => '2021-12-30 09:41:03',
                'updated_at' => '2021-12-30 09:41:03',
            ),
            12 => 
            array (
                'id' => 99,
                'nama' => 'Umum',
                'created_at' => '2022-01-24 15:15:34',
                'updated_at' => '2022-01-24 15:15:38',
            ),
        ));
        
        
    }
}