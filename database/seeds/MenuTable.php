<?php

use Illuminate\Database\Seeder;

class MenuTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('menu')->delete();

        \DB::table('menu')->insert(array (
            0 =>
            array (
                'id' => 1,
                'nama' => 'Profil Desa',
                'key_nama'=>'Profil Desa',
                'show_droid' => 1,
                'created_at' => '2022-01-18 10:58:23',
                'updated_at' => '2022-01-18 10:58:23',
            ),
            1 =>
            array (
                'id' => 2,
                'nama' => 'Administrasi Perangkat',
                'key_nama' => 'Administrasi Perangkat',
                'show_droid' => 0,
                'created_at' => '2022-01-18 10:58:23',
                'updated_at' => '2022-01-18 10:58:23',
            ),
            2 =>
            array (
                'id' => 3,
                'nama' => 'Kelola Penduduk',
                'key_nama' => 'Kelola Penduduk',
                'show_droid' => 0,
                'created_at' => '2022-01-18 10:58:23',
                'updated_at' => '2022-01-18 10:58:23',
            ),
            3 =>
            array (
                'id' => 4,
                'nama' => 'Sekretariat',
                'key_nama' => 'Sekretariat',
                'show_droid' => 0,
                'created_at' => '2022-01-18 10:58:23',
                'updated_at' => '2022-01-18 10:58:23',
            ),
            4 =>
            array (
                'id' => 5,
                'nama' => 'Menu',
                'key_nama' => 'Menu',
                'show_droid' => 0,
                'created_at' => '2022-01-18 10:58:23',
                'updated_at' => '2022-01-18 10:58:23',
            ),
            5 =>
            array (
                'id' => 6,
                'nama' => 'Pelayanan Masyarakat',
                'key_nama' => 'Pelayanan Masyarakat',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            6 =>
            array (
                'id' => 7,
                'nama' => 'Organisasi',
                'key_nama' => 'Organisasi',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            7 =>
            array (
                'id' => 8,
                'nama' => 'Bidang',
                'key_nama' => 'Bidang',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            8 =>
            array (
                'id' => 9,
                'nama' => 'Program Bantuan',
                'key_nama' => 'Program Bantuan',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            9 =>
            array (
                'id' => 10,
                'nama' => 'Kegiatan Pembangunan',
                'key_nama' => 'Kegiatan Pembangunan',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            10 =>
            array (
                'id' => 11,
                'nama' => 'Potensi Desa',
                'key_nama' => 'Potensi Desa',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            11 =>
            array (
                'id' => 12,
                'nama' => 'Laporan',
                'key_nama' => 'Laporan',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            12 =>
            array (
                'id' => 13,
                'nama' => 'Badan Usaha Desa',
                'key_nama' => 'Badan Usaha Desa',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            13 =>
            array (
                'id' => 14,
                'nama' => 'Informasi',
                'key_nama' => 'Informasi',
                'show_droid' => 1,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
            14 =>
            array (
                'id' => 15,
                'nama' => 'Form Surat',
                'key_nama' => 'form_surat',
                'show_droid' => 0,
                'created_at' => '2022-01-27 11:44:07',
                'updated_at' => '2022-01-27 11:44:07',
            ),
        ));

    }
}
