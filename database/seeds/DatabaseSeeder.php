<?php

use Database\Seeders\MenuTableSeeder;
use Database\Seeders\SubmenuTableSeeder;
use Database\Seeders\UsersTableSeeder;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DatabaseMenuSeeder::class);
        // $this->call(UserSeeder::class);
        $this->call(DesaSeeder::class);
        $this->call(AgamaSeeder::class);
        $this->call(DarahSeeder::class);
        $this->call(PekerjaanSeeder::class);
        $this->call(PendidikanSeeder::class);
        $this->call(StatusHubunganDalamKeluargaSeeder::class);
        $this->call(StatusPerkawinanSeeder::class);
        $this->call(JenisAnggaranSeeder::class);
        $this->call(KelompokJenisAnggaranSeeder::class);
        $this->call(DetailJenisAnggaranSeeder::class);
        $this->call(AkseptorKbSeeder::class);
        $this->call(AsuransiSeeder::class);
        $this->call(JenisCacatSeeder::class);
        $this->call(JenisKelahiranSeeder::class);
        $this->call(PenolongKelahiranSeeder::class);
        $this->call(SakitMenahunSeeder::class);
        $this->call(StatusPendudukSeeder::class);
        $this->call(StatusRekamSeeder::class);
        $this->call(TempatDilahirkanSeeder::class);
        $this->call(KodeSuratSeeder::class);
        $this->call(BarangSeeder::class);

        // $this->call(MenuTableSeeder::class);
        // $this->call(SubmenuTableSeeder::class);
        // $this->call(PeranMenuTableSeeder::class);
        // $this->call(PeranMenuSubmenuTableSeeder::class);
        // $this->call(PeranTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(SubdistrictsTableSeeder::class);
        $this->call(NegaraTableSeeder::class);
    }
}
