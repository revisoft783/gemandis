<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SubmenuTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('submenu')->delete();
        
        \DB::table('submenu')->insert(array (
            0 => 
            array (
                'id' => 1,
                'menu_id' => 1,
                'nama' => 'Identitas Desa',
                'url' => 'identitas-desa',
                'icon' => 'fas fa-id-card',
                'warna' => '#11cdef',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            1 => 
            array (
                'id' => 2,
                'menu_id' => 1,
                'nama' => 'Wilayah Administratif',
                'url' => 'dusun',
                'icon' => 'fas fa-map',
                'warna' => '#ffd600',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            2 => 
            array (
                'id' => 3,
                'menu_id' => 1,
                'nama' => 'Pemerintahan Desa',
                'url' => 'pemerintahan-desa',
                'icon' => 'fa fa-sitemap',
                'warna' => '#2dce89',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            3 => 
            array (
                'id' => 4,
                'menu_id' => 2,
                'nama' => 'Sekretaris Desa',
                'url' => '#',
                'icon' => 'fas fa-user',
                'warna' => '#11cdef',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            4 => 
            array (
                'id' => 5,
                'menu_id' => 2,
                'nama' => 'Kaur Tata Usaha',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#2bffc6',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            5 => 
            array (
                'id' => 6,
                'menu_id' => 2,
                'nama' => 'Kaur Perencanaan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#2dce89',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            6 => 
            array (
                'id' => 7,
                'menu_id' => 2,
                'nama' => 'Kasi Pemerintahan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#5e72e4',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            7 => 
            array (
                'id' => 8,
                'menu_id' => 2,
                'nama' => 'Kasi Pelayanan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            8 => 
            array (
                'id' => 9,
                'menu_id' => 2,
                'nama' => 'Kasi Kesejahteraan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#f5365c',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            9 => 
            array (
                'id' => 10,
                'menu_id' => 3,
                'nama' => 'Penduduk',
                'url' => 'penduduk',
                'icon' => 'fas fa-user',
                'warna' => '#2dce89',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            10 => 
            array (
                'id' => 11,
                'menu_id' => 3,
                'nama' => 'Keluarga',
                'url' => 'keluarga-penduduk',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            11 => 
            array (
                'id' => 12,
                'menu_id' => 3,
                'nama' => 'Calon Pemilih',
                'url' => 'calon-pemilih',
                'icon' => 'fas fa-user',
                'warna' => '#2bffc6',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            12 => 
            array (
                'id' => 13,
                'menu_id' => 3,
                'nama' => 'Group',
                'url' => 'grup',
                'icon' => 'fas fa-users',
                'warna' => '#2dce89',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            13 => 
            array (
                'id' => 14,
                'menu_id' => 3,
                'nama' => 'Analisis',
                'url' => 'analisis',
                'icon' => 'fa fa-calculator',
                'warna' => '#5e72e4',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            14 => 
            array (
                'id' => 15,
                'menu_id' => 3,
                'nama' => 'Bantuan',
                'url' => 'bantuan',
                'icon' => 'fa fa-heart',
                'warna' => '#11cdef',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            15 => 
            array (
                'id' => 16,
                'menu_id' => 4,
                'nama' => 'Kelola Surat',
                'url' => 'surat',
                'icon' => 'fas fa-file-alt',
                'warna' => '#f5365c',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            16 => 
            array (
                'id' => 17,
                'menu_id' => 4,
                'nama' => 'Kelola Surat Masuk',
                'url' => 'surat-masuk',
                'icon' => 'fas fa-file-import',
                'warna' => '#2bffc6',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            17 => 
            array (
                'id' => 18,
                'menu_id' => 4,
                'nama' => 'Kelola Surat Keluar',
                'url' => 'surat-keluar',
                'icon' => 'fas fa-file-export',
                'warna' => '#5e72e4',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            18 => 
            array (
                'id' => 19,
                'menu_id' => 4,
                'nama' => 'Pengaturan Surat',
                'url' => 'pengaturan-surat',
                'icon' => 'fas fa-cog',
                'warna' => '#2dce89',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            19 => 
            array (
                'id' => 20,
                'menu_id' => 4,
                'nama' => 'Kelola Produk Hukum',
                'url' => 'produk-hukum',
                'icon' => 'fa fa-book-reader',
                'warna' => '#f5365c',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            20 => 
            array (
                'id' => 21,
                'menu_id' => 4,
                'nama' => 'Kelola Inventaris',
                'url' => 'inventaris',
                'icon' => 'fa fa-cubes',
                'warna' => '#11cdef',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            21 => 
            array (
                'id' => 22,
                'menu_id' => 5,
                'nama' => 'Kelola APBDes',
                'url' => 'anggaran-realisasi',
                'icon' => 'fa fa-cubes',
                'warna' => '#fb6340',
                'created_at' => '2021-05-07 13:37:12',
                'updated_at' => '2021-05-07 13:37:12',
            ),
            22 => 
            array (
                'id' => 23,
                'menu_id' => 5,
                'nama' => 'Kelola Artikel',
                'url' => 'artikel',
                'icon' => 'fas fa-newspaper',
                'warna' => '#2dce89',
                'created_at' => '2022-01-18 11:06:35',
                'updated_at' => '2022-01-18 11:06:35',
            ),
            23 => 
            array (
                'id' => 24,
                'menu_id' => 5,
                'nama' => 'Kelola Gallery',
                'url' => 'kelola-gallery',
                'icon' => 'fas fa-images',
                'warna' => '#2bffc6',
                'created_at' => '2022-01-18 11:06:35',
                'updated_at' => '2022-01-18 11:06:35',
            ),
            24 => 
            array (
                'id' => 25,
                'menu_id' => 5,
                'nama' => 'Kelola Informasi',
                'url' => 'informasi',
                'icon' => 'fas fa-images',
                'warna' => '#fb6340',
                'created_at' => '2022-01-18 11:06:35',
                'updated_at' => '2022-01-18 11:06:35',
            ),
            25 => 
            array (
                'id' => 26,
                'menu_id' => 5,
                'nama' => 'Kelola Peran',
                'url' => 'peran',
                'icon' => 'fas fa-universal-access',
                'warna' => '#8965e0',
                'created_at' => '2022-01-18 11:06:35',
                'updated_at' => '2022-01-18 11:06:35',
            ),
            26 => 
            array (
                'id' => 27,
                'menu_id' => 5,
                'nama' => 'Kelola User',
                'url' => 'user',
                'icon' => 'fas fa-users',
                'warna' => '#2bffc6',
                'created_at' => '2022-01-18 11:06:35',
                'updated_at' => '2022-01-18 11:06:35',
            ),
            27 => 
            array (
                'id' => 28,
                'menu_id' => 5,
                'nama' => 'Kelola Database',
                'url' => 'database',
                'icon' => 'fas fa-database',
                'warna' => '#11cdef',
                'created_at' => '2022-01-18 11:06:35',
                'updated_at' => '2022-01-18 11:06:35',
            ),
            28 => 
            array (
                'id' => 29,
                'menu_id' => 6,
                'nama' => 'E-Surat',
                'url' => '#',
                'icon' => 'fas fa-file-alt',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            29 => 
            array (
                'id' => 30,
                'menu_id' => 6,
                'nama' => 'Pengaduan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            30 => 
            array (
                'id' => 31,
                'menu_id' => 7,
                'nama' => 'PKK',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            31 => 
            array (
                'id' => 32,
                'menu_id' => 7,
                'nama' => 'Lembaga Kemasyarakatan Desa',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            32 => 
            array (
                'id' => 33,
                'menu_id' => 7,
            'nama' => 'KIM (Kelompok Informasi Masyarakat)',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            33 => 
            array (
                'id' => 34,
                'menu_id' => 8,
                'nama' => 'Keagamaan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            34 => 
            array (
                'id' => 35,
                'menu_id' => 8,
                'nama' => 'Pendidikan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            35 => 
            array (
                'id' => 36,
                'menu_id' => 8,
                'nama' => 'Kesehatan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            36 => 
            array (
                'id' => 37,
                'menu_id' => 8,
                'nama' => 'Kesenian',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            37 => 
            array (
                'id' => 38,
                'menu_id' => 8,
                'nama' => 'Karangtaruna',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            38 => 
            array (
                'id' => 39,
                'menu_id' => 9,
                'nama' => 'Program Bantuan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            39 => 
            array (
                'id' => 40,
                'menu_id' => 10,
                'nama' => 'Kegiatan Pembangunan',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            40 => 
            array (
                'id' => 41,
                'menu_id' => 11,
                'nama' => 'Wisata Desa',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            41 => 
            array (
                'id' => 42,
                'menu_id' => 11,
                'nama' => 'UMKM',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            42 => 
            array (
                'id' => 43,
                'menu_id' => 11,
                'nama' => 'Kelompok Usaha',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            43 => 
            array (
                'id' => 44,
                'menu_id' => 12,
                'nama' => 'Apbdesa',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            44 => 
            array (
                'id' => 45,
                'menu_id' => 12,
                'nama' => 'PBB',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            45 => 
            array (
                'id' => 46,
                'menu_id' => 13,
                'nama' => 'Bumdesa Margomulyo',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            46 => 
            array (
                'id' => 47,
                'menu_id' => 14,
                'nama' => 'Warta Desa',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            47 => 
            array (
                'id' => 48,
                'menu_id' => 14,
                'nama' => 'Kegiatan Masyarakat',
                'url' => '#',
                'icon' => 'fas fa-users',
                'warna' => '#11cdef',
                'created_at' => '2022-01-27 11:46:25',
                'updated_at' => '2022-01-27 11:46:28',
            ),
            48 => 
            array (
                'id' => 49,
                'menu_id' => 5,
                'nama' => 'Kelola Menu',
                'url' => '#',
                'icon' => 'fas fa-cubes',
                'warna' => '#11cdef',
                'created_at' => '2022-02-01 11:38:35',
                'updated_at' => '2022-02-01 11:38:35',
            ),
        ));
        
        
    }
}