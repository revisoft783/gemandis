<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAgendaKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agenda_kegiatan', function (Blueprint $table) {
            $table->foreign(['bidang_id'], 'agenda_kegiatan_ibfk_1')->references(['id'])->on('bidang')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agenda_kegiatan', function (Blueprint $table) {
            $table->dropForeign('agenda_kegiatan_ibfk_1');
        });
    }
}
