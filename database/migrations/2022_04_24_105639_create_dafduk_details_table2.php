<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDafdukDetailsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('dafduk_details');
        Schema::create('dafduk_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_hdr')->nullable()->index('dafduk_details_id_hdr_foreign');
            $table->string('nama_lenkap', 100)->nullable();
            $table->string('gelar_dpn', 5)->nullable();
            $table->string('gelar_blkg', 5)->nullable();
            $table->string('nmr_paspor', 50)->nullable();
            $table->date('tgl_expired_paspor')->nullable();
            $table->string('nama_sponsor', 100)->nullable();
            $table->tinyInteger('tipe_sponsor')->nullable()->comment('1:Organisasi;2:Pemerintah;3:Perusahaan;4:Perorangan;5:Tanpa Sponsor');
            $table->string('alamat_sponsor', 100)->nullable();
            $table->tinyInteger('jns_kelamin')->nullable()->comment('1:Laki-laki;2:Perempuan');
            $table->string('tempat_lahir', 50)->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('kewarganegaraan', 5)->nullable()->default('WNI');
            $table->string('nomor_sk', 50)->nullable();
            $table->tinyInteger('akta_lahir')->nullable()->comment('1:Tidak Ada;2:Ada');
            $table->string('nomor_akta_lahir', 50)->nullable();
            $table->tinyInteger('gol_darah')->nullable()->comment('1:A;2:B;3:AB;4:O;5:A+;6:A-;7:B+;8:B-;9:AB+;10:AB-;11:O+;12:O-;13:Tidak Tahu');
            $table->tinyInteger('agama')->nullable()->comment('1:Islam;2:Kristen;3:Katolik;4:Hindu;5:Budha;6:Konghucu;7:Lainnya');
            $table->string('nama_organisasi_yme', 50)->nullable();
            $table->tinyInteger('status_perkawinan')->nullable()->comment('1:Belum Kawin;2:Kawin Tercatat;3:Kawin Belum Tercatat;4:Cerai Hidup Tercatat;5:Cerai Hidup belum tercatat;6:Cerai Mati');
            $table->tinyInteger('akta_perkawinan')->nullable()->comment('1:Tidak Ada;2:Ada');
            $table->string('nomor_akta_perkawinan', 50)->nullable();
            $table->date('tgl_perkawinan')->nullable();
            $table->tinyInteger('akta_cerai')->nullable()->comment('1:Tidak Ada;2:Ada');
            $table->string('nomor_akta_cerai', 50)->nullable();
            $table->date('tgl_cerai')->nullable();
            $table->tinyInteger('status_keluarga')->nullable()->comment('1:Kepala Keluarga;2:Suami;3:Istri;4:Anak;5:Menantu;6:Cucu;7:Orang Tua;8:Mertua;9:Famili:10:Lainnya');
            $table->tinyInteger('kelainan_fisik_mental')->nullable()->comment('1:Tidak Ada;2:Ada');
            $table->tinyInteger('penyandang_cacat')->nullable()->comment('1:Cacat Fisik;2:Cacat Netra;3:Cacat rungu/wicara;4:Cacat mental/jiwa;5:cacat mental dan fisik;6:lainnya');
            $table->unsignedBigInteger('pendidikan_id')->nullable()->index('dafduk_details_pendidikan_id_foreign');
            $table->unsignedBigInteger('pekerjaan_id')->nullable()->index('dafduk_details_pekerjaan_id_foreign');
            $table->string('nomor_itas_itap', 50)->nullable();
            $table->string('tempat_terbit_itas_itap', 50)->nullable();
            $table->date('tgl_itas_itap')->nullable();
            $table->date('tgl_itas_itap_expired')->nullable();
            $table->string('tempat_datang_pertama', 50)->nullable();
            $table->date('tgl_kedatangan_pertama')->nullable();
            $table->string('nik_ibu', 50)->nullable();
            $table->string('nama_ibu', 50)->nullable();
            $table->string('nik_ayah', 50)->nullable();
            $table->string('nama_ayah', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dafduk_details');
    }
}
