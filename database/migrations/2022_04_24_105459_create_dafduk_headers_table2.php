<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDafdukHeadersTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('dafduk_details');
        Schema::dropIfExists('dafduk_headers');

        Schema::create('dafduk_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trx')->nullable()->comment('untuk trx number');
            $table->string('no_kk')->nullable();
            $table->string('nama_kk')->nullable();
            $table->string('alamat_kk')->nullable();
            $table->string('email_kk')->nullable();
            $table->bigInteger('kd_profinsi')->nullable();
            $table->bigInteger('kd_kabupaten')->nullable();
            $table->bigInteger('kd_kelurahan')->nullable();
            $table->bigInteger('kd_kampung')->nullable();
            $table->string('alamat', 100)->nullable();
            $table->string('kota', 100)->nullable();
            $table->string('negara', 100)->nullable();
            $table->string('kode_pos_wn', 8)->nullable();
            $table->string('telp_wn', 20)->nullable();
            $table->tinyInteger('kd_negara')->nullable();
            $table->string('kd_kbri', 255)->nullable();
            $table->string('jml_anggota')->nullable()->comment('untuk jml anggota');
            $table->string('user_input', 100)->nullable();
            $table->string('user_update', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dafduk_headers');
    }
}
