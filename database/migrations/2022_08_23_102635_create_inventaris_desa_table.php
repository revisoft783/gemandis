<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventarisDesaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventaris_desa', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->integer('no_urut')->nullable();
            $table->string('no_barcode', 255)->nullable();
            $table->string('jenis_barang', 255)->nullable();
            $table->date('tgl_perolehan')->nullable();
            $table->bigInteger('asal_perolehan')->nullable()->index('asal_perolehan');
            $table->bigInteger('anggaran_id')->nullable()->index('anggaran_id');
            $table->string('foto', 255)->nullable();
            $table->tinyInteger('keadaan_barang_bangunan')->nullable()->comment('0: Baik; 1: Rusak');
            $table->string('word', 255)->nullable();
            $table->string('pdf', 255)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventaris_desa');
    }
}
