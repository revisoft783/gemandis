<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusyawarahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('musyawarah', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('no_urut')->nullable();
            $table->dateTime('tgl')->nullable();
            $table->bigInteger('jenis_musyawarah')->nullable()->index('jenis_musyawarah');
            $table->string('narasumber', 255)->nullable();
            $table->string('peserta', 255)->nullable();
            $table->text('isi_singkat')->nullable();
            $table->date('tgl_undangan')->nullable();
            $table->string('perihal', 255)->nullable();
            $table->string('peserta_undangan', 255)->nullable();
            $table->string('pdf_undangan', 255)->nullable();
            $table->string('foto', 255)->nullable();
            $table->string('notulen_word', 255)->nullable();
            $table->string('notulen_pdf', 255)->nullable();
            $table->string('daftar_hadir_word', 255)->nullable();
            $table->string('daftar_hadir_pdf', 255)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musyawarah');
    }
}
