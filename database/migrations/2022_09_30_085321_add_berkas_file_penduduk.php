<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBerkasFilePenduduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('penduduk', function (Blueprint $table) {
            $table->string('berkas_file')->nullable()->after('penyebab_meninggal');
            $table->string('berkas_file_kk')->nullable()->after('berkas_file');
            $table->string('berkas_file_ktp')->nullable()->after('berkas_file_kk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
