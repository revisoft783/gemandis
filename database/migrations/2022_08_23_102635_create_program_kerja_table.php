<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_kerja', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('no_urut')->nullable();
            $table->date('tgl')->nullable();
            $table->year('year')->nullable();
            $table->bigInteger('bidang')->nullable()->index('bidang');
            $table->string('program_kerja', 255)->nullable();
            $table->longText('uraian_program')->nullable();
            $table->string('sasaran', 255)->nullable();
            $table->string('indikator', 255)->nullable();
            $table->string('tujuan', 255)->nullable();
            $table->string('sasaran_kebijakan', 255)->nullable();
            $table->string('capaian_program', 255)->nullable();
            $table->text('keterangan')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_kerja');
    }
}
