<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKotaKecamatanKelurahanAlamatDatangPenduduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('penduduk', function (Blueprint $table) {
            $table->string('kota_dari')->nullable()->default(null)->after('tgl_mutasi');
            $table->string('kecamatan_dari')->nullable()->default(null)->after('kota_dari');
            $table->string('kelurahan_dari')->nullable()->default(null)->after('kecamatan_dari');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
