<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKepKepdesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kep_kepdes', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->integer('no_urut')->nullable();
            $table->string('nomor_kepdes', 255)->nullable();
            $table->date('tgl_kepdes')->nullable();
            $table->string('tentang', 255)->nullable();
            $table->text('uraian')->nullable();
            $table->string('nomor_lapor', 255)->nullable();
            $table->date('tgl_lapor')->nullable();
            $table->longText('keterangan')->nullable();
            $table->string('document_pdf', 255)->nullable();
            $table->string('document_word', 255)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();

            $table->index(['nomor_kepdes', 'tgl_kepdes'], 'idx_nomor_tgl_kepdes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kep_kepdes');
    }
}
