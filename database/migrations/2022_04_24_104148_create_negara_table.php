<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNegaraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('negara');
        Schema::create('negara', function (Blueprint $table) {
            $table->integer('id_country', true);
            $table->string('negara_name', 255);
            $table->string('code1', 10);
            $table->string('code2', 10);
            $table->string('flag', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('negara');
    }
}
