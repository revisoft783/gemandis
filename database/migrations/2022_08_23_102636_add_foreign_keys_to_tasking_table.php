<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTaskingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasking', function (Blueprint $table) {
            $table->foreign(['bidang_id'], 'tasking_ibfk_1')->references(['id'])->on('bidang')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasking', function (Blueprint $table) {
            $table->dropForeign('tasking_ibfk_1');
        });
    }
}
