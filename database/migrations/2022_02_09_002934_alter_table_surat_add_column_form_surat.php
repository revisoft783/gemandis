<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSuratAddColumnFormSurat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('surat', function (Blueprint $table) {
            $table->string('form_surat')->nullable()->unique()->after('deskripsi')->comment('untuk membedakan bentuk print-out surat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('surat', function (Blueprint $table) {
            $table->dropColumn('form_surat');
        });
    }
}
