<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTamuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku_tamu', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('no_urut')->nullable();
            $table->date('tgl_tamu')->nullable();
            $table->year('years')->nullable();
            $table->tinyInteger('jns_tamu')->nullable()->comment('0: tamu khusus; 1: tamu umum');
            $table->string('nama', 255)->nullable();
            $table->string('nik', 255)->nullable();
            $table->string('alamat', 255)->nullable();
            $table->string('instansi', 255)->nullable();
            $table->bigInteger('tujuan_id')->index('tujuan_id');
            $table->text('keperluan')->nullable();
            $table->time('jam_datang')->nullable();
            $table->string('foto', 255)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku_tamu');
    }
}
