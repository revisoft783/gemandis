<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTagCardIdEktpReaders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ktp_reader', function (Blueprint $table) {
            $table->string('tag_card_id')->nullable()->default(null);
        });

        Schema::table('penduduk', function (Blueprint $table) {
            $table->string('tag_card_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
