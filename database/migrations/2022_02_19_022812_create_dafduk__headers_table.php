<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDafdukHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dafduk_headers', function (Blueprint $table) {
            $table->id();
            $table->string('no_kk')->nullable();
            $table->string('nama_kk')->nullable();
            $table->string('alamat_kk')->nullable();
            $table->string('email_kk')->nullable();
            $table->tinyInteger('kd_profinsi')->nullable();
            $table->tinyInteger('kd_kabupaten')->nullable();
            $table->tinyInteger('kd_kelurahan')->nullable();
            $table->tinyInteger('kd_kampung')->nullable();
            $table->string('alamat', 100)->nullable()->default(null);
            $table->string('kota', 100)->nullable()->default(null);
            $table->string('negara', 100)->nullable()->default(null);
            $table->string('kode_pos_wn', 8)->nullable()->default(null);
            $table->string('telp_wn', 20)->nullable()->default(null);
            $table->tinyInteger('kd_negara')->nullable();
            $table->tinyInteger('kd_kbri')->nullable();
            $table->string('user_input', 100)->nullable()->default(null);
            $table->string('user_update', 100)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dafduk_headers');
    }
}
