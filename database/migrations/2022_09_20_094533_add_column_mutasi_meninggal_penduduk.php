<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMutasiMeninggalPenduduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('penduduk', function (Blueprint $table) {
            $table->date('tgl_pindah')->nullable();
            $table->string('kota_tujuan')->nullable();
            $table->string('kecamatan_tujuan')->nullable();
            $table->string('kelurahan_tujuan')->nullable();
            $table->string('alamat_tujuan')->nullable();
            $table->string('tgl_mutasi')->nullable();
            $table->date('tgl_meninggal')->nullable();
            $table->time('jam_meninggal')->nullable();
            $table->string('tempat_meninggal')->nullable();
            $table->string('penyebab_meninggal')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updatedfd_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
