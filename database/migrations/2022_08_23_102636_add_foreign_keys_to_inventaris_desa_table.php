<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToInventarisDesaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventaris_desa', function (Blueprint $table) {
            $table->foreign(['asal_perolehan'], 'inventaris_desa_ibfk_1')->references(['id'])->on('jenis_perolehan')->onUpdate('CASCADE');
            $table->foreign(['anggaran_id'], 'inventaris_desa_ibfk_2')->references(['id'])->on('anggaran')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventaris_desa', function (Blueprint $table) {
            $table->dropForeign('inventaris_desa_ibfk_1');
            $table->dropForeign('inventaris_desa_ibfk_2');
        });
    }
}
