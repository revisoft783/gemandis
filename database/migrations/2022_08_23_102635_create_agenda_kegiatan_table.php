<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_kegiatan', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('no_urut')->nullable();
            $table->date('tgl')->nullable();
            $table->bigInteger('bidang_id')->nullable()->index('bidang_id');
            $table->tinyInteger('jenis_kegiatan')->nullable()->comment('0: Dinas; 1: Mandiri');
            $table->longText('uraian_kegiatan')->nullable();
            $table->string('peserta', 255)->nullable();
            $table->dateTime('tgl_kegiatan')->nullable();
            $table->longText('catatan_kegiatan')->nullable();
            $table->string('laporan_kegiatan', 255)->nullable();
            $table->string('documentasi', 255)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_kegiatan');
    }
}
