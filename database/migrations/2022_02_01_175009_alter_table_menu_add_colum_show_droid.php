<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableMenuAddColumShowDroid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu', function (Blueprint $table) {
            $table->string('key_nama')->nullable()->after('nama');
            $table->string('show_droid')->nullable()->default(0)->after('key_nama')->comment('Kolom ini digunakan sebagai penanda menu ini muncul di gemandis droid, 0 = tidak tampil | 1 = tampil');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('key_nama');
            $table->dropColumn('show_droid');
        });
    }
}
