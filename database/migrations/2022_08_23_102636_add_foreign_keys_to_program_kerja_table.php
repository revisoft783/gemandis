<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProgramKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_kerja', function (Blueprint $table) {
            $table->foreign(['bidang'], 'program_kerja_ibfk_1')->references(['id'])->on('bidang')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_kerja', function (Blueprint $table) {
            $table->dropForeign('program_kerja_ibfk_1');
        });
    }
}
