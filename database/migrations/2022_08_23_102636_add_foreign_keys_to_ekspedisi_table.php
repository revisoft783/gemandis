<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToEkspedisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ekspedisi', function (Blueprint $table) {
            $table->foreign(['id_agenda'], 'ekspedisi_ibfk_1')->references(['id'])->on('agenda_surat')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ekspedisi', function (Blueprint $table) {
            $table->dropForeign('ekspedisi_ibfk_1');
        });
    }
}
