<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeraturanDesaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peraturan_desa', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('no_urut')->nullable();
            $table->smallInteger('jns_peraturan')->nullable()->default(0)->index('idx_jns_peraturan')->comment('0: peraturan desa; 1: peraturan bersama; 2: peraturan kepala desa');
            $table->string('nomor_penetapan', 255)->nullable();
            $table->date('tgl_penetapan')->nullable();
            $table->string('tentang', 255)->nullable();
            $table->text('uraian')->nullable();
            $table->date('tgl_kesepakatan')->nullable()->comment('Diisi Tanggal, Bulan, dan Tahun dari kesepakatan pemerintah desa');
            $table->string('nomor_dilaporkan', 255)->nullable();
            $table->date('tgl_dilaporkan')->nullable();
            $table->string('nomor_udld', 255)->nullable()->comment('Diisi dengan nomor sesuai dengan diundangkannya');
            $table->date('tgl_udld')->nullable()->comment('Diisi dengan tanggal sesuai dengan diundangkannya');
            $table->string('nomor_udbd', 255)->nullable()->comment('Diisi dengan nomor sesuai dengan diundangkannya');
            $table->date('tgl_udbd')->nullable()->comment('Diisi dengan tanggal sesuai dengan diundangkannya');
            $table->longText('keterangan')->nullable();
            $table->string('document_word', 255)->nullable();
            $table->string('document_pdf', 255)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();

            $table->index(['jns_peraturan', 'tentang'], 'idx_jns_tentang');
            $table->index(['document_word', 'document_pdf'], 'idx_document');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peraturan_desa');
    }
}
