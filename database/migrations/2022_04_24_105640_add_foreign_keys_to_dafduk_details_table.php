<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDafdukDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dafduk_details', function (Blueprint $table) {
            $table->foreign(['id_hdr'])->references(['id'])->on('dafduk_headers')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['pekerjaan_id'])->references(['id'])->on('pekerjaan')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['pendidikan_id'])->references(['id'])->on('pendidikan')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dafduk_details', function (Blueprint $table) {
            $table->dropForeign('dafduk_details_id_hdr_foreign');
            $table->dropForeign('dafduk_details_pekerjaan_id_foreign');
            $table->dropForeign('dafduk_details_pendidikan_id_foreign');
        });
    }
}
