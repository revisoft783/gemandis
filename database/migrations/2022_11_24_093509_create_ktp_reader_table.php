<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKtpReaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ktp_reader', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('nik')->nullable()->unique('nik');
            $table->string('nama')->nullable();
            $table->string('ttl')->nullable();
            $table->date('tgllhr')->nullable();
            $table->string('gdr')->nullable();
            $table->string('goldar', 5)->nullable();
            $table->string('almt')->nullable();
            $table->string('rt', 5)->nullable();
            $table->string('rw', 5)->nullable();
            $table->string('kec', 50)->nullable();
            $table->string('kel', 50)->nullable();
            $table->string('agm')->nullable();
            $table->string('mrtl')->nullable();
            $table->string('pkj')->nullable();
            $table->string('prv')->nullable();
            $table->string('kab')->nullable();
            $table->string('kwgn')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ktp_reader');
    }
}
