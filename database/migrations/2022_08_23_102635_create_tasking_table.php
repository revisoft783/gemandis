<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasking', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('no_urut')->nullable();
            $table->date('tgl_pelimpahan')->nullable();
            $table->longText('uraian')->nullable();
            $table->date('tgl_penyelesaian')->nullable();
            $table->longText('tujuan')->nullable();
            $table->bigInteger('bidang_id')->index('bidang_id');
            $table->integer('disposisi_id')->nullable();
            $table->string('document_word', 255)->nullable();
            $table->string('document_pdf', 255)->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();

            $table->index(['tgl_pelimpahan', 'disposisi_id'], 'idx_uraian_tgl_pelimpahan_tujuan_disposisi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasking');
    }
}
