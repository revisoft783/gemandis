<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaSuratTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_surat', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->integer('no_urut')->nullable();
            $table->date('tgl_surat_kirim')->nullable();
            $table->string('no_surat')->nullable();
            $table->date('tgl_surat_keluar')->nullable();
            $table->string('penerima')->nullable();
            $table->text('isi_singkat')->nullable();
            $table->longText('keterangan')->nullable();
            $table->string('word')->nullable();
            $table->string('pdf')->nullable();
            $table->tinyInteger('status')->nullable()->default(0)->comment('0: surat keluar; 1: surat masuk');
            $table->bigInteger('created_by')->nullable();
            $table->timestamp('created_at')->nullable()->useCurrent();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_surat');
    }
}
