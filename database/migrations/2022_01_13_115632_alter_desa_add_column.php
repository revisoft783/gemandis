<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDesaAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('desa', function (Blueprint $table) {
            $table->string('label_header')->nullable();
            $table->string('logo_header')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('desa', function (Blueprint $table) {
            $table->dropColumn('label_header');
            $table->dropColumn('logo_header');
        });
    }
}

