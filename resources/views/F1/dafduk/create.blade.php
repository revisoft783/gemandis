@extends('layouts.layout')

@section('title', 'Tambah Formulir F.1.01')

@section('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
    overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
    font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg .tg-9wq8{border-color:inherit;text-align:center;vertical-align:middle}
    .tg .tg-baqh{text-align:center;vertical-align:top}
    /* .modal{
        display: block !important;
    }
    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height: 400px;
        overflow-y: auto;
    } */
</style>
@endsection

@section('content-header')
<div class="container">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Tambah Surat Keluar</h2>
                                <p class="mb-0 text-sm">Kelola Surat Keluar</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ route("surat-keluar.index") }}?page={{ request('page') }}" class="btn btn-success" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card bg-white shadow h-100">
    <div class="card-header">
        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
            <div class="mb-3">
                <h2 class="mb-0">FORMULIR BIODATA KELUARGA</h2>
                <p class="mb-0 text-sm">Kelola Form F-1.0.1</p>
            </div>
        </div>
    </div>
    <div class="card-body">
        <ul class="nav nav-tabs flex-nowrap" role="tablist">
            <li role="presentation" class="nav-item">
                <a href="#step1" class="nav-link active" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">Step 1 </a>
            </li>
            <li role="presentation" class="nav-item">
                <a href="#step2" class="nav-link disabled" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">Step 2 </a>
            </li>
            <li role="presentation" class="nav-item">
                <a href="#step3" class="nav-link disabled" data-toggle="tab" aria-controls="Complete" role="tab" title="Complete">Step 3 </a>
            </li>
        </ul>
        <form autocomplete="off" action="{{ route('surat-keluar.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            {!! Form::hidden("id_dafduk", $id_dafduk, ['class'=>"form-control id_dafduk"]) !!}
            <div class="tab-content py-2">
                <div class="tab-pane active" role="tabpanel" id="step1">
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-12">Data Kepala Keluarga</label>
                    </div>
                    <hr/>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="nama_kepala">Nama Kepala Keluarga/Name of head of the family</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control @error('nama_kepala') is-invalid @enderror nama_kepala" name="nama_kepala" placeholder="Masukkan Nama Kepala Keluarga...">
                            @error('nama_kepala')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="alamat">Alamat/address</label>
                        <div class="col-md-9">
                            <input type="text"class="form-control @error('alamat') is-invalid @enderror alamat" name="alamat" placeholder="Masukkan Alamat ..." >
                            @error('alamat')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="kode_pos">Kode Pos/Post Code</label>
                        <div class="col-md-3">
                            <input type="text"class="form-control @error('kode_pos') is-invalid @enderror kode_pos" name="kode_pos" placeholder="Masukkan Kode Pos ..." >
                            @error('kode_pos')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>

                        <label class="form-control-label col-form-label col-md-1" for="email">Email</label>
                        <div class="col-md-3">
                            <input type="email" class="form-control @error('email') is-invalid @enderror email" name="email" placeholder="Masukkan Email ...">
                            @error('email')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-12">Data Wilayah</label>
                    </div>
                    <hr/>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="provinsi_id">Kode-Nama Provinsi/Code-Provinsi</label>
                        <div class="col-md-9">
                            {!! Form::select("provinsi_id", getProvincies(), null, ['class' => 'form-control select2 mr-3 col-4', 'id' => "provinsi_id"]) !!}
                            @error('provinsi_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="kabupaten_id">Kode-Nama Kabupaten</label>
                        <div class="col-md-9">
                            <select class="form-control kabupaten_id @error('kabupaten_id') is-invalid @enderror mr-3 col-4" name="kabupaten_id" id="kabupaten_id">
                                <option value="">Pilih Kabupaten</option>
                            </select>
                            @error('kabupaten_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="kecamatan_id">Kode-Nama Kecamatan</label>
                        <div class="col-md-9">
                            <select class="form-control kecamatan_id @error('kecamatan_id') is-invalid @enderror mr-3 col-4" name="kecamatan_id" id="kecamatan_id">
                                <option value="">Pilih Kecamatan</option>
                            </select>
                            @error('kecamatan_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="desa_id">Nama Dusun</label>
                        <div class="col-md-9">
                            <select class="form-control desa_id @error('desa_id') is-invalid @enderror mr-3 col-4" name="desa_id" id="desa_id">
                                <option value="">Pilih Desa</option>
                            </select>
                            @error('desa_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-12">Alamat di Luar Negeri (diisi oleh WNI di Luar Negeri)</label>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="alamat_ln">Alamat</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control @error('alamat_ln') is-invalid @enderror alamat_ln" name="alamat_ln" id="alamat_ln" placeholder="Masukkan Alamat di Luar Negeri ..." value="{{ old('alamat_ln') }}">
                            @error('alamat_ln')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="kota">Kota</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control @error('kota') is-invalid @enderror kota" name="kota" id="kota" placeholder="Masukkan Kota ..." value="{{ old('kota') }}">
                            @error('kota')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <label class="form-control-label col-form-label col-md-3" for="provinsi_ln">Provinsi/Negara Bagian</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control @error('provinsi_ln') is-invalid @enderror provinsi_ln" name="provinsi_ln" id="provinsi_ln" placeholder="Masukkan Provinsi Luar Negeri ..." value="{{ old('provinsi_ln') }}">
                            @error('provinsi_ln')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>

                        <label class="form-control-label col-form-label mt-1 col-md-3" for="negara_ln">Negara</label>
                        <div class="col-md-3 mt-1">
                            <input type="text" class="form-control @error('negara_ln') is-invalid @enderror negara_ln" name="negara_ln" id="negara_ln" placeholder="Masukkan Negera ..." value="{{ old('negara_ln') }}">
                            @error('negara_ln')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>

                        <label class="form-control-label col-form-label mt-1 col-md-3" for="telp">Telephone/Handphone</label>
                        <div class="col-md-3 mt-1">
                            <input type="text" class="form-control @error('telp') is-invalid @enderror telp" id="telp" name="telp" placeholder="Masukkan No. Telephone..." value="{{ old('negara_ln') }}">
                            @error('telp')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>

                        <label class="form-control-label col-form-label col-md-3" for="email">Email</label>
                        <div class="col-md-3 mt-1">
                            <input type="email" class="form-control @error('email') is-invalid @enderror email" id="email" name="email" placeholder="Masukkan Email ..." value="{{ old('email') }}">
                            @error('email')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>

                        <label class="form-control-label col-form-label col-md-3" for="jml_anggota">Jumlah Anggota Keluarga</label>
                        <div class="col-md-3 mt-1">
                            <input type="number" class="form-control @error('jml_anggota') is-invalid @enderror jml_anggota" id="jml_anggota" name="jml_anggota" placeholder="Masukkan Jumlah Anggota keluarga ..." value="{{ old('jml_anggota') }}">
                            @error('jml_anggota')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-12">Diisi oleh petugas</label>
                    </div>
                    <hr/>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="kode_negara">Kode - Nama Negara</label>
                        <div class="col-md-9">
                            {!! Form::select("kode_negara", getNegara(), null, ['class' => 'form-control select2 mr-3 col-4', 'id' => "kode_negara"]) !!}
                            @error('kode_negara')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="form-control-label col-form-label col-md-3" for="kode_perwakilan">Kode - Nama Perwakilan RI</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control @error('kode_perwakilan') is-invalid @enderror kode_perwakilan" id="kode_perwakilan" name="kode_perwakilan" placeholder="Masukkan Perwakilan RI ..." value="{{ old('pewakilan_ri') }}">
                            @error('kode_perwakilan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary next-step float-right">Next</button>
                </div>
                <div class="tab-pane" role="tabpanel" id="step2">
                    <h3>Data Anggota Keluarga</h3>
                    <p>Catatan:</p>
                    <ul>
                        <li>Bagi Penduduk WNI mengisi kolom 2 s/d 6, 10 s/d 31, 38 s/d 41</li>
                        <li>For Foreighner only, please fill coloum 2 to 13, 15 to 41</li>
                        <li>bagi WNI di luar wilayah NKRI mengisi nomor 2 s/d 31, 38 s/d 41</li>
                    </ul>
                    <div class="row">
                        <div class="col-12">
                            <a href="#add-penduduk" data-toggle="modal" class="btn btn-success float-right mb-2 save" title="Tambah"><i class="fas fa-plus"></i> Tambah Anggota</a>
                        </div>
                        <div class="col-12">
                            <table class="tg table table-hover table-sm table-striped table-bordered table-responsive table-1" width="100%">
                                <colgroup>
                                    <col style="width: 28px">
                                    <col style="width: 106px">
                                    <col style="width: 53px">
                                    <col style="width: 69px">
                                    <col style="width: 120px">
                                    <col style="width: 149px">
                                    <col style="width: 105px">
                                    <col style="width: 113px">
                                    <col style="width: 117px">
                                    <col style="width: 99px">
                                    <col style="width: 94px">
                                    <col style="width: 184px">
                                    <col style="width: 126px">
                                    <col style="width: 109px">
                                    <col style="width: 74px">
                                    <col style="width: 148px">
                                    <col style="width: 96px">
                                    <col style="width: 62px">
                                    <col style="width: 169px">
                                    <col style="width: 128px">
                                    <col style="width: 116px">
                                    <col style="width: 164px">
                                    <col style="width: 137px">
                                    <col style="width: 76px">
                                    <col style="width: 156px">
                                    <col style="width: 131px">
                                    <col style="width: 222px">
                                    <col style="width: 157px">
                                    <col style="width: 130px">
                                    <col style="width: 134px">
                                    <col style="width: 110px">
                                    <col style="width: 121px">
                                    <col style="width: 163px">
                                    <col style="width: 166px">
                                    <col style="width: 163px">
                                    <col style="width: 163px">
                                    <col style="width: 196px">
                                    <col style="width: 58px">
                                    <col style="width: 71px">
                                    <col style="width: 66px">
                                    <col style="width: 83px">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="tg-9wq8">No</th>
                                        <th class="tg-9wq8">Nama Lengkap</th>
                                        <th class="tg-9wq8" colspan="2">Gelar</th>
                                        <th class="tg-9wq8">Nomor Paspor</th>
                                        <th class="tg-9wq8">Tgl. Berakhir Passport</th>
                                        <th class="tg-9wq8">Nama Sponsor</th>
                                        <th class="tg-9wq8">Tipe Sponsor</th>
                                        <th class="tg-9wq8">Alamat Sponsor</th>
                                        <th class="tg-9wq8">Jenis Kelamin</th>
                                        <th class="tg-9wq8">Tempat Lahir</th>
                                        <th class="tg-9wq8">Tanggal, Bulan, Tahun Lahir</th>
                                        <th class="tg-9wq8">Kewarganegaraan</th>
                                        <th class="tg-9wq8">Nomor SK</th>
                                        <th class="tg-9wq8" rowspan="2">Akta Lahir</th>
                                        <th class="tg-9wq8" rowspan="2">Nomor Akta Kelahiran</th>
                                        <th class="tg-9wq8">Gol. Darah</th>
                                        <th class="tg-9wq8">Agama</th>
                                        <th class="tg-9wq8" rowspan="2">Nama Organisasi Kepercayaan Terhadap Tuhan YME</th>
                                        <th class="tg-9wq8">Status Perkawinan</th>
                                        <th class="tg-9wq8" rowspan="2">Akta Perkawinan</th>
                                        <th class="tg-9wq8" rowspan="2">Nomor AKta Perkawinan</th>
                                        <th class="tg-9wq8" rowspan="2">Tanggal Perkawinan</th>
                                        <th class="tg-9wq8" rowspan="2">Akta Cerai</th>
                                        <th class="tg-9wq8" rowspan="2">Nomor Akta Perceraian</th>
                                        <th class="tg-9wq8" rowspan="2">Tanggal Perceraian</th>
                                        <th class="tg-9wq8" rowspan="2">Status Hubungan dalam Keluarga</th>
                                        <th class="tg-9wq8" rowspan="2">Kelainan Fisik &amp; Mental</th>
                                        <th class="tg-9wq8" rowspan="2">Penyandang Cacat</th>
                                        <th class="tg-9wq8" rowspan="2">Pendidikan Terakhir</th>
                                        <th class="tg-9wq8" rowspan="2">Jenis Pekerjaan</th>
                                        <th class="tg-9wq8" rowspan="2">Nomor ITAS/ITAP</th>
                                        <th class="tg-9wq8" rowspan="2">Tempat Terbit ITAS/ITAP</th>
                                        <th class="tg-9wq8" rowspan="2">Tanggal Terbit ITAS/ITAP</th>
                                        <th class="tg-9wq8" rowspan="2">Tanggal Akhir ITAS/ITAP</th>
                                        <th class="tg-9wq8" rowspan="2">Tempat Datang Pertama</th>
                                        <th class="tg-9wq8" rowspan="2">Tanggal Kedatangan Pertama</th>
                                        <th class="tg-9wq8" rowspan="2">NIK Ibu</th>
                                        <th class="tg-9wq8" rowspan="2">Nama Ibu</th>
                                        <th class="tg-9wq8" rowspan="2">Nik Ayah</th>
                                        <th class="tg-9wq8" rowspan="2">Nama Ayah</th>
                                    </tr>
                                    <tr>
                                        <th class="tg-9wq8">No</th>
                                        <th class="tg-9wq8">Full Name</th>
                                        <th class="tg-9wq8">Depan</th>
                                        <th class="tg-9wq8">Belakang</th>
                                        <th class="tg-9wq8">Passport Number</th>
                                        <th class="tg-9wq8">Date of Expiry</th>
                                        <th class="tg-9wq8">Sponsor Name</th>
                                        <th class="tg-9wq8">Type of Sponsor</th>
                                        <th class="tg-9wq8">Sponsor Address</th>
                                        <th class="tg-9wq8">Sex</th>
                                        <th class="tg-9wq8">Place of Birth</th>
                                        <th class="tg-9wq8">Date of Birth</th>
                                        <th class="tg-9wq8">Nationality</th>
                                        <th class="tg-9wq8">Penetapan WNI</th>
                                        <th class="tg-9wq8">Type of Blood</th>
                                        <th class="tg-9wq8">Religion</th>
                                        <th class="tg-9wq8">Marital Status</th>
                                    </tr>
                                    <tr>
                                        <td class="tg-9wq8">1</td>
                                        <td class="tg-9wq8">2</td>
                                        <td class="tg-9wq8">3</td>
                                        <td class="tg-9wq8">4</td>
                                        <td class="tg-9wq8">5</td>
                                        <td class="tg-9wq8">6</td>
                                        <td class="tg-9wq8">7</td>
                                        <td class="tg-9wq8">8</td>
                                        <td class="tg-9wq8">9</td>
                                        <td class="tg-9wq8">10</td>
                                        <td class="tg-9wq8">11</td>
                                        <td class="tg-9wq8">12</td>
                                        <td class="tg-9wq8">13</td>
                                        <td class="tg-9wq8">14</td>
                                        <td class="tg-9wq8">15</td>
                                        <td class="tg-9wq8">16</td>
                                        <td class="tg-9wq8">17</td>
                                        <td class="tg-9wq8">18</td>
                                        <td class="tg-9wq8">19</td>
                                        <td class="tg-9wq8">20</td>
                                        <td class="tg-9wq8">21</td>
                                        <td class="tg-9wq8">22</td>
                                        <td class="tg-9wq8">23</td>
                                        <td class="tg-9wq8">24</td>
                                        <td class="tg-9wq8">25</td>
                                        <td class="tg-9wq8">26</td>
                                        <td class="tg-9wq8">27</td>
                                        <td class="tg-9wq8">28</td>
                                        <td class="tg-9wq8">29</td>
                                        <td class="tg-9wq8">30</td>
                                        <td class="tg-9wq8">31</td>
                                        <td class="tg-9wq8">32</td>
                                        <td class="tg-9wq8">33</td>
                                        <td class="tg-9wq8">34</td>
                                        <td class="tg-9wq8">35</td>
                                        <td class="tg-9wq8">36</td>
                                        <td class="tg-9wq8">37</td>
                                        <td class="tg-9wq8">38</td>
                                        <td class="tg-9wq8">39</td>
                                        <td class="tg-9wq8">40</td>
                                        <td class="tg-9wq8">41</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="tg-9wq8" colspan="41">Empty Recods</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <ul class="float-right">
                        <li class="list-inline-item">
                            <button type="button" class="btn btn-outline-primary prev-step">Previous</button>
                        </li>
                        <li class="list-inline-item">
                            <button type="button" class="btn btn-primary next-step">Save and continue</button>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="complete">
                    <h3>Complete!</h3>
                    <p>You have successfully completed all steps.</p>
                </div>
                <div class="clearfix"></div>
            </div>
            @csrf
        </form>
    </div>
</div>

<div class="modal fade" id="add-penduduk" tabindex="-1" role="dialog" aria-labelledby="add-penduduk" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-title-pengaturan">Add Anggota</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <form action="{{ route("buat-dafduk-detail") }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {!! Form::hidden("id_dafduk", $id_dafduk, ['class'=>"form-control id_dafduk"]) !!}
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12">
                            <h6 class="mb-0">Silahkan pilih jenis penduduk</h6>
                        </div>
                        <div class="form-group col-lg-12 col-md-12">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="checkbox" id="customRadioInline1" name="jenis-penduduk" class="custom-control-input" value="1">
                                <label class="custom-control-label" for="customRadioInline1">WNI</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="checkbox" id="customRadioInline2" name="jenis-penduduk" class="custom-control-input" value="2">
                                <label class="custom-control-label" for="customRadioInline2">WNA</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="checkbox" id="customRadioInline3" name="jenis-penduduk" class="custom-control-input" value="3">
                                <label class="custom-control-label" for="customRadioInline3">WNI di Luar Negeri</label>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="form-group col-lg-12 col-md-12">
                            <label class="form-control-label" for="nama_lenkap">Nama Lengkap (2)</label>
                            <input type="text" id="nama_lenkap" class="form-control @error('nama_lenkap') is-invalid @enderror" name="nama_lenkap" placeholder="Masukkan Nama ..." value="{{ old('nama_lenkap') }}" disabled>
                            @error('nama')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 col-md-12">
                            <label class="form-control-label" for="gelar_dpn">Gelar Depan (3)</label>
                            <input type="text" id="gelar-dpn" class="form-control @error('gelar_dpn') is-invalid @enderror" name="gelar_dpn" placeholder="Masukkan Gelar Depan ..." value="{{ old('gelar_dpn') }}" disabled>
                            @error('gelar_dpn')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-6 col-md-12">
                            <label class="form-control-label" for="gelar_dpn">Gelar Belakang (4)</label>
                            <input type="text" id="gelar_blkg" class="form-control @error('gelar_blkg') is-invalid @enderror" name="gelar_blkg" placeholder="Masukkan Gelar Belakang ..." value="{{ old('gelar-belakang') }}" disabled>
                            @error('gelar_blkg')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 col-md-12">
                            <label class="form-control-label" for="nmr_paspor">Nomor Pasport (5)</label>
                            <input type="text" id="nmr_paspor" class="form-control @error('nmr_paspor') is-invalid @enderror" name="nmr_paspor" placeholder="Masukkan Nomor Paspor ..." value="{{ old('nmr_paspor') }}" disabled>
                            @error('nmr_paspor')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label class="form-control-label" for="tgl_expired_paspor">Tanggal Berakhir Pasport (6)</label>
                            <input type="date" id="tgl_expired_paspor" class="form-control @error('tgl_expired_paspor') is-invalid @enderror" name="tgl_expired_paspor" placeholder="Masukkan Tanggal Passport ..." value="{{ old('tgl_expired_paspor') }}" disabled>
                            @error('tgl_expired_paspor')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-12">
                            <label class="form-control-label" for="nama_sponsor">Nama Sponsor (7)</label>
                            <input type="text" id="nama_sponsor" class="form-control @error('nama_sponsor') is-invalid @enderror" name="nama_sponsor" placeholder="Masukkan Nama Sponsor ..." value="{{ old('nama_sponsor') }}" disabled>
                            @error('nama_sponsor')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="tipe_sponsor">Tipe Sponsor (8)</label>
                            <select class="form-control select2 @error('tipe_sponsor') is-invalid @enderror" name="tipe_sponsor" id="tipe_sponsor" disabled>
                                <option selected value="">Pilih Tipe Sponsor</option>
                                <option value="1" {{ old('tipe_sponsor') == 1 ? 'selected="true"' : ''  }}>Organisasi Internasional</option>
                                <option value="2" {{ old('tipe_sponsor') == 2 ? 'selected="true"' : ''  }}>Pemerintah</option>
                                <option value="3" {{ old('tipe_sponsor') == 3 ? 'selected="true"' : ''  }}>Perusahaan</option>
                                <option value="4" {{ old('tipe_sponsor') == 4 ? 'selected="true"' : ''  }}>Perorangan</option>
                                <option value="5" {{ old('tipe_sponsor') == 5 ? 'selected="true"' : ''  }}>Tanpa Sponsor</option>
                            </select>
                            @error('tipe_sponsor')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-5 col-md-12">
                            <label class="form-control-label" for="alamat_sponsor">Alamat Sponsor (9)</label>
                            <input type="text" id="alamat_sponsor" class="form-control @error('alamat_sponsor') is-invalid @enderror" name="alamat_sponsor" placeholder="Masukkan Alamat Sponsor ..." value="{{ old('alamat_sponsor') }}" disabled>
                            @error('alamat_sponsor')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label" for="jns_kelamin">Jenis Kelamin (10)</label>
                            <select class="form-control select2 @error('jns_kelamin') is-invalid @enderror" name="jns_kelamin" id="jns_kelamin" disabled>
                                <option selected value="">Pilih Jenis Kelamin</option>
                                <option value="1" {{ old('jns_kelamin') == 1 ? 'selected="true"' : ''  }}>Laki-laki</option>
                                <option value="2" {{ old('jns_kelamin') == 2 ? 'selected="true"' : ''  }}>Perempuan</option>
                            </select>
                            @error('jns_kelamin')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label" for="tempat_lahir">Tempat Lahir (11)</label>
                            <input type="text" id="tempat_lahir" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" placeholder="Masukkan Tempat Lahir ..." value="{{ old('tempat_lahir') }}" disabled>
                            @error('tempat_lahir')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label" for="tgl_lahir">Tanggal Lahir (12)</label>
                            <input type="date" id="tgl_lahir" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" placeholder="Masukkan Tanggal Lahir ..." value="{{ old('tgl_lahir') }}" disabled>
                            @error('tgl_lahir')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="kewarganegaraan">Kewarganegaraan (13)</label>
                            <select class="form-control select2 @error('kewarganegaraan') is-invalid @enderror" name="kewarganegaraan" id="kewarganegaraan" disabled>
                                <option selected value="">Pilih Kewarganegaraan</option>
                                <option value="1" {{ old('kewarganegaraan') == 1 ? 'selected="true"' : ''  }}>WNI</option>
                                <option value="2" {{ old('kewarganegaraan') == 2 ? 'selected="true"' : ''  }}>WNA</option>
                                <option value="3" {{ old('kewarganegaraan') == 3 ? 'selected="true"' : ''  }}>Dua Kewarganegaraan</option>
                            </select>
                            @error('kewarganegaraan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nomor_sk">Nomor SK Penetapan WNI (14)</label>
                            <input type="text" id="nomor_sk" class="form-control @error('nomor_sk') is-invalid @enderror" name="nomor_sk" placeholder="Masukkan Nomor SK ..." value="{{ old('nomor_sk') }}" disabled>
                            @error('nomor_sk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label mr-5" for="akta_lahir">Akta Lahir</label>
                            <label class="custom-toggle">
                                <input type="checkbox" id="akta_lahir" checked name="akta_lahir" id="akta_lahir" disabled>
                                <span class="custom-toggle-slider rounded-circle" data-label-off="Tidak" data-label-on="Ada"></span>
                            </label>
                        </div>
                        <div class="form-group col-lg-5 col-md-12">
                            <label class="form-control-label mr-5" for="nomor_akta_lahir">Nomor Akta Lahir (16)</label>
                            <input type="text" id="nomor_akta_lahir" class="form-control @error('nomor_akta_lahir') is-invalid @enderror" name="nomor_akta_lahir" placeholder="Masukkan Nomor Akta Lahir ..." value="{{ old('nomor_akta_lahir') }}" disabled>
                            @error('nomor_akta_lahir')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label" for="gol_darah">Gol. Darah (17)</label>
                            <select class="form-control select2 @error('gol_darah') is-invalid @enderror" name="gol_darah" id="gol_darah" disabled>
                                <option selected value="">Pilih Gol. Darah</option>
                                <option value="1" {{ old('gol_darah') == 1 ? 'selected="true"' : ''  }}>A</option>
                                <option value="2" {{ old('gol_darah') == 2 ? 'selected="true"' : ''  }}>B</option>
                                <option value="3" {{ old('gol_darah') == 3 ? 'selected="true"' : ''  }}>AB</option>
                                <option value="4" {{ old('gol_darah') == 4 ? 'selected="true"' : ''  }}>O</option>
                                <option value="5" {{ old('gol_darah') == 5 ? 'selected="true"' : ''  }}>A+</option>
                                <option value="6" {{ old('gol_darah') == 6 ? 'selected="true"' : ''  }}>A-</option>
                                <option value="7" {{ old('gol_darah') == 7 ? 'selected="true"' : ''  }}>B+</option>
                                <option value="8" {{ old('gol_darah') == 8 ? 'selected="true"' : ''  }}>B-</option>
                                <option value="9" {{ old('gol_darah') == 9 ? 'selected="true"' : ''  }}>AB+</option>
                                <option value="10" {{ old('gol_darah') == 10 ? 'selected="true"' : ''  }}>AB-</option>
                                <option value="11" {{ old('gol_darah') == 11 ? 'selected="true"' : ''  }}>O+</option>
                                <option value="12" {{ old('gol_darah') == 12 ? 'selected="true"' : ''  }}>O-</option>
                                <option value="13" {{ old('gol_darah') == 13 ? 'selected="true"' : ''  }}>Tidak Tahu</option>
                            </select>
                            @error('gol_darah')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label mr-5" for="agama">Agama (18)</label>
                            <select class="form-control select2 @error('agama') is-invalid @enderror" name="agama" id="agama" disabled>
                                <option selected value="">Pilih Agama</option>
                                <option value="1" {{ old('agama') == 1 ? 'selected="true"' : ''  }}>Islam</option>
                                <option value="2" {{ old('agama') == 2 ? 'selected="true"' : ''  }}>Kristen</option>
                                <option value="3" {{ old('agama') == 3 ? 'selected="true"' : ''  }}>Katholik</option>
                                <option value="4" {{ old('agama') == 4 ? 'selected="true"' : ''  }}>Hindu</option>
                                <option value="5" {{ old('agama') == 5 ? 'selected="true"' : ''  }}>Budha</option>
                                <option value="6" {{ old('agama') == 6 ? 'selected="true"' : ''  }}>Kong Hu Cu</option>
                                <option value="7" {{ old('agama') == 7 ? 'selected="true"' : ''  }}>Lainnya</option>
                            </select>
                            @error('agama')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-5 col-md-12">
                            <label class="form-control-label" for="nama_organisasi_yme">Nama Organisasi Kepercayaan terhadap Tuhan YME (19)</label>
                            <input type="text" id="nama_organisasi_yme" class="form-control @error('nama_organisasi_yme') is-invalid @enderror" name="nama_organisasi_yme" placeholder="Masukkan Nama Organisasi ..." value="{{ old('nama_organisasi_yme') }}" disabled>
                            @error('nama_organisasi_yme')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="status_perkawinan">Status Perkawinan (20)</label>
                            <select class="form-control select2 @error('status_perkawinan') is-invalid @enderror" name="status_perkawinan" id="status_perkawinan" disabled>
                                <option selected value="">Pilih Status Perkawinan</option>
                                <option value="1" {{ old('status_perkawinan') == 1 ? 'selected="true"' : ''  }}>Belum Kawin</option>
                                <option value="2" {{ old('status_perkawinan') == 2 ? 'selected="true"' : ''  }}>Kawin Tercatat</option>
                                <option value="3" {{ old('status_perkawinan') == 3 ? 'selected="true"' : ''  }}>Kawin Belum Tercatat</option>
                                <option value="4" {{ old('status_perkawinan') == 4 ? 'selected="true"' : ''  }}>Cerai Hidup Tercatat</option>
                                <option value="5" {{ old('status_perkawinan') == 5 ? 'selected="true"' : ''  }}>Cerai Hidup belum Tercatat</option>
                                <option value="6" {{ old('status_perkawinan') == 6 ? 'selected="true"' : ''  }}>Cerai Mati</option>
                            </select>
                            @error('status_perkawinan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label" for="akta_perkawinan">Akta Perkawinan (21)</label>
                            <label class="custom-toggle">
                                <input type="checkbox" id="akta_perkawinan" checked name="akta_perkawinan" id="akta_perkawinan" disabled>
                                <span class="custom-toggle-slider rounded-circle" data-label-off="Tidak" data-label-on="Ada"></span>
                            </label>
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nomor_akta_perkawinan">Nomer Akta Perkawinan (22)</label>
                            <input type="text" id="nomor_akta_perkawinan" class="form-control @error('nomor_akta_perkawinan') is-invalid @enderror" name="nomor_akta_perkawinan" placeholder="Masukkan Nama Organisasi ..." value="{{ old('nomor_akta_perkawinan') }}" disabled>
                            @error('nomor_akta_perkawinan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="tgl_perkawinan">Tanggal Perkawinan (23)</label>
                            <input type="date" id="tgl_perkawinan" class="form-control @error('tgl_perkawinan') is-invalid @enderror" name="tgl_perkawinan" placeholder="Masukkan Tanggal Perkawinan ..." value="{{ old('tgl_perkawinan') }}" disabled>
                            @error('tgl_perkawinan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-2 col-md-12">
                            <label class="form-control-label" for="akta_cerai">Akta Cerai (24)</label>
                            <label class="custom-toggle">
                                <input type="checkbox" id="akta_cerai" checked name="akta_cerai" id="akta_cerai" disabled>
                                <span class="custom-toggle-slider rounded-circle" data-label-off="Tidak" data-label-on="Ada"></span>
                            </label>
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nomor_akta_cerai">Nomer Akta Perkawinan (25)</label>
                            <input type="text" id="nomor_akta_cerai" class="form-control @error('nomor_akta_cerai') is-invalid @enderror" name="nomor_akta_cerai" placeholder="Masukkan Nomor Akta Cerai ..." value="{{ old('nomor_akta_cerai') }}" disabled>
                            @error('nomor_akta_cerai')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="tgl_cerai">Tanggal Perceraian (26)</label>
                            <input type="date" id="tgl_cerai" class="form-control @error('tgl_cerai') is-invalid @enderror" name="tgl_cerai" placeholder="Masukkan Tanggal Perceraian ..." value="{{ old('tgl_cerai') }}" disabled>
                            @error('tgl_cerai')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="status_keluarga">Status Hubungan (27)</label>
                            <select class="form-control @error('status_keluarga') is-invalid @enderror" name="status_keluarga" id="status_keluarga" disabled>
                                <option selected value="">Pilih Status Keluarga</option>
                                <option value="1" {{ old('status_keluarga') == 1 ? 'selected="true"' : ''  }}>Kepala Keluarga</option>
                                <option value="2" {{ old('status_keluarga') == 2 ? 'selected="true"' : ''  }}>Suami</option>
                                <option value="3" {{ old('status_keluarga') == 3 ? 'selected="true"' : ''  }}>Istri</option>
                                <option value="4" {{ old('status_keluarga') == 4 ? 'selected="true"' : ''  }}>Anak</option>
                                <option value="5" {{ old('status_keluarga') == 5 ? 'selected="true"' : ''  }}>Menantu</option>
                                <option value="6" {{ old('status_keluarga') == 6 ? 'selected="true"' : ''  }}>Cucu</option>
                                <option value="7" {{ old('status_keluarga') == 7 ? 'selected="true"' : ''  }}>Orang Tua</option>
                                <option value="8" {{ old('status_keluarga') == 8 ? 'selected="true"' : ''  }}>Mertua</option>
                                <option value="9" {{ old('status_keluarga') == 9 ? 'selected="true"' : ''  }}>Famili</option>
                                <option value="10" {{ old('status_keluarga') == 10 ? 'selected="true"' : ''  }}>Lainnya</option>
                            </select>
                            @error('status_keluarga')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label mr-2" for="kelainan_fisik_mental">Kelainan Fisik dan Mental (28)</label>
                            <label class="custom-toggle">
                                <input type="checkbox" id="kelainan_fisik_mental" checked name="kelainan_fisik_mental" id="kelainan_fisik_mental" disabled>
                                <span class="custom-toggle-slider rounded-circle" data-label-off="Tidak" data-label-on="Ada"></span>
                            </label>
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label mr-5" for="penyandang_cacat">Penyandang Cacat (29)</label>
                            <select class="form-control select2 @error('penyandang_cacat') is-invalid @enderror" name="penyandang_cacat" id="penyandang_cacat" disabled>
                                <option selected value="">Pilih Penyandang Cacat</option>
                                <option value="1" {{ old('penyandang_cacat') == 1 ? 'selected="true"' : ''  }}>Cacat Fisik</option>
                                <option value="2" {{ old('penyandang_cacat') == 2 ? 'selected="true"' : ''  }}>Cacat Neta/Buta</option>
                                <option value="3" {{ old('penyandang_cacat') == 3 ? 'selected="true"' : ''  }}>Cacat Rungu/Wicara</option>
                                <option value="4" {{ old('penyandang_cacat') == 4 ? 'selected="true"' : ''  }}>Cacat Mental/Jiwa</option>
                                <option value="5" {{ old('penyandang_cacat') == 5 ? 'selected="true"' : ''  }}>Cacat Fisik dan Mental</option>
                                <option value="6" {{ old('penyandang_cacat') == 6 ? 'selected="true"' : ''  }}>Cacat Lainnya</option>
                            </select>
                            @error('penyandang_cacat')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label mr-5" for="pendidikan_id">Pendidikan Terakhir (30)</label>
                            <select class="form-control select2 @error('pendidikan_id') is-invalid @enderror" name="pendidikan_id" id="pendidikan_id" disabled>
                                <option selected value="">Pilih Pendidikan</option>
                                @foreach ($pendidikan as $item)
                                    <option value="{{ $item->id }}" {{ old('pendidikan_id') == $item->id ? 'selected="true"' : ''  }}>{{ $item->nama }}</option>
                                @endforeach
                                </select>
                                @error('pendidikan_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label mr-5" for="pekerjaan_id">Jenis Pekerjaan (31)</label>
                            <select class="form-control select2 @error('pekerjaan_id') is-invalid @enderror" name="pekerjaan_id" id="pekerjaan_id" disabled>
                                <option selected value="">Pilih Pekerjaan</option>
                                @foreach ($pekerjaan as $item)
                                        <option value="{{ $item->id }}" {{ old('pekerjaan_id') == $item->id ? 'selected="true"' : ''  }}>{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                                @error('pekerjaan_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nomor_itas_itap">Nomer ITAS/ITAP (32)</label>
                            <input type="text" id="nomor_itas_itap" class="form-control @error('nomor_itas_itap') is-invalid @enderror" name="nomor_itas_itap" placeholder="Masukkan Nomor Itas/Itap ..." value="{{ old('nomor_itas_itap') }}" disabled>
                            @error('nomor_itas_itap')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="tempat_terbit_itas_itap">Tanggal Perceraian (33)</label>
                            <input type="date" id="tempat_terbit_itas_itap" class="form-control @error('tempat_terbit_itas_itap') is-invalid @enderror" name="tempat_terbit_itas_itap" placeholder="Masukkan Tempat penerbitas ITAS/ITAP ..." value="{{ old('tempat_terbit_itas_itap') }}" disabled>
                            @error('tempat_terbit_itas_itap')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="tgl_itas_itap">Tanggal Diterbitakan ITAS/ITAP (34)</label>
                            <input type="date" id="tgl_itas_itap" class="form-control @error('tgl_itas_itap') is-invalid @enderror" name="tgl_itas_itap" placeholder="Masukkan Tanggal Terbit ITAS/ITAP ..." value="{{ old('tgl_itas_itap') }}" disabled>
                            @error('tgl_itas_itap')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="tgl_itas_itap_expired">Tanggal Berakhir ITAS/ITAP (35)</label>
                            <input type="date" id="tgl_itas_itap_expired" class="form-control @error('tgl_itas_itap_expired') is-invalid @enderror" name="tgl_itas_itap_expired" placeholder="Masukkan Tanggal Expired ITAS/ITAP ..." value="{{ old('tgl_itas_itap_expired') }}" disabled>
                            @error('tgl_itas_itap_expired')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="tempat_datang_pertama">Tempat Kedatangan Pertama (36)</label>
                            <input type="date" id="tempat_datang_pertama" class="form-control @error('tempat_datang_pertama') is-invalid @enderror" name="tempat_datang_pertama" placeholder="Masukkan Tempat kedatangan pertama ..." value="{{ old('tempat_datang_pertama') }}" disabled>
                            @error('tempat_datang_pertama')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-6 col-md-12">
                            <label class="form-control-label" for="tgl_kedatangan_pertama">Tanggal Kedatangan Pertama (37)</label>
                            <input type="date" id="tgl_kedatangan_pertama" class="form-control @error('tgl_kedatangan_pertama') is-invalid @enderror" name="tgl_kedatangan_pertama" placeholder="Masukkan Tanggal Kedatangan ..." value="{{ old('tgl_kedatangan_pertama') }}" disabled>
                            @error('tgl_kedatangan_pertama')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nik_ibu">NIK Ibu (38)</label>
                            <input type="text" id="nik_ibu" class="form-control @error('nik_ibu') is-invalid @enderror" name="nik_ibu" placeholder="Masukkan Nik Ibu ..." value="{{ old('nik_ibu') }}" disabled>
                            @error('nik_ibu')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nama_ibu">Nama Ibu (39)</label>
                            <input type="text" id="nama_ibu" class="form-control @error('nama_ibu') is-invalid @enderror" name="nama_ibu" placeholder="Masukkan Nama Ibu ..." value="{{ old('nama_ibu') }}" disabled>
                            @error('nama_ibu')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nik_ibu">NIK Ayah (40)</label>
                            <input type="text" id="nik_ayah" class="form-control @error('nik_ayah') is-invalid @enderror" name="nik_ayah" placeholder="Masukkan Nik Ayah ..." value="{{ old('nik_ayah') }}" disabled>
                            @error('nik_ayah')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-lg-3 col-md-12">
                            <label class="form-control-label" for="nama_ayah">Nama Ayah (41)</label>
                            <input type="text" id="nama_ayah" class="form-control @error('nama_ayah') is-invalid @enderror" name="nama_ayah" placeholder="Masukkan Nama Ayah ..." value="{{ old('nama_ayah') }}" disabled>
                            @error('nama_ayah')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="mt-5 d-flex justify-content-between">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    $(document).ready(function () {

        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();

        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var $target = $(e.target);
            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {
            var $active = $('.nav-tabs li>.active');
            $active.parent().next().find('.nav-link').removeClass('disabled');
            nextTab($active);
        });

        $(".prev-step").click(function (e) {
            var $active = $('.nav-tabs li>a.active');
            prevTab($active);
        });

        $('#kode_surat_id').select2({
            placeholder: "Pilih Kode Surat",
            allowClear: true
        });

        $('.select2').select2();

        $("#customRadioInline2").change(function() {
            if(this.checked) {
                $('#customRadioInline1').attr("disabled","disabled");
                $('#customRadioInline3').attr("disabled","disabled");
                wni(2);
            }else{
                $('#customRadioInline1').removeAttr("disabled");
                $('#customRadioInline3').removeAttr("disabled");
                wni_disabled(2);
            }
        });

        $("#customRadioInline1").change(function() {
            if(this.checked) {
                $('#customRadioInline2').attr("disabled","disabled");
                $('#customRadioInline3').attr("disabled","disabled");
                wni(1);
            }else{
                $('#customRadioInline2').removeAttr("disabled");
                $('#customRadioInline3').removeAttr("disabled");
                wni_disabled(1);
            }
        });

        $("#customRadioInline3").change(function() {
            if(this.checked) {
                $('#customRadioInline2').attr("disabled","disabled");
                $('#customRadioInline1').attr("disabled","disabled");
                wni(3);
            }else{
                $('#customRadioInline2').removeAttr("disabled");
                $('#customRadioInline1').removeAttr("disabled");
                wni_disabled(3);
            }
        });

        $('#akta_lahir').change(function() {
            if(this.checked){
                $('#nomor_akta_lahir').removeAttr('disabled');
                $('#nomor_akta_lahir').focus();
            }else{
                $('#nomor_akta_lahir').attr('disabled','disabled');
                $('#nomor_akta_lahir').val("-");
            }
        });

        $('#akta_perkawinan').change(function() {
            if(this.checked){
                $('#nomor_akta_perkawinan').removeAttr('disabled');
                $('#tgl_perkawinan').removeAttr('disabled');
                $('#nomor_akta_perkawinan').focus();
            }else{
                $('#nomor_akta_perkawinan').attr('disabled','disabled');
                $('#tgl_perkawinan').attr('disabled','disabled');
                $('#nomor_akta_perkawinan').val("-");
            }
        });

        $('#akta_cerai').change(function() {
            if(this.checked){
                $('#nomor_akta_cerai').removeAttr('disabled');
                $('#tgl_cerai').removeAttr('disabled');
                $('#nomor_akta_cerai').focus();
            }else{
                $('#nomor_akta_cerai').attr('disabled','disabled');
                $('#tgl_cerai').attr('disabled','disabled');
                $('#nomor_akta_cerai').val("-");
            }
        });

        $('#kelainan_fisik_mental').change(function() {
            if(this.checked){
                $('#penyandang_cacat').removeAttr('disabled');
                $('#penyandang_cacat').focus();
            }else{
                $('#penyandang_cacat').attr('disabled','disabled');
                $('#penyandang_cacat').val("");
            }
        });

        $('#provinsi_id').on('change', function(){
            let id = $(this).val();
            $('.kabupaten_id').select2({
                placeholder: 'Search Kabupaten...',
                dropdownAutoWidth: true,
                tags: false,
                allowClear: false,
                ajax: {
                    url: "{{url('kabupaten')}}/"+id,
                    dataType: 'json',
                    delay: 500,
                    processResults: function (data) {
                        let  data1 = data;
                    return {
                        results:  $.map(data1, function (item) {
                            return {
                                text: item.city_name,
                                id: item.city_id
                            }
                        })
                    };
                },
                cache: true
                }
            });
        })

        $('#kabupaten_id').on('change', function(){
            let id = $(this).val();
            $('.kecamatan_id').select2({
                placeholder: 'Search Kecamatan...',
                dropdownAutoWidth: true,
                tags: false,
                allowClear: false,
                ajax: {
                    url: "{{url('kecamatan')}}/"+id,
                    dataType: 'json',
                    delay: 500,
                    processResults: function (data) {
                        let  data1 = data;
                    return {
                        results:  $.map(data1, function (item) {
                            return {
                                text: item.dis_name,
                                id: item.dis_id
                            }
                        })
                    };
                },
                cache: true
                }
            });
        })

        $('#kecamatan_id').on('change', function(){
            let id = $(this).val();
            $('.desa_id').select2({
                placeholder: 'Search Desa...',
                dropdownAutoWidth: true,
                tags: false,
                allowClear: false,
                ajax: {
                    url: "{{url('kelurahan')}}/"+id,
                    dataType: 'json',
                    delay: 500,
                    processResults: function (data) {
                        let  data1 = data;
                    return {
                        results:  $.map(data1, function (item) {
                            return {
                                text: item.subdis_name,
                                id: item.subdis_id
                            }
                        })
                    };
                },
                cache: true
                }
            });
        })

        $('.save').on('click', function(e){
            e.preventDefault();
            let id = $('.id_dafduk').val();
            let nama_kepala = $('.nama_kepala').val();
            let alamat = $('.alamat').val();
            let kode_pos = $('.kode_pos').val();
            let email = $('.email').val();
            let provinsi_id  = $('#provinsi_id').val();
            let kecamatan_id = $('#kecamatan_id').val();
            let kabupaten_id = $('#kabupaten_id').val();
            let desa_id = $('#desa_id').val();
            let alamat_ln =  $('.alamat_ln').val();
            let kota = $('.kota').val();
            let provinsi_ln = $('.provinsi_ln').val();
            let negara_ln = $('.negara_ln').val();
            let telp = $('.telp').val();
            // let email = $('.email').val();
            let kode_negara = $('.kode_negara').val();
            let kode_perwakilan = $('.kode_perwakilan').val();
            let jml_anggota = $('.jml_anggota').val();
            $.ajax({
                type: "POST",
                dataType: "json",
                data: {
                    _token: '{{ csrf_token() }}',
                    id:id,
                    nama_kk  : nama_kepala,
                    alamat_kk : alamat,
                    kode_pos_wn : kode_pos,
                    email_kk:email,
                    kd_profinsi:provinsi_id,
                    kd_kabupaten:kabupaten_id,
                    kd_kelurahan:kecamatan_id,
                    kd_kampung:desa_id,
                    alamat:alamat_ln,
                    kota:kota,
                    negara:negara_ln,
                    telp_wn:telp,
                    kd_negara:kode_negara,
                    kd_kbri:kode_perwakilan
                },
                url: "{{route('buat-dafduk.update')}}",
                success: function (data) {

                },
                error: function (data) {

                }
            });
        })
    });

    function nextTab(elem) {
        $(elem).parent().next().find('a[data-toggle="tab"]').click();
    }
    function prevTab(elem) {
        $(elem).parent().prev().find('a[data-toggle="tab"]').click();
    }

    function wni(status){
        $('#nama_lenkap').removeAttr("disabled");
        $('#gelar_dpn').removeAttr("disabled");
        $('#gelar_blkg').removeAttr("disabled");
        $('#nmr_paspor').removeAttr("disabled");
        $('#tgl_expired_paspor').removeAttr("disabled");
        if(status ===2 || status === 3  ){
            $('#nama_sponsor').removeAttr("disabled");
            $('#tipe_sponsor').removeAttr("disabled");
            $('#alamat_sponsor').removeAttr("disabled");
        }
        $('#jns_kelamin').removeAttr("disabled");
        $('#tempat_lahir').removeAttr("disabled");
        $('#tgl_lahir').removeAttr("disabled");
        $('#kewarganegaraan').removeAttr("disabled");
        if(status === 3) {
            $('#nomor_sk').removeAttr("disabled");
        }
        $('#akta_lahir').removeAttr("disabled");
        $('#nomor_akta_lahir').removeAttr("disabled");
        $('#gol_darah').removeAttr("disabled");
        $('#agama').removeAttr("disabled");
        $('#nama_organisasi_yme').removeAttr("disabled");
        $('#status_perkawinan').removeAttr("disabled");
        $('#akta_perkawinan').removeAttr("disabled");
        $('#nomor_akta_perkawinan').removeAttr("disabled");
        $('#tgl_perkawinan').removeAttr("disabled");
        $('#akta_cerai').removeAttr("disabled");
        $('#nomor_akta_cerai').removeAttr("disabled");
        $('#tgl_cerai').removeAttr("disabled");
        $('#status_keluarga').removeAttr("disabled");
        $('#kelainan_fisik_mental').removeAttr("disabled");
        $('#penyandang_cacat').removeAttr("disabled");
        $('#pendidikan_id').removeAttr("disabled");
        $('#pekerjaan_id').removeAttr("disabled");
        if(status ===2 ){
            $('#nomor_itas_itap').removeAttr("disabled");
            $('#tempat_terbit_itas_itap').removeAttr("disabled");
            $('#tgl_itas_itap').removeAttr("disabled");
            $('#tgl_itas_itap_expired').removeAttr("disabled");
            $('#tempat_datang_pertama').removeAttr("disabled");
            $('#tgl_kedatangan_pertama').removeAttr("disabled");
        }
        $('#nik_ibu').removeAttr("disabled");
        $('#nama_ibu').removeAttr("disabled");
        $('#nik_ayah').removeAttr("disabled");
        $('#nama_ayah').removeAttr("disabled");
        $('#nama').focus();
    }

    function wni_disabled(status){
        $('#nama_lenkap').attr("disabled","disabled");
        $('#gelar_dpn').attr("disabled","disabled");
        $('#gelar_blkg').attr("disabled","disabled");
        $('#nmr_paspor').attr("disabled","disabled");
        $('#tgl_expired_paspor').attr("disabled","disabled");
        if(status === 2 || status === 3 ){
            $('#nama_sponsor').attr("disabled","disabled");
            $('#tipe_sponsor').attr("disabled","disabled");
            $('#alamat_sponsor').attr("disabled","disabled");
        }
        $('#jns_kelamin').attr("disabled","disabled");
        $('#tempat_lahir').attr("disabled","disabled");
        $('#tgl_lahir').attr("disabled","disabled");
        $('#kewarganegaraan').attr("disabled","disabled");
        if(status === 3) {
            $('#nomor_sk').attr("disabled","disabled");
        }
        $('#akta_lahir').attr("disabled","disabled");
        $('#nomor_akta_lahir').attr("disabled","disabled");
        $('#gol_darah').attr("disabled","disabled");
        $('#agama').attr("disabled","disabled");
        $('#nama_organisasi_yme').attr("disabled","disabled");
        $('#status_perkawinan').attr("disabled","disabled");
        $('#akta_perkawinan').attr("disabled","disabled");
        $('#nomor_akta_perkawinan').attr("disabled","disabled");
        $('#tgl_perkawinan').attr("disabled","disabled");
        $('#akta_cerai').attr("disabled","disabled");
        $('#nomor_akta_cerai').attr("disabled","disabled");
        $('#tgl_cerai').attr("disabled","disabled");
        $('#status_keluarga').attr("disabled","disabled");
        $('#kelainan_fisik_mental').attr("disabled","disabled");
        $('#penyandang_cacat').attr("disabled","disabled");
        $('#pendidikan_id').attr("disabled","disabled");
        $('#pekerjaan_id').attr("disabled","disabled");
        if(status ===2 ){
            $('#nomor_itas_itap').attr("disabled","disabled");
            $('#tempat_terbit_itas_itap').attr("disabled","disabled");
            $('#tgl_itas_itap').attr("disabled","disabled");
            $('#tgl_itas_itap_expired').attr("disabled","disabled");
            $('#tempat_datang_pertama').attr("disabled","disabled");
            $('#tgl_kedatangan_pertama').attr("disabled","disabled");
        }
        $('#nik_ibu').attr("disabled","disabled");
        $('#nama_ibu').attr("disabled","disabled");
        $('#nik_ayah').attr("disabled","disabled");
        $('#nama_ayah').attr("disabled","disabled");

    }
</script>
@endpush
