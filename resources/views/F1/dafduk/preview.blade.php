@php
    $desa = App\Desa::find(1);
@endphp


    <div style="margin-top: 1cm; margin-bottom:0.5cm; margin-left: 1cm; margin-right: 1cm;">
        <div style="height:100px;width:100%; margin-bottom: 1rem">
            <div class="row">
                <div class="col-3" style="text-align:right">
                    <b><span>F-1.01</span></b>
                </div>
                <div style="height:100px;width:100px;float:left" class="col-3">
                    <img src="{{ asset(Storage::url($desa->logo_header)) }}" alt="" height="100px" class="mr-3">
                </div>
                <div class="col-6" style="text-align:center">
                    <span style="font-size: 14pt; font-weight: bold">PEMERINTAHAN KABUPATEN PONOROGO</span><br>
                    <span style="font-size: 14pt; font-weight: bold">DINAS KEPENDUDUKAN DAN PENCATATAN SIPIL</span><br>
                    <span style="font-size: 11pt; font-weight: bold">Jl. Aloon-Alon Utara Nomor 8 Telepon (0352) 489317</span><br>
                    <div style="font-size: 14pt; font-weight: bold">
                        <u>PONOROGO</u>
                    </div>
                    <span style="font-size: 14pt; font-weight: bold">FORMULIR BIODATA KELUARGA</span><br>
                </div>

            </div>
            <br>
            <div class="row" style="background-color: #000;">
                <span style="color: #ffffff">PERHATIAN : Isilah Formulir ini dengan huruf cetak dan jelas serta mengikuti "TATA CARA PENGISIAN FORMULIR"</span>
            </div>
            <div class="row">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="checkbox" id="customRadioInline1" name="jenis-penduduk" class="custom-control-input" value="1">
                    <label class="custom-control-label" for="customRadioInline1">Input Data Kepala Keluarga dan Anggota Keluarga WNI</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="checkbox" id="customRadioInline2" name="jenis-penduduk" class="custom-control-input" value="2">
                    <label class="custom-control-label" for="customRadioInline2">Input Data Kepala Keluarga dan Anggota Keluarga Orang Asing</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="checkbox" id="customRadioInline3" name="jenis-penduduk" class="custom-control-input" value="3">
                    <label class="custom-control-label" for="customRadioInline3">Input Data Kepala Keluarga dan Anggota Keluarga WNI diluar Negeri</label>
                </div>
            </div>
            <div class="row mb-2">
                <div class="form-group row">
                    <label class="form-control-label col-form-label col-md-12" style="text-transform: uppercase">Data Kepala Keluarga</label>
                </div>
                <div class="form-group row custom-control-inline">
                    <label class="form-control-label col-form-label col-md-3" for="nama_kepala">1. Nama Kepala Keluarga/Name of head of the family</label>
                    <span style="font-size: 12pt; font-weight: bold; text-transform: uppercase">{{$dafduk->nama_kk}}</span>
                </div>
                <div class="form-group row custom-control-inline">
                    <label class="form-control-label col-form-label col-md-3" for="alamat">2. Alamat/address</label>
                    <span style="font-size: 12pt; font-weight: bold; text-transform: uppercase">{{$dafduk->alamat_kk}}</span>
                </div>
                <div class="form-group row custom-control-inline">
                    <label class="form-control-label col-form-label col-md-3" for="alamat">3. Kode Pos/Post Code</label>
                    <span style="font-size: 12pt; font-weight: bold; text-transform: uppercase">{{$dafduk->kode_pos_wn}}</span>
                </div>
            </div>
        </div>
    </div>
