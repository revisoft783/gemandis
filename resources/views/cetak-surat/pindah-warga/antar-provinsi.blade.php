<div class="row">
    <div class="col-4">
        <span style="font-size: 12pt;font-weight: bold">Provinsi</span>
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">:</span>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">*)</span>
    </div>
    <div class="col-2">
        <input type="text" name="provinsi" id="provinsi"></div>
    </div>

</div>
<div class="row">
    <div class="col-4">
        <span style="font-size: 12pt;font-weight: bold">Kabupaten</span>
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">:</span>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">*)</span>
    </div>
    <div class="col-2">
        <input type="text" name="kabupaten" id="kabupaten"></div>
    </div>

</div>
<div class="row">
    <div class="col-4">
        <span style="font-size: 12pt;font-weight: bold">Kecamatan</span>
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">:</span>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">*)</span>
    </div>
    <div class="col-2">
        <input type="text" name="kecamatan" id="kecamatan"></div>
    </div>

</div>
<div class="row">
    <div class="col-4">
        <span style="font-size: 12pt;font-weight: bold">Desa/Kelurahan</span>
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">:</span>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">*)</span>
    </div>
    <div class="col-2">
        <input type="text" name="kelurahan" id="kelurahan"></div>
    </div>

</div>
<div class="row">
    <div class="col-4">
        <span style="font-size: 12pt;font-weight: bold">Dusun/Dukuh/Kampung</span>
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">:</span>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
        <table border="1">
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
    </div>
    <div class="col-2">
        <span style="font-size: 12pt;font-weight: bold">*)</span>
    </div>
    <div class="col-2">
        <input type="text" name="kelurahan" id="kelurahan"></div>
    </div>

</div>
<div class="row">
    <div class="col-12">
        <p><span style="font-size: 12pt;font-weight: bold">FORMULIR PERMOHONAN PINDAH DATANG WNI</span></p>
        <p><span style="font-size: 10pt;font-weight: bold">Antar Kabupaten/Kota atau Antar Provinsi</span></p>
        <p><span style="font-size: 10pt;font-weight: bold">No. 471.2//405.30.14.10/{{date('Y')}}</span></p>
    </div>
</div>
