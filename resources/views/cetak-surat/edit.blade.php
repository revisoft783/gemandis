@extends('layouts.app')
@section('title', 'Edit Detail Cetak Surat')

@section('content-header')
<div class="pt-5 pb-8 header pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="shadow card h-100">
                    <div class="border-0 card-header">
                        <div class="text-center d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Edit Detail Cetak Surat</h2>
                                <p class="mb-0 text-sm">Kelola Cetak Surat</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ route("surat.show",$cetakSurat->surat) }}?page={{ request('page') }}" class="btn btn-success" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="border-0 shadow card bg-secondary">
    <div class="card-body px-lg-5 py-lg-5">
        <form role="form" action="{{ route('cetakSurat.update', $cetakSurat) }}" method="POST">
            @csrf @method('patch')
            <div class="mb-3 form-group">
                <label for="nomor" class="form-control-label">Nomor</label>
                <input id="nomor" type="number" class="form-control form-control-alternative" name="nomor" autofocus placeholder="Masukkan Nomor" value="{{ $cetakSurat->nomor }}">
            </div>
            @foreach ($cetakSurat->surat->isiSurat as $key => $isiSurat)
                @if ($isiSurat->jenis_isi == 3)
                    <div class="mb-3 form-group">
                        <label for="{{ $isiSurat->isi .''.$key }}" class="form-control-label">{{ $isiSurat->isi }}</label>
                        <input required id="{{ $isiSurat->isi .''.$key }}" class="form-control form-control-alternative" name="isian[]" autofocus placeholder="Masukkan {{ $isiSurat->isi }}">
                    </div>
                @endif
                @if ($isiSurat->tampilkan == 1)
                    <p class="mt-5 mb-0">{{ $isiSurat->isi }}</p>
                @endif
                @php
                    $string = $isiSurat->isi;
                    preg_match_all("/\{[A-Za-z\s\(\)]+\}/", $string, $matches);
                @endphp
                @foreach ($matches[0] as $k => $value)
                    @php
                        $pertama = substr($value,1);
                        $hasil = substr($pertama,0,-1);
                    @endphp
                    <div class="mb-3 form-group">
                        <label for="{{ $hasil .''.$k }}" class="form-control-label">{{ $hasil }}</label>
                        <input required id="{{ $hasil .''.$k }}" class="form-control form-control-alternative" name="isian[]" autofocus placeholder="Masukkan {{ $hasil }}">
                    </div>
                @endforeach
                @if($isiSurat->jenis_isi == 4)
                    <div class="mt-3 mb-3 form-group">
                        <label class="form-control-label"  for="berkas_file_{{ $key}}">{{ $isiSurat->isi }}</label>
                        <input type="file" accept=".pdf" id="berkas_file_{{ $key}}" class="form-control @error('berkas_file_kk') is-invalid @enderror" name="berkas_file[]" placeholder="Masukkan Berkas Scan KK  ..." value="{{ old('berkas_file_kk') }}">
                            @error('berkas_file_{{ $key}}')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                    </div>
                @endif
            @endforeach

            @if ($cetakSurat->surat->tanda_tangan_bersangkutan == 1)
                <div class="mb-3 form-group">
                    <label for="tanda_tangan_bersangkutan" class="form-control-label">Nama yang bersangkutan</label>
                    <input required id="tanda_tangan_bersangkutan" class="form-control form-control-alternative" name="isian[]" autofocus placeholder="Masukkan nama yang bersangkutan">
                </div>
            @endif

            <div class="text-center">
                <button type="submit" class="my-4 btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        let detailCetak = {!! json_encode($cetakSurat->detailCetak) !!};
        $.each($('[name="isian[]"]'), function (i,e){
            $(this).val(detailCetak[i].isian)
        });
    });
</script>
@endpush
