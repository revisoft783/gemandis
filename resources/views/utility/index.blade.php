@extends('layouts.app')

@section('title', 'Rekam KTP')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Penduduk</h2>
                                <p class="mb-0 text-sm">Kelola Rekam KTP</p>
                            </div>
                            <div class="mb-3">
                                <a target="_blank" href="{{ route('penduduk.print_all') }}" data-toggle="tooltip" class="mb-1 btn btn-secondary" title="Cetak"><i class="fas fa-print"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('form-search')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" action="{{ URL::current() }}" method="GET">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Cari ...." type="serach" name="cari" value="{{ request('cari') }}">
        </div>
    </div>
</form>
@endsection

@section('form-search-mobile')
<form class="mt-4 mb-3 d-md-none" action="{{ URL::current() }}" method="GET">
    <div class="input-group input-group-rounded input-group-merge">
        <input type="search" name="cari" class="form-control form-control-rounded form-control-prepended" placeholder="cari" aria-label="Search" value="{{ request('cari') }}">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <span class="fa fa-search"></span>
            </div>
        </div>
    </div>
</form>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card shadow">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover table-sm table-striped table-bordered">
                <thead>
                    <th class="text-center">No</th>
                    <th class="text-center">Opsi</th>
                    <th class="text-center">tag_card_id</th>
                    <th class="text-center">NIK</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Tempat, Tanggal Lahir</th>
                    <th class="text-center">Golongan Darah</th>
                    <th class="text-center">Agama</th>
                    <th class="text-center">Pekerjaan</th>
                    <th class="text-center">RT/RW</th>
                    <th class="text-center">Kelurahan</th>
                    <th class="text-center">Kecamatan</th>
                    <th class="text-center">Kabupaten</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">Kewarganegaraan</th>
                </thead>
                <tbody>
                    @forelse ($penduduk as $item)
                        <tr>
                            <td style="vertical-align: middle" class="text-center">{{ ($penduduk->currentpage()-1) * $penduduk->perpage() + $loop->index + 1 }}</td>
                            <td style="vertical-align: middle">
                                <a href="{{ route('utility.show.ktp', ['nik'=>$item->nik]) }}&page={{ request('page') }}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                                <a href="javascript:void(0)" onclick="synchronize('.$item->nik.')" class="btn btn-sm btn-danger synchronize" data-toggle="tooltip" title="Synchronize"><i class="fas fa-sync"></i></a>
                            </td>
                            <td style="vertical-align: middle"><a href="{{ route('utility.show.ktp', ['nik'=>$item->tag_card_id]) }}&page={{ request('page') }}">{{ $item->tag_card_id }}</a></td>
                            <td style="vertical-align: middle"><a href="{{ route('utility.show.ktp', ['nik'=>$item->nik]) }}&page={{ request('page') }}">{{ $item->nik }}</a></td>
                            <td style="vertical-align: middle">{{ $item->nama }}</td>
                            <td style="vertical-align: middle">{{ $item->ttl }}, {{ date('d/m/Y',strtotime($item->tgllhr)) }}</td>
                            <td style="vertical-align: middle">{{ $item->goldar }}</td>
                            <td style="vertical-align: middle">{{ $item->agm}}</td>
                            <td style="vertical-align: middle">{{ $item->pkj}}</td>
                            <td style="vertical-align: middle">{{ $item->rt }}/ {{ $item->rw }}</td>
                            <td style="vertical-align: middle">{{ $item->kel}}</td>
                            <td style="vertical-align: middle">{{ $item->kec}}</td>
                            <td style="vertical-align: middle">{{ $item->kab}}</td>
                            <td style="vertical-align: middle">{{ $item->prv}}</td>
                            <td style="vertical-align: middle">{{ $item->kwgn}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="14" align="center">Data tidak tersedia</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            {{ $penduduk->links('layouts.components.pagination') }}
        </div>
    </div>
</div>

@include('layouts.components.hapus', ['nama_hapus' => 'penduduk'])
@endsection

@push('scripts')
<script>
    $(document).ready(function () {


    });

    var synchronize = function(nik) {
        if (confirm("Apakah anda yakin ingin synchronize data ini?")) {
            $.ajax({
                url     : "{{ route('utility.sync.ktp') }}",
                method  : 'post',
                data    : {
                    _token  : "{{ csrf_token() }}",
                    nik      : nik,
                },
                success : function(data){
                    alertSuccess(data.message);
                    location.reload();
                }
            })
        }
    }
</script>
@endpush
