@extends('layouts.app')

@section('title', 'Edit Rekam KTP')

@section('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
<style>
    .upload-image:hover{
        cursor: pointer;
        opacity: 0.7;
    }
</style>
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Edit Penduduk</h2>
                                <p class="mb-0 text-sm">Kelola Penduduk</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ route("utility") }}?page={{ request('page') }}" class="btn btn-success" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="row">
    <div class="col">
        <div class="card bg-secondary shadow h-100">
            <div class="card-body">
                <form autocomplete="off" method="POST" id="formUpdatePenduduk" action="{{route('utility.store.ktp')}}" class="formUpdatePenduduk">
                    @csrf
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="form-group col-lg-4 col-md-3">
                                    <label class="form-control-label" for="nik">NIK</label>
                                    <input readonly type="text" onkeypress="return hanyaAngka(event)" class="form-control @error('nik') is-invalid @enderror" name="nik" placeholder="Masukkan NIK ..." value="{{ $penduduk->nik }}">
                                    @error('nik')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group col-lg-4 col-md-3">
                                    <label class="form-control-label" for="nama">Nama</label>
                                    <input readonly type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Masukkan Nama ..." value="{{ $penduduk->nama }}">
                                    @error('nama')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                                </div>
                                <div class="form-group col-lg-4 col-md-3">
                                    <label class="form-control-label" for="tag_card_id">Tag Card Id</label>
                                    <input type="text" class="form-control @error('tag_card_id') is-invalid @enderror" name="tag_card_id" id="tag_card_id" placeholder="Masukkan Tag Card Id ..." value="{{ $penduduk->tag_card_id }}" focus>
                                    @error('tag_card_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-lg-4 col-md-3">
                                    <label class="form-control-label" for="agm">Agama</label>
                                    <input readonly type="text" class="form-control @error('agm') is-invalid @enderror" name="agm" placeholder="Masukkan Tag Card Id ..." value="{{ $penduduk->agm }}">
                                            @error('agm')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                                    @error('agm')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <h4>Data Kelahiran</h4>
                    <div class="pl-4">
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6">
                                <label class="form-control-label" for="ttl">Tempat Lahir</label>
                                <input readonly type="text" class="form-control @error('ttl') is-invalid @enderror" name="ttl" placeholder="Masukkan Tempat Lahir ..." value="{{  $penduduk->ttl }}">
                                @error('ttl')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group col-lg-6 col-md-6">
                                <label class="form-control-label" for="tgllhr">Tanggal Lahir</label>
                                <input readonly type="text" class="form-control @error('tgllhr') is-invalid @enderror" name="tgllhr" placeholder="Masukkan Tanggal Lahir ..." value="{{ $penduduk->tgllhr }}">
                                @error('tgllhr')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>
                        </div>
                    </div>
                    <h4>Pekerjaan</h4>
                    <div class="pl-4">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-control-label" for="pkj">Pekerjaan</label>
                                <input readonly type="text" class="form-control @error('pkj') is-invalid @enderror" name="pkj" placeholder="Masukkan Pekerjaan ..." value="{{ $penduduk->pkj }}">
                                @error('pkj')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>
                        </div>
                    </div>
                    <h4>Data Kewarganegaraan</h4>
                    <div class="pl-4">
                        <div class="row">
                            <div class="form-group col-lg-4 col-md-6">
                                <label class="form-control-label" for="kwgn">Kewarganegaraan</label>
                                <input readonly type="text" class="form-control @error('kwgn') is-invalid @enderror" name="kwgn" placeholder="Masukkan Kewarganegaraan ..." value="{{ $penduduk->kwgn }}">
                                @error('kwgn')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>
                        </div>
                    </div>
                    <h4>Alamat</h4>
                    <div class="pl-4">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-control-label" for="almt">Alamat</label>
                                <input readonly type="text" class="form-control @error('almt') is-invalid @enderror" name="almt" placeholder="Masukkan Alamat ..." value="{{$penduduk->almt }}">
                                @error('almt')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label" for="detail_dusun_id">RT/RW</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input readonly type="text" class="form-control @error('rt') is-invalid @enderror" name="rt" placeholder="Masukkan Nomor Rt ..." value="{{ $penduduk->rt }}">
                                    </div>
                                    <div class="col-md-6">
                                        <input readonly type="text" class="form-control @error('rw') is-invalid @enderror" name="rw" placeholder="Masukkan Nomor Rw ..." value="{{ $penduduk->rw }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label" for="kel">Kelurahan</label>
                                <input readonly type="text" class="form-control @error('kel') is-invalid @enderror" name="kel" placeholder="Masukkan Nomor Kelurahan ..." value="{{ $penduduk->kel }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label" for="kec">Kecamatan</label>
                                <input readonly type="text" class="form-control @error('kec') is-invalid @enderror" name="kel" placeholder="Masukkan Nomor Kecamatan ..." value="{{ $penduduk->kec }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label" for="kab">Kabupaten</label>
                                <input readonly type="text" class="form-control @error('kab') is-invalid @enderror" name="kab" placeholder="Masukkan Nomor Kabupaten ..." value="{{$penduduk->kab }}">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-control-label" for="kab">Provinsi</label>
                                <input readonly type="text" class="form-control @error('prv') is-invalid @enderror" name="prv" placeholder="Masukkan Nomor Provinsi ..." value="{{ $penduduk->prv }}">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" id="simpan">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#tag_card_id').focus();
        $('.formUpdatePenduduk').on('keyup keypress', function(e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });

    });
</script>
@endpush
