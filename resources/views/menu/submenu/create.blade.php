<div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="modal-create" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-dialog-centered modal-lg" role="document">
        <div class="modal-content bg-gradient-primary">
            <form action="{{route('sub-menu.store')}}" method="POST">
                @csrf
                <div class="modal-header">
                    <h2 class="modal-title" id="modal-title-delete">create {{ $nama_create }}</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-control-label" for="nama" style="color: white">Nama Submenu</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" placeholder="Masukkan Nama Submenu ...">
                            @error('nama')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-4">
                            <label class="form-control-label" for="url" style="color: white">Url</label>
                            <input type="text" class="form-control @error('url') is-invalid @enderror" id="url" name="url" placeholder="Masukkan Url Submenu ...">
                            @error('url')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-4">
                            <label class="form-control-label" for="icon" style="color: white">Icon</label>
                            <input type="text" class="form-control @error('icon') is-invalid @enderror" id="icon" name="icon" placeholder="Masukkan Icon Submenu ...">
                            @error('icon')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-4">
                            <label class="form-control-label" for="icon" style="color: white">Warna</label>
                            <input type="text" class="form-control @error('warna') is-invalid @enderror" id="warna" name="warna" placeholder="Masukkan warna Submenu ...">
                            @error('warna')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    {!! Form::hidden("menu_id", null, ["id"=>"menu_id"]) !!}
                    <button type="submit" class="btn btn-white">Save</button>
                    <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
