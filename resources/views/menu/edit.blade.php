@extends('layouts.app')

@section('title', 'Edit Menu')

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Edit Menu</h2>
                                <p class="mb-0 text-sm">Kelola menu</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ route("menu.index") }}" class="btn btn-success" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="row">
    <div class="col-md-5 mb-3">
        <div class="card bg-secondary shadow">
            <div class="card-body">
                <form autocomplete="off" action="{{ route('menu.update', $menu) }}" method="post">
                    @csrf @method('patch')
                    <div class="form-group">
                        <label class="form-control-label" for="nama">Nama Menu</label>
                        <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" placeholder="Masukkan Nama Menu ..." value="{{ old('nama', $menu->nama) }}">
                        @error('nama')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <input class="form-check-input ml-1" type="checkbox" id="show_droid" name="show_droid" @if($menu->show_droid ==1) checked="checked" @endif>
                        <label class="form-control-label ml-4" for="nama">Show in Android</label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" id="simpan">SIMPAN</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-7 mb-3">
        <div class="card shadow">
            <div class="card-header font-weight-bold">Sub Menu
                    <a href="javascript:void(0)" onclick="createSubmenu({{$menu->id}})" class="btn btn-success mb-3 float-right" title="Tambah" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Sub Menu</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20px">No</th>
                            <th width="40%">Nama Menu</th>
                            <th width="30%">url</th>
                            <th min-width="2%">Opsi</th>
                        </thead>
                        <tbody>
                            @forelse (App\Submenu::where('menu_id',$menu->id)->get() as $item)
                                <tr>
                                    <td style="vertical-align: middle">{{ $loop->iteration }}</td>
                                    <td style="vertical-align: middle">{{ $item->nama }}</td>
                                    <td style="vertical-align: middle">{{ $item->url }}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-6">
                                                <a href="#modal-hapus" class="btn btn-dark btn-sm akses" data-toggle="modal"><i class="fas fa-trash"></i> Hapus</a>
                                                <form action="{{ route('sub-menu.destroy', $item) }}" method="post">@csrf @method('delete')</form>
                                            </div>
                                            <div class="col-6">
                                                <a href="javascript:void(0)" onclick="editSubmenu({{$item->id}})" class="btn btn-success btn-sm edit" data-toggle="modal"><i class="fas fa-edit"></i> Edit</a>
                                                <form action="{{ route('sub-menu.store') }}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="menu_id" value="{{ $menu->id }}">
                                                    <input type="hidden" name="submenu_id" value="{{ $item->id }}">
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="4" align="center">Data tidak tersedia</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('menu.submenu.edit',['nama_edit' => 'Submenu'])
@include('menu.submenu.create',['nama_create' => 'Submenu'])
@include('layouts.components.hapus',['nama_hapus' => 'submenu'])
@include('layouts.components.alert')
@endsection

@push('scripts')
<script src="{{ asset('js/spectrum.js') }}"></script>
<script>
    $(document).ready(function () {
        $(".akses").click(function (event) {
            event.preventDefault();
            $(this).siblings('form').submit();
        });

        $('#myModal').on('shown.bs.modal', function() {
            $('.colorPicker').spectrum({
                color: $(this).data('color'),
                change: function (color) {
                    $(this).parent().siblings('.colorCode').val(color.toHexString().replace(/^#?/, ''));
                }
            });

            $('.colorCode').on('input', function () {
                var clr = $(this).val();
                $(this).parents('.input-group').find('.colorPicker').spectrum({
                    color: clr,
                });
            });
        });

    });

    var editSubmenu = function(id) {
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: '{{url('/menu/sub-menu/edit')}}/'+id,
            success: function(data) {
                $('#modal-edit #nama').val(data.nama);
                $('#modal-edit #url').val(data.url);
                $('#modal-edit #icon').val(data.icon);
                $('#modal-edit #warna').val(data.warna);
                $('#modal-edit #submenu_id').val(data.id);
                $('#modal-edit #menu_id').val(data.menu_id);
                $('#modal-edit').modal("show");
            },
            error: function(data) {
                $.toast({
                    heading: 'Error',
                    text: "Submenu can\'t find",
                    showHideTransition: 'plain',
                    icon: 'error',
                    position: 'top-right',
                })
            }
        });
    }

    var createSubmenu = function(id) {
        $('#modal-create #menu_id').val(id);
        $('#modal-create #nama').val("");
        $('#modal-create #url').val("");
        $('#modal-create #icon').val("");
        $('#modal-create #warna').val("");
        $('#modal-create #submenu_id').val("");
        $('#modal-create').modal("show");
    }
</script>
@endpush
