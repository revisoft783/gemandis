<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-dialog-centered modal-lg" role="document">
        <div class="modal-content bg-gradient-primary">
            <form autocomplete="off" action="{{ route('penduduk.update_mutasi')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h2 class="modal-title" id="modal-title-delete">Add Mutasi Penduduk</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label class="form-control-label" for="pilihan_keluarga" style="color: white">Semua Anggota</label>
                            <select class="form-control"name="pilihan_keluarga" id="pilihan_keluarga">
                                <option value="0">Semua Keluarga</option>
                                <option value="1" selected>Sendiri</option>
                            </select>
                        </div>
                        <div id="sendiri_pindah" class="col-12 row">
                            <div class="form-group col-6">
                                <label class="form-control-label" for="nomor_induk_penduduk" style="color: white">NIK Penduduk</label>
                                <div class="input-group-prepend">
                                    <input type="search" name="nomor_induk_penduduk" id="nomor_induk_penduduk" class="form-control form-control-rounded form-control-prepended cari" placeholder="nomor_induk_penduduk" aria-label="nomor_induk_penduduk" value="{{ request('nomor_induk_penduduk') }}">
                                    <div class="input-group-text">
                                        <span class="fa fa-search"></span>
                                    </div>
                                </div>
                                @error('nomor_induk_penduduk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>

                            <div class="form-group col-6">
                                <label class="form-control-label ml-3" for="nama_penduduk" style="color: white">Nama Penduduk</label>
                                <input type="text" class="form-control ml-3 @error('nama_penduduk') is-invalid @enderror" id="nama_penduduk" name="nama_penduduk" placeholder="Masukkan Nama Penduduk ..." readonly>
                                @error('nama_penduduk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div id="all_pindah" style="display: none" class="col-12 row">
                            <div class="form-group col-6">
                                <label class="form-control-label" for="nomor_induk_penduduk" style="color: white">KK Penduduk</label>
                                <div class="input-group-prepend">
                                    <input type="search" name="nomor_induk_penduduk" id="nomor_kk_penduduk" class="form-control form-control-rounded form-control-prepended cari_kk" placeholder="nomor_kk" aria-label="nomor_kk" value="{{ request('nomor_kk') }}">
                                    <div class="input-group-text">
                                        <span class="fa fa-search"></span>
                                    </div>
                                </div>
                                @error('nomor_induk_penduduk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                            </div>
                            <table class="ml-3 table table-hover table-sm table-striped table-bordered _keluarga" width="100%">
                                <thead>
                                    <th class="text-center">No</th>
                                    <th class="text-center">NIK</th>
                                    <th class="text-center">KK</th>
                                    <th class="text-center">Nama Lengkap</th>
                                </thead>
                                <tbody id="anggota_keluarga">

                                </tbody>
                            </table>
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="tgl_pindah" style="color: white">Tanggal Pindah</label>
                            <input type="date" class="form-control @error('tgl_pindah') is-invalid @enderror" name="tgl_pindah" id="tgl_pindah" placeholder="Masukkan Tanggal Pindah ..." value="{{ date('Y-m-d',strtotime(request('tanggal')))  }}">
                            @error('tgl_pindah')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="provinsi_id">Kode-Nama Provinsi/Code-Provinsi</label>
                            {!! Form::select("provinsi_id", getProvincies(), null, ['class' => 'form-control select2', 'id' => "provinsi_id"]) !!}
                                @error('provinsi_id')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="kota_tujuan">Kota Tujuan</label>
                            <select class="form-control kota_tujuan @error('kota_tujuan') is-invalid @enderror" name="kota_tujuan" id="kota_tujuan">
                                <option value="">Pilih Kota Tujuan</option>
                            </select>
                            @error('kota_tujuan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="kecamatan_tujuan">Kecamatan Tujuan</label>

                            <select class="form-control kecamatan_tujuan @error('kecamatan_tujuan') is-invalid @enderror" name="kecamatan_tujuan" id="kecamatan_tujuan">
                                <option value="">Pilih Kecamatan</option>
                            </select>
                            @error('kecamatan_tujuan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="kelurahan_tujuan">Kelurahan</label>
                            <select class="form-control kelurahan_tujuan @error('kelurahan_tujuan') is-invalid @enderror" name="kelurahan_tujuan" id="kelurahan_tujuan">
                                <option value="">Pilih Desa</option>
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="alamat" style="color: white">Alamat</label>
                            <input type="text" class="form-control @error('alamat_tujuan') is-invalid @enderror" id="alamat_tujuan" name="alamat_tujuan" placeholder="Masukkan Alamat tujuan ...">
                            @error('alamat_tujuan')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-12">
                            <label class="form-control-label"  style="color: white" for="berkas_file">Berkas Scan Surat Pengantar</label>
                            <input type="file" accept=".pdf" class="form-control @error('berkas_file') is-invalid @enderror" name="berkas_file" placeholder="Masukkan Berkas Scan Akta Meninggal  ..." value="{{ old('berkas_file') }}">
                                @error('berkas_file')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-12">
                            <label class="form-control-label"  style="color: white" for="berkas_file_kk">Berkas Scan KK</label>
                            <input type="file" accept=".pdf" class="form-control @error('berkas_file_kk') is-invalid @enderror" name="berkas_file_kk" placeholder="Masukkan Berkas Scan KTP  ..." value="{{ old('berkas_file_kk') }}">
                                @error('berkas_file_kk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    {!! Form::hidden("submenu_id", null, ["id"=>"submenu_id"]) !!}
                    {!! Form::hidden("menu_id", null, ["id"=>"menu_id"]) !!}
                    <a href="javascript:void(0)" class="btn btn-white save">Save</a>
                    <button type="submit" class="btn btn-link text-white ml-auto" style="display: none" id="process"></button>
                    <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
