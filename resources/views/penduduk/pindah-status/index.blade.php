@extends('layouts.app')

@section('title', 'Penduduk Mutasi')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

<style>
    .table th, .table td {
        padding: 5px;
    }
    .card .table td, .card .table th {
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Penduduk Mutasi</h2>
                                <p class="mb-0 text-sm">Kelola Penduduk</p>
                            </div>
                            <div class="mb-3">
                                <a href="javascript:void(0)" onclick="createMutasi()" class="btn btn-success mb-3 float-right" title="Tambah" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Mutasi</a>
                                <a href="{{ route("penduduk.change") }}?page={{ request('page') }}" class="btn btn-success mr-2" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                                <a target="_blank" href="{{ route('penduduk.print_all_mutasi') }}" data-toggle="tooltip" class="mr-2 btn btn-secondary" title="Cetak"><i class="fas fa-print"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('form-search')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" action="{{ URL::current() }}" method="GET">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Cari ...." type="search" name="cari" value="{{ request('cari') }}">
        </div>
    </div>
</form>
@endsection

@section('form-search-mobile')
<form class="mt-4 mb-3 d-md-none" action="{{ URL::current() }}" method="GET">
    <div class="input-group input-group-rounded input-group-merge">
        <input type="search" name="cari" class="form-control form-control-rounded form-control-prepended" placeholder="cari" aria-label="Search" value="{{ request('cari') }}">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <span class="fa fa-search"></span>
            </div>
        </div>
    </div>
</form>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card shadow">
    <div class="card-body">
        <div class="text-center mb-3">
            <form action="{{ URL::current() }}" method="GET">
                <div class="row">
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">Tanggal</label>
                        <input class="form-control-sm" type="date" name="tanggal" id="tanggal" value="{{ date('Y-m-d',strtotime(request('tanggal')))  }}">
                    </div>
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">Dusun/RW/RT</label>
                        <input class="form-control-sm" type="text" name="dusun" id="dusun" value="{{ request('dusun') }}">
                    </div>
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">NIK</label>
                        <input class="form-control-sm" type="text" name="nik" id="nik" value="{{ request('nik') }}">
                    </div>
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">Nama Penduduk</label>
                        <input class="form-control-sm" type="text" name="nama" id="nama" value="{{ request('nama') }}">
                    </div>
                </div>
            </form>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-sm table-striped table-bordered">
                <thead>
                    <th class="text-center">No</th>
                    <th class="text-center">NIK</th>
                    <th class="text-center">KK</th>
                    <th class="text-center">Nama Lengkap</th>
                    <th class="text-center">Nama Kepala Rumah Tangga</th>
                    <th class="text-center">Tanggal Pindah</th>
                    <th class="text-center">Kota</th>
                    <th class="text-center">Kecamatan</th>
                    <th class="text-center">Kelurahan</th>
                    <th class="text-center">Alamat</th>
                    <th class="text-center">Tanggal Mutasi</th>
                    <th class="text-center">Nama Operator</th>
                </thead>
                <tbody>
                    @forelse ($penduduk as $item)
                        <tr>
                            <td class="text-center">{{ ($penduduk->currentpage()-1) * $penduduk->perpage() + $loop->index + 1 }}</td>
                            <td><a href="{{ route('penduduk.show', $item->nik) }}">{{ $item->nik }}</a></td>
                            <td><a href="{{ route('penduduk.keluarga.show', $item->kk) }}">{{ $item->kk }}</a></td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->statusHubunganDalamKeluarga->nama }}</td>
                            <td>{{ date('d-m-Y',strtotime($item->tgl_pindah)) }}</td>
                            <td>{{ $item->kota_tujuan }}</td>
                            <td>{{ $item->kecamatan_tujuan }}</td>
                            <td>{{ $item->kelurahan_tujuan }}</td>
                            <td>{{ $item->alamat_tujuan }}</td>
                            <td>{{ date('d-m-Y',strtotime($item->tgl_mutasi)) }}</td>
                            <td>{{ $item->users_updated->nama }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="15" align="center">Data tidak tersedia</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {{ $penduduk->links('layouts.components.pagination') }}
    </div>
</div>
@include('penduduk.pindah-status.edit')
@endsection

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#tanggal,#dusun,#rt,#rw,#nik,#nama").change(function () {
            $(this).parent().submit();
        });
        $('#pilihan_keluarga').on('change', function(e){
            if($(this).val() == 0) {
                $('#sendiri_pindah').hide();
                $('#all_pindah').show();
            }else{
                $('#sendiri_pindah').show();
                $('#all_pindah').hide();
            }
        })
        $('.cari').on('change', function(e){
            e.preventDefault();
            $.ajax({
                url : baseURL + '/penduduk/cari',
                type: 'GET',
                data: {
                    nik: $("#nomor_induk_penduduk").val()
                },
                before: function () {
                    $("#cari-nik").html(`<img height="20px" src="${baseURL}/storage/loading.gif" alt="">`);
                },
                success: function(response){
                    if (response.nik) {
                        $("#nomor_induk_penduduk").html(`cari`);
                        $("#nama_penduduk").val(response.nama);
                        $('#tgl_pindah').focus();
                    }
                }
            })
        })
        $('.cari_kk').on('change', function(e){
            e.preventDefault();
            $.ajax({
                url : baseURL + '/penduduk/cari-kk',
                type: 'GET',
                data: {
                    nik: $("#nomor_kk_penduduk").val()
                },
                before: function () {
                    $("#cari-nik").html(`<img height="20px" src="${baseURL}/storage/loading.gif" alt="">`);
                },
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let dataPengikut = "";
                    for(let i=0; i < data.length; i++){
                        dataPengikut += "<tr><td>" + data[i].nomor_urut_dalam_kk + "</td><td>" + data[i].nik + "</td><td>" + data[i].kk + "</td><td>" + data[i].nama + "</td></tr>";
                    }
                    $('#anggota_keluarga').html(dataPengikut)
                }
            })
        })
        $('.save').on('click', function(e){
            e.preventDefault();
            let nik = $('#nomor_induk_penduduk').val();
            let tgl_pindah = $('#tgl_pindah').val();
            let kota_tujuan = $('#kota_tujuan').val();
            let kecamatan_tujuan = $('#kecamatan_tujuan').val();
            let kelurahan_tujuan = $('#kelurahan_tujuan').val();
            let alamat_tujuan = $('#alamat_tujuan').val();
            if (kota_tujuan == undefined || kota_tujuan == "") {
                alertFail('Harap kota tujuan diisi');
                return false;
            }else if(kecamatan_tujuan == undefined || kecamatan_tujuan == "") {
                alertFail('Harap kecamatan tujuan diisi');
                return false;
            }else if(kelurahan_tujuan == undefined || kelurahan_tujuan == "") {
                alertFail('Harap kelurahan tujuan diisi');
                return false;
            }else if(alamat_tujuan == undefined || alamat_tujuan == "") {
                alertFail('Harap alamat tujuan diisi');
                return false;
            }else {
               $('body #process').click();
               return true;
            }
        })

        $(window).keydown(function(event){
            if( (event.keyCode == 13) && (validationFunction() == false) ) {
            event.preventDefault();
            return false;
            }
        });

        $('#provinsi_id').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kabupaten')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kota_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kota_tujuan += '<option value='+ data[i].city_id +'>' + data[i].city_name+ '</option>';
                    }
                   $("#kota_tujuan").html(kota_tujuan);
                }
            });
        })

        $('#kota_tujuan').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kecamatan')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kecamatan_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kecamatan_tujuan += '<option value='+ data[i].dis_id +'>' + data[i].dis_name+ '</option>';
                    }
                   $("#kecamatan_tujuan").html(kecamatan_tujuan);
                }
            });
        })

        $('#kecamatan_tujuan').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kelurahan')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kecamatan_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kecamatan_tujuan += '<option value='+ data[i].subdis_id +'>' + data[i].subdis_name+ '</option>';
                    }
                   $("#kelurahan_tujuan").html(kecamatan_tujuan);
                }
            });

        })

    });
    var createMutasi = function() {
        $('#modal-edit #nik').val("");
        $('#modal-edit #nama_penduduk').val("");
        $('#modal-edit #tgl_pindah').val("");
        $('#modal-edit #kota').val("");
        $('#modal-edit #kecamatan').val("");
        $('#modal-edit #kelurahan').val("");
        $('#modal-edit #alamat').val("");
        $('#modal-edit').modal("show");
    }

    function validationFunction() {
        let good= true;
        let nik = $('#nomor_induk_penduduk').val();
        let tgl_pindah = $('#tgl_pindah').val();
        let kota_tujuan = $('#kota_tujuan').val();
        let kecamatan_tujuan = $('#kecamatan_tujuan').val();
        let kelurahan_tujuan = $('#kelurahan_tujuan').val();
        let alamat_tujuan = $('#alamat_tujuan').val();
        if (kota_tujuan == undefined || kota_tujuan == "") {
            alertFail('Harap kota tujuan diisi');
            good= false;
        }else if(kecamatan_tujuan == undefined || kecamatan_tujuan == "") {
            alertFail('Harap kecamatan tujuan diisi');
            good= false;
        }else if(kelurahan_tujuan == undefined || kelurahan_tujuan == "") {
            alertFail('Harap kelurahan tujuan diisi');
            good= false;
        }else if(alamat_tujuan == undefined || alamat_tujuan == "") {
            alertFail('Harap alamat tujuan diisi');
            good= false;
        }
        if(good) {
            return true;
        }
        return false;
    }
</script>
@endpush
