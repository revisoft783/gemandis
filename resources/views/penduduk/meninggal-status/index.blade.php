@extends('layouts.app')

@section('title', 'Penduduk Meninggal')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">

<style>
    .table th, .table td {
        padding: 5px;
    }
    .card .table td, .card .table th {
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Penduduk Meninggal</h2>
                                <p class="mb-0 text-sm">Kelola Penduduk</p>
                            </div>
                            <div class="mb-3">
                                <a href="javascript:void(0)" onclick="createMutasi()" class="btn btn-success mb-3 float-right" title="Tambah" data-toggle="modal"><i class="fas fa-plus"></i> Tambah Meninggal</a>
                                <a href="{{ route("penduduk.change") }}?page={{ request('page') }}" class="btn btn-success mr-2" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                                <a target="_blank" href="{{ route('penduduk.print_all_mutasi_meninggal') }}" data-toggle="tooltip" class="btn btn-secondary mr-2" title="Cetak"><i class="fas fa-print"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('form-search')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" action="{{ URL::current() }}" method="GET">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Cari ...." type="search" name="cari" value="{{ request('cari') }}">
        </div>
    </div>
</form>
@endsection

@section('form-search-mobile')
<form class="mt-4 mb-3 d-md-none" action="{{ URL::current() }}" method="GET">
    <div class="input-group input-group-rounded input-group-merge">
        <input type="search" name="cari" class="form-control form-control-rounded form-control-prepended" placeholder="cari" aria-label="Search" value="{{ request('cari') }}">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <span class="fa fa-search"></span>
            </div>
        </div>
    </div>
</form>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card shadow">
    <div class="card-body">
        <div class="text-center mb-3">
            <form action="{{ URL::current() }}" method="GET">
                <div class="row">
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">Tanggal</label>
                        <input class="form-control-sm" type="date" name="tanggal" id="tanggal" value="{{ date('Y-m-d',strtotime(request('tanggal')))  }}">
                    </div>
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">Dusun/RW/RT</label>
                        <input class="form-control-sm" type="text" name="dusun" id="dusun" value="{{ request('dusun') }}">
                    </div>
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">NIK</label>
                        <input class="form-control-sm" type="text" name="nik" id="nik" value="{{ request('nik') }}">
                    </div>
                    <div class="form-group col-3">
                        <label class="form-control-label" for="nomor_induk_penduduk" style="color: black">Nama Penduduk</label>
                        <input class="form-control-sm" type="text" name="nama" id="nama" value="{{ request('nama') }}">
                    </div>
                </div>
            </form>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-sm table-striped table-bordered">
                <thead>
                    <th class="text-center">No</th>
                    <th class="text-center">NIK</th>
                    <th class="text-center">KK</th>
                    <th class="text-center">Nama Lengkap</th>
                    <th class="text-center">Nama Kepala Rumah Tangga</th>
                    <th class="text-center">Tanggal Meninggal</th>
                    <th class="text-center">Waktu Meninggal</th>
                    <th class="text-center">Tempat Meninggal</th>
                    <th class="text-center">Penyebab meninggal</th>
                    <th class="text-center">Tanggal Mutasi</th>
                    <th class="text-center">Nama Operator</th>
                </thead>
                <tbody>
                    @forelse ($penduduk as $item)
                        <tr>
                            <td class="text-center">{{ ($penduduk->currentpage()-1) * $penduduk->perpage() + $loop->index + 1 }}</td>
                            <td><a href="{{ route('penduduk.show', $item->nik) }}">{{ $item->nik }}</a></td>
                            <td><a href="{{ route('penduduk.keluarga.show', $item->kk) }}">{{ $item->kk }}</a></td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->statusHubunganDalamKeluarga->nama }}</td>
                            <td>{{ date('d-m-Y',strtotime($item->tgl_meninggal)) }}</td>
                            <td>{{ date('d-m-Y',strtotime($item->jam_meninggal)) }}</td>
                            <td>{{ $item->tempat_meninggal }}</td>
                            <td>{{ $item->penyebab_meninggal }}</td>
                            <td>{{ date('d-m-Y',strtotime($item->tgl_mutasi)) }}</td>
                            <td>{{ $item->users_updated->nama }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="15" align="center">Data tidak tersedia</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {{ $penduduk->links('layouts.components.pagination') }}
    </div>
</div>
@include('penduduk.meninggal-status.edit')
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $("#tanggal,#dusun,#rt,#rw,#nik,#nama").change(function () {
            $(this).parent().submit();
        });
        $('.cari').on('change', function(e){
            e.preventDefault();
            $.ajax({
                url : baseURL + '/penduduk/cari',
                type: 'GET',
                data: {
                    nik: $("#nomor_induk_penduduk").val(),
                    status: 1,
                },
                before: function () {
                    $("#cari-nik").html(`<img height="20px" src="${baseURL}/storage/loading.gif" alt="">`);
                },
                success: function(response){
                    if (response.nik) {
                        $("#nomor_induk_penduduk").html(`cari`);
                        $("#nama_penduduk").val(response.nama);
                        $('#tgl_pindah').focus();
                    }
                    if (!response.status)
                    {
                        alertFail(response.message);
                    }
                }
            })
        })

        $('.save').on('click', function(e){
            e.preventDefault();
            let nik = $('#nomor_induk_penduduk').val();
            let tgl_meninggal = $('#tgl_meninggal').val();
            let jam_meninggal = $('#jam_meninggal').val();
            let tempat_meninggal = $('#tempat_meninggal').val();
            let penyebab_meninggal = $('#penyebab_meninggal').val();
            if (tgl_meninggal == undefined || tgl_meninggal == "") {
                alertFail('Harap Tanggal meninggal diisi');
                return false;
            }else if(jam_meninggal == undefined || jam_meninggal == "") {
                alertFail('Harap Jam meninggal diisi');
                return false;
            }else if(tempat_meninggal == undefined || tempat_meninggal == "") {
                alertFail('Harap Tempat meninggal diisi');
                return false;
            }else if(penyebab_meninggal == undefined || penyebab_meninggal == "") {
                alertFail('Harap Penyebab meninggal diisi');
                return false;
            }else {
                $('#process').click();
                return true;
            }
        })

        $(window).keydown(function(event){
            if( (event.keyCode == 13) && (validationFunction() == false) ) {
            event.preventDefault();
            return false;
            }
        });
    });
    var createMutasi = function() {
        $('#modal-edit #nik').val("");
        $('#modal-edit #nama_penduduk').val("");
        $('#modal-edit #tgl_meninggal').val("");
        $('#modal-edit #jam_meninggal').val("");
        $('#modal-edit #tempat_meninggal').val("");
        $('#modal-edit #penyebab_meninggal').val("");
        $('#modal-edit').modal("show");
    }

    function validationFunction() {
        let good= true;
        let nik = $('#nomor_induk_penduduk').val();
        let tgl_meninggal = $('#tgl_meninggal').val();
        let jam_meninggal = $('#jam_meninggal').val();
        let tempat_meninggal = $('#tempat_meninggal').val();
        let penyebab_meninggal = $('#penyebab_meninggal').val();
        if (tgl_meninggal == undefined || tgl_meninggal == "") {
            alertFail('Harap Tanggal meninggal diisi');
            good = false;
        }else if(jam_meninggal == undefined || jam_meninggal == "") {
            alertFail('Harap Jam meninggal diisi');
            good = false;
        }else if(tempat_meninggal == undefined || tempat_meninggal == "") {
            alertFail('Harap Tempat meninggal diisi');
            good = false;
        }else if(penyebab_meninggal == undefined || penyebab_meninggal == "") {
            alertFail('Harap Penyebab meninggal diisi');
            good = false;
        }
        if(good) {
            return true;
        }
        return false;
    }
</script>
@endpush
