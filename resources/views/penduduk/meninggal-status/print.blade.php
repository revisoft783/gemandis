@extends('layouts.cetak')
@section('title','Data Penduduk Calon Pemilih')

@section('content')
<div class="text-center mb-3" style="margin-top:20px">
    <h6 class="font-weight-bold">Data Penduduk Meninggal</h6>
    <p>{{ tgl(date('Y-m-d')) }}</p>
</div>
<table class="table table-bordered">
    <thead>
        <th class="text-center">No</th>
        <th class="text-center">NIK</th>
        <th class="text-center">KK</th>
        <th class="text-center">Nama Lengkap</th>
        <th class="text-center">Nama Kepala Rumah Tangga</th>
        <th class="text-center">Tanggal Meninggal</th>
        <th class="text-center">Waktu Meninggal</th>
        <th class="text-center">Tempat Meninggal</th>
        <th class="text-center">Penyebab meninggal</th>
        <th class="text-center">Tanggal Mutasi</th>
        <th class="text-center">Nama Operator</th>
    </thead>
    <tbody>
        @forelse ($penduduk as $item)
            <tr>
                <td class="text-center">{{ $loop->iteration }}</td>
                <td><a href="{{ route('penduduk.show', $item->nik) }}">{{ $item->nik }}</a></td>
                <td><a href="{{ route('penduduk.keluarga.show', $item->kk) }}">{{ $item->kk }}</a></td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->statusHubunganDalamKeluarga->nama }}</td>
                <td>{{ date('d-m-Y',strtotime($item->tgl_meninggal)) }}</td>
                <td>{{ date('d-m-Y',strtotime($item->jam_meninggal)) }}</td>
                <td>{{ $item->tempat_meninggal }}</td>
                <td>{{ $item->penyebab_meninggal }}</td>
                <td>{{ date('d-m-Y',strtotime($item->tgl_mutasi)) }}</td>
                <td>{{ $item->users_updated->nama }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="15" align="center">Data tidak tersedia</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection
