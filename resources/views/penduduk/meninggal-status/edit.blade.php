<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="modal-edit" aria-hidden="true">
    <div class="modal-dialog modal-primary modal-dialog-centered modal-lg" role="document">
        <div class="modal-content bg-gradient-primary">
            <form autocomplete="off" action="{{ route('penduduk.update_mutasi_meninggal')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h2 class="modal-title" id="modal-title-delete">Add Mutasi Penduduk</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="form-control-label" for="nomor_induk_penduduk" style="color: white">NIK Penduduk</label>
                            <div class="input-group-prepend">
                                <input type="search" name="nomor_induk_penduduk" id="nomor_induk_penduduk" class="form-control form-control-rounded form-control-prepended cari" placeholder="nomor_induk_penduduk" aria-label="nomor_induk_penduduk" value="{{ request('nomor_induk_penduduk') }}">
                                <div class="input-group-text">
                                    <span class="fa fa-search"></span>
                                </div>
                            </div>
                            @error('nomor_induk_penduduk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="nama_penduduk" style="color: white">Nama Penduduk</label>
                            <input type="text" class="form-control @error('nama_penduduk') is-invalid @enderror" id="nama_penduduk" name="nama_penduduk" placeholder="Masukkan Nama Penduduk ..." readonly>
                            @error('nama_penduduk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="tgl_meninggal" style="color: white">Tanggal Meninggal</label>
                            <input type="date" class="form-control @error('tgl_meninggal') is-invalid @enderror" name="tgl_meninggal" id="tgl_meninggal" placeholder="Masukkan Tanggal Meninggal ..." value="{{ date('Y-m-d',strtotime(request('tanggal')))  }}">
                            @error('tgl_meninggal')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="jam_meninggal" style="color: white">Jam Meninggal</label>
                            <input type="time" class="form-control @error('jam_meninggal') is-invalid @enderror" id="jam_meninggal" name="jam_meninggal" placeholder="Masukkan Jam meninggal ...">
                            @error('jam_meninggal')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="tempat_meninggal" style="color: white">Tempat Meninggal</label>
                            <input type="text" class="form-control @error('tempat_meninggal') is-invalid @enderror" id="tempat_meninggal" name="tempat_meninggal" placeholder="Masukkan tempat meninggal ...">
                            @error('tempat_meninggal')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label class="form-control-label" for="penyebab_meninggal" style="color: white">Penyebab Meninggal</label>
                            <input type="text" class="form-control @error('penyebab_meninggal') is-invalid @enderror" id="penyebab_meninggal" name="penyebab_meninggal" placeholder="Masukkan penyebab meninggal ...">
                            @error('penyebab_meninggal')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-12">
                            <label class="form-control-label"  style="color: white" for="berkas_file">Berkas Scan Akta Meninggal</label>
                            <input type="file" accept=".pdf" class="form-control @error('berkas_file') is-invalid @enderror" name="berkas_file" placeholder="Masukkan Berkas Scan Akta Meninggal  ..." value="{{ old('berkas_file') }}">
                                @error('berkas_file')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-12">
                            <label class="form-control-label"  style="color: white" for="berkas_file_ktp">Berkas Scan KTP</label>
                            <input type="file" accept=".pdf" class="form-control @error('berkas_file_ktp') is-invalid @enderror" name="berkas_file_ktp" placeholder="Masukkan Berkas Scan KTP  ..." value="{{ old('berkas_file_ktp') }}">
                                @error('berkas_file_ktp')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group col-12">
                            <label class="form-control-label"  style="color: white" for="berkas_file_kk">Berkas Scan KK</label>
                            <input type="file" accept=".pdf" class="form-control @error('berkas_file_kk') is-invalid @enderror" name="berkas_file_kk" placeholder="Masukkan Berkas Scan KK  ..." value="{{ old('berkas_file_kk') }}">
                                @error('berkas_file_kk')<span class="invalid-feedback font-weight-bold">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    {!! Form::hidden("submenu_id", null, ["id"=>"submenu_id"]) !!}
                    {!! Form::hidden("menu_id", null, ["id"=>"menu_id"]) !!}
                    <a href="javascript:void(0)" class="btn btn-white save">Save</a>
                    <button type="submit" class="btn btn-link text-white ml-auto" style="display: none" id="process"></button>
                    <button type="button" class="btn btn-link text-white ml-auto" data-dismiss="modal">Close</button>
                </div>
            </form>

        </div>
    </div>
</div>
