@php
    $menu = DB::table('artikel')->where('menu','!=',null)->select('menu','submenu','sub_submenu')->get()->groupBy('menu');
@endphp
<header id="header">
    <div class="container-fluid d-flex justify-content-center">
        <div class="logo mr-auto d-lg-none d-md-block">
            <!-- Uncomment below if you prefer to use an image logo -->
            <h2>
                <a href="{{ url('') }}">
                    <img src="{{ asset(Storage::url($desa->logo2)) }}" alt="" class="img-fluid">
                    Desa {{ $desa->nama_desa }}
                </a>
            </h2>
        </div>

        <!-- Navbar -->
        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li>
                    <a class="text-uppercase" href="{{ route('home.index') }}">Beranda</a>
                </li>
                @foreach ($menu as $menu => $submenu)
                    <li class="{{ count($submenu->where('submenu','!=', null)) > 0 ? 'drop-down' : '' }}">
                        <a class="text-uppercase" href="/{{ Str::slug($menu) }}">{{ $menu }}</a>
                        @if (count($submenu->where('submenu','!=', null)) > 0)
                            <ul>
                                @foreach ($submenu->where('submenu','!=', null)->groupBy('submenu') as $submenu => $sub_submenu)
                                    <li class="{{ count($sub_submenu->where('submenu','!=', null)->where('sub_submenu','!=', null)) > 0 ? 'drop-down' : '' }}">
                                        <a href="/{{ Str::slug($menu) }}/{{ Str::slug($submenu) }}">{{ $submenu }}</a>
                                        @if (count($sub_submenu->where('submenu','!=', null)->where('sub_submenu','!=', null)) > 0)
                                            <ul>
                                                @foreach ($sub_submenu->where('submenu','!=', null)->where('sub_submenu','!=', null)->groupBy('sub_submenu') as $sub_submenu => $item)
                                                    <li><a href="/{{ Str::slug($menu) }}/{{ Str::slug($submenu) }}/{{ Str::slug($sub_submenu) }}">{{ $sub_submenu }}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
                <li class="drop-down">
                    <a class="text-uppercase" href="#">Formulir</a>
                    <ul>
                        <li class="drop-down @if (Request::segment(1) == 'layanan-surat') active @endif">
                            <a class="text-uppercase" href="#">biodata</a>
                            <ul>
                                <li class="@if (Request::segment(2) == 'buat-dafduk') active @endif">
                                    <a class="" href="{{ route('buat-dafduk') }}">F-1.01</a>
                                </li>
                                <li class="@if (Request::segment(2) == 'f-1.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-1.04 SKPWNI</a>
                                </li>
                                <li class="@if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-2.01 CAPIL KELAHIRAN</a>
                                </li>
                                <li class="@if (Request::segment(2) == 'f-2.03') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-2.03 SPTJM KEBENARAN DATA KELAHIRAN</a>
                                </li>
                            </ul>
                        </li>
                        <li class="drop-down @if (Request::segment(1) == 'gallery') active @endif">
                            <a class="text-uppercase" href="#">Kelahiran</a>
                            <ul>                                
                                <li class="@if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-2.01 KELAHIRAN</a>
                                </li>
                                <li class="@if (Request::segment(2) == 'f-2.03') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-2.03 SPTJM KEBENARAN DATA KELAHIRAN</a>
                                </li>
                                <li class="@if (Request::segment(2) == 'f-2.04') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-2.04 SPTJM KEBENARAN PASUTRI</a>
                                </li>
                            </ul>
                        </li>
                        <li class="drop-down @if (Request::segment(1) == 'gallery') active @endif">
                            <a class="text-uppercase" href="#">Kematian</a>
                            <ul>                                
                                <li class="@if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-2.01 Kematian</a>
                                </li>
                                <li class="@if (Request::segment(2) == 'f-2.03') active @endif">
                                    <a class="" href="{{ route('gallery') }}">FILE U BUKTI KEMATIAN, KELAHIRAN DAN LAHIR MATI TERBARU</a>
                                </li>                            
                            </ul>
                        </li>
                        <li class="drop-down @if (Request::segment(1) == 'gallery') active @endif">
                            <a class="text-uppercase" href="#">Lahir Mati</a>
                            <ul>                                
                                <li class="@if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">F-2.01 Lahir Mati</a>
                                </li>
                                <li class="@if (Request::segment(2) == 'f-2.03') active @endif">
                                    <a class="" href="{{ route('gallery') }}">SPTJM Lahir Mati</a>
                                </li>                            
                            </ul>
                        </li>
                        <li class="drop-down @if (Request::segment(1) == 'gallery') active @endif">
                            <a class="text-uppercase" href="#">Kartu Keluarga</a>
                            <ul>                                
                                <li class="@if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">KK BARU (F-1.02)</a>
                                </li>
                                <li class="drop-down @if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">KK Perubahan</a>
                                    <ul>
                                        <li class="@if (Request::segment(1) == 'statistik-penduduk') active @endif">
                                            <a class="" href="{{ route('statistik-penduduk') }}">F-1.02 (Formulir Pendaftaran)</a>
                                        </li>
                                        <li class="@if (Request::segment(1) == 'statistik-penduduk') active @endif">
                                            <a class="" href="{{ route('statistik-penduduk') }}">F-1.06 (Pernyataan Perubahan Element Data)</a>
                                        </li>
                                    </ul>                                    
                                </li> 
                                <li class="@if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">KK RUSAK (F-1.02)</a>
                                </li>                           
                            </ul>
                        </li>
                        <li class="drop-down @if (Request::segment(1) == 'statistik-penduduk') active @endif">
                            <a class="text-uppercase" href="#">Penerbitan Surat Keterangan Kependudukan</a>
                            <ul>                                
                                <li class="drop-down @if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">Keterangan Pindah</a>
                                    <ul>
                                        <li>
                                            <a class="" href="{{ route('masuk') }}">F-1.03 SKPWNI</a>
                                        </li>
                                    </ul>                                    
                                </li>
                                <li class="drop-down @if (Request::segment(2) == 'f-2.01') active @endif">
                                    <a class="" href="{{ route('gallery') }}">Keterangan Pindah LN</a>
                                    <ul>
                                        <li>
                                            <a class="" href="{{ route('masuk') }}">F-1.03 SKPLN</a>
                                        </li>
                                    </ul>                                    
                                </li>                           
                            </ul>
                        </li>                                                
                    </ul>
                </li>
                <li class="drop-down">
                    <a class="text-uppercase" href="#">Menu Utama</a>
                    <ul>
                        <li class="@if (Request::segment(1) == 'layanan-surat') active @endif">
                            <a class="" href="{{ route('layanan-surat') }}">Layanan Surat</a>
                        </li>
                        <li class="@if (Request::segment(1) == 'gallery') active @endif">
                            <a class="" href="{{ route('gallery') }}">Gallery</a>
                        </li>
                        <li class="@if (Request::segment(1) == 'statistik-penduduk') active @endif">
                            <a class="" href="{{ route('statistik-penduduk') }}">Statistik Penduduk</a>
                        </li>
                        <li>
                            <a class="" href="{{ route('masuk') }}">Login Admin</a>
                        </li>
                        @auth
                            <li>
                                <a href="{{ route('dashboard') }}">Dashboard Admin</a>
                            </li>
                            <hr class="m-0">
                            <li>
                                <a href="{{ route('keluar') }}" onclick="event.preventDefault(); document.getElementById('form-logout').submit();">Keluar</a>
                            </li>
                        @endguest
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <form id="form-logout" action="{{ route('keluar') }}" method="POST" style="display: none;">
        @csrf
    </form>
</header>
