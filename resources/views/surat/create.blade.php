@extends('layouts.app')

@section('title', 'Tambah Surat')

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('content-header')
<div class="pt-5 pb-8 header pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="shadow card h-100">
                    <div class="border-0 card-header">
                        <div class="text-center d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Tambah Surat</h2>
                                <p class="mb-0 text-sm">Kelola Surat</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ route('surat.index') }}" class="btn btn-success" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="row">
    <div class="col">
        <div class="shadow card bg-secondary h-100">
            <div class="bg-white border-0 card-header">
                <h3 class="mb-0">Tambah Surat</h3>
            </div>
            <div class="card-body">
                <form autocomplete="off" action="{{ route("surat.store") }}" method="post">
                    @csrf
                    <h6 class="heading-small text-muted">Detail Surat</h6>
                    <div class="pl-lg-4">
                        <div class="form-group">
                            <label class="form-control-label">Kode Surat</label>
                            <select name="kode_surat" id="kode_surat" class="form-control">
                                <option value="">Pilih Kode Surat</option>
                                @foreach (App\KodeSurat::all() as $key => $item)
                                    <option value="{{ $item->kode }}">{{ $item->kode }} - {{ $item->nama }} {{ $item->uraian != '-' ? '- '. $item->uraian : ''}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Nama Surat</label>
                            <input class="form-control" name="nama" placeholder="Masukkan Nama Surat">
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Deskripsi</label>
                            <textarea class="form-control" name="deskripsi" placeholder="Masukkan Deskripsi"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Bidang</label>
                            <select name="bidang_id" id="bidang_id" class="form-control">
                                <option value="">Pilih Bidang</option>
                                @foreach (App\Bidang::all() as $key => $item)
                                    <option value="{{ $item->id }}">{{ $item->id }} - {{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Form Surat</label>
                            <input class="form-control" name="form_surat" placeholder="Masukkan tipe Form Surat"></input>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label">Persyaratan</label>
                            <textarea class="form-control" name="persyaratan" placeholder="Masukkan persyaratan untuk membuat surat yang ditujukan untuk warga"></textarea>
                        </div>
                    </div>
                    <h6 class="mt-4 heading-small text-muted">Isian</h6>
                    <div class="pl-lg-4" id="isian">
                        <div class="mb-3 shadow card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <textarea class="form-control" name="isi[]" placeholder="Paragraf ..."></textarea>
                                        </div>
                                        <div class="form-group">
                                            <select name="isian[]" class="form-control" style="display: none;">
                                                <option value="">Pilih Isian</option>
                                                @foreach ($isian as $key => $item)
                                                    <option value="{{ $item }}">{{ str_replace('_',' ',$item) }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <select name="jenis_isi[]" class="form-control">
                                                <option value="1">Paragraf</option>
                                                <option value="2">Kalimat</option>
                                                <option value="3">Isian</option>
                                                <option value="4">Upload File</option>
                                                <option value="5">Subjudul</option>
                                            </select>
                                        </div>
                                        <div class="mt-3 text-right">
                                            <input type="checkbox" name="tampil[]" value="1" style="transform: scale(1.5); margin-right: 15px" data-toggle="tooltip" title="Centang untuk ditampilkan pada form buat surat">
                                            <input type="hidden" name="tampilkan[]" value="0">
                                            <a class="mb-1 mr-2 bantuan" href="{{ url('img/bantuan-paragraf.png') }}" data-fancybox><i class="fas fa-question-circle text-blue" title="Bantuan" data-toggle="tooltip"></i></a>
                                            <button class="mb-1 btn btn-sm btn-success atas-isian" data-toggle="tooltip" title="Pindahkan Ke Atas" type="button"><i  class="fas fa-arrow-up"></i></button>
                                            <button class="mb-1 btn btn-sm btn-success bawah-isian" data-toggle="tooltip" title="Pindahkan Ke Bawah" type="button"><i class="fas fa-arrow-down"></i></button>
                                            <button class="mb-1 btn btn-sm btn-primary tambah-isian" data-toggle="tooltip" title="Tambah Isian" type="button"><i class="fas fa-plus"></i></button>
                                            <button class="mb-1 btn btn-sm btn-danger hapus-isian" data-toggle="tooltip" title="Hapus Isian Ini" type="button"><i class="fas fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h6 class="heading-small text-muted">Alat</h6>
                    <div class="pl-lg-4">
                        <a class="btn btn-primary btn-sm"  href="{{ url('img/bantuan-paragraf-kalimat-isian.png') }}" data-fancybox><i class="fas fa-question-circle" data-toggle="tooltip"></i> Bantuan</a>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="tampilkan_surat_ini" name="tampilkan_surat_ini" value="1">
                            <input type="hidden" name="tampilkan_surat" id="tampilkan_surat" value="0">
                            <label class="custom-control-label" for="tampilkan_surat_ini">Tampilkan surat ini untuk warga yang ingin mencetak surat keterangan ini</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="tampilkan_perihal" name="tampilkan_perihal" value="1">
                            <input type="hidden" name="perihal" id="perihal" value="0">
                            <label class="custom-control-label" for="tampilkan_perihal">Perihal</label> <a href="{{ url('img/bantuan-perihal.png') }}" data-fancybox data-caption="Akan menampilkan surat seperti ini"><i class="fas fa-question-circle text-blue" title="Bantuan" data-toggle="tooltip"></i></a>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="tampilkan_data_kades" name="tampilkan_data_kades" value="1">
                            <input type="hidden" name="data_kades" id="data_kades" value="0">
                            <label class="custom-control-label" for="tampilkan_data_kades">Data Kades</label> <a href="{{ url('img/bantuan-data-kades.png') }}" data-fancybox data-caption="Akan menampilkan data kepala desa"><i class="fas fa-question-circle text-blue" title="Bantuan" data-toggle="tooltip"></i></a>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="tampilkan_tanda_tangan_bersangkutan" name="tampilkan_tanda_tangan_bersangkutan" value="1">
                            <input type="hidden" name="tanda_tangan_bersangkutan" id="tanda_tangan_bersangkutan" value="0">
                            <label class="custom-control-label" for="tampilkan_tanda_tangan_bersangkutan">Tanda tangan bersangkutan</label> <a href="{{ url('img/bantuan-tanda-tangan-bersangkutan.png') }}" data-fancybox data-caption="Akan menampilkan tanda tangan yang bersangkutan"><i class="fas fa-question-circle text-blue" title="Bantuan" data-toggle="tooltip"></i></a>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="fp" name="fp" value="1">
                            <input type="hidden" name="fp" id="fp" value="0">
                            <label class="custom-control-label" for="fp">Cetak surat format landscape </label>
                        </div>
                    </div>
                    <div class="mt-3 form-group">
                        <button type="submit" class="btn btn-primary btn-block" id="simpan">SIMPAN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('js/surat.js') }}"></script>
<script src="{{ asset('js/form.js') }}"></script>
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#kode_surat').select2({
            placeholder: "Pilih Kode Surat",
            allowClear: true
        });
        $('#bidang_id').select2(
            placeholder: "Pilih Bidang Surat",
            allowClear: true
        );
    });
</script>
@endpush
