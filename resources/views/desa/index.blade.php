@extends('layouts.app')

@section('title')
Identitas Desa
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}">
@endsection

@section('content-header')

<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">

    <!-- Mask -->
    <!-- Header container -->
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-lg-7 col-md-10">
                <h1 class="display-2 text-white">Desa {{ $desa->nama_desa }}</h1>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<form action="{{ route('update-desa', $desa) }}" method="POST">
    @include('layouts.components.alert')
    <div class="row">
        @csrf @method('patch')
        <div class="col-xl-4 order-xl-2 mb-3 mb-xl-0">
            <div class="card card-profile shadow">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="{{asset('api/gambar/'.str_replace('/', '&', $desa->logo))}}" data-fancybox>
                                <img id="logo" src="{{asset('api/gambar/'.str_replace('/', '&', $desa->logo))}}" alt="" class="rounded-circle" style="height: 150px; width: 150px; object-fit: cover">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-md-4 pb-0 pb-md-4">
                    <a id="btn-ganti-logo" href="#input-logo" class="btn btn-sm btn-default mt-5"><span class="fas fa-camera"></span> Ganti</a>
                </div>
                <div class="card-body pt-0 pt-md-4 pt-5">
                    <div class="text-center">
                        <h3>
                            Desa {{ $desa->nama_desa }}
                        </h3>
                        <div class="h5 font-weight-300">
                            Kec. {{ $desa->nama_kecamatan }}, Kab. {{ $desa->nama_kabupaten }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-profile shadow mt-5">
                <div class="row justify-content-center">
                    <div class="col-lg-3 order-lg-2">
                        <div class="card-profile-image">
                            <a href="{{asset('api/gambar/'.str_replace('/', '&', $desa->logo_header))}}" data-fancybox>
                                <img id="logo_header" src="{{asset('api/gambar/'.str_replace('/', '&', $desa->logo_header))}}" alt="" class="rounded-circle" style="height: 150px; width: 150px; object-fit: cover">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-header text-center border-0 pt-md-4 pb-0 pb-md-4">
                    <a id="btn-ganti-logo-header" href="#input-logo-header" class="btn btn-sm btn-default mt-5"><span class="fas fa-camera"></span> Ganti</a>
                </div>
            </div>
            <div class="card shadow mt-3">
                <div class="card-header font-weight-bold">
                    Tautan <i class="fas fa-link"></i>
                </div>
                <div class="card-body bg-secondary">
                    <div class="form-group">
                        <label class="form-control-label" for="link_website">Website</label>
                        <textarea name="link_website" id="link_website" class="form-control form-control-alternative @error('link_website') is-invalid @enderror" placeholder="Masukkan link website desa">{{ old('link_website',$desa->link_website) }}</textarea>
                        @error('link_website')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="email">Email</label>
                        <textarea name="email" id="email" class="form-control form-control-alternative @error('email') is-invalid @enderror" placeholder="Masukkan email desa">{{ old('email',$desa->email) }}</textarea>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="link_facebook">Facebook</label>
                        <textarea name="link_facebook" id="link_facebook" class="form-control form-control-alternative @error('link_facebook') is-invalid @enderror" placeholder="Masukkan link facebook">{{ old('link_facebook',$desa->link_facebook) }}</textarea>
                        @error('link_facebook')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="link_instagram">Instagram</label>
                        <textarea name="link_instagram" id="link_instagram" class="form-control form-control-alternative @error('link_instagram') is-invalid @enderror" placeholder="Masukkan link instagram">{{ old('link_instagram',$desa->link_instagram) }}</textarea>
                        @error('link_instagram')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="link_twitter">Twitter</label>
                        <textarea name="link_twitter" id="link_twitter" class="form-control form-control-alternative @error('link_twitter') is-invalid @enderror" placeholder="Masukkan link twitter">{{ old('link_twitter',$desa->link_twitter) }}</textarea>
                        @error('link_twitter')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="link_youtube">Youtube</label>
                        <textarea name="link_youtube" id="link_youtube" class="form-control form-control-alternative @error('link_youtube') is-invalid @enderror" placeholder="Masukkan link youtube">{{ old('link_youtube',$desa->link_youtube) }}</textarea>
                        @error('link_youtube')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="link_maps">Maps</label>
                        <textarea name="link_maps" id="link_maps" class="form-control form-control-alternative @error('link_maps') is-invalid @enderror" placeholder="Masukkan link maps">{{ old('link_maps',$desa->link_maps) }}</textarea>
                        @error('link_maps')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Informasi Identitas Desa</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                        <div class="form-group">
                            <label class="form-control-label" for="nama_desa">Nama Desa</label>
                            <input name="nama_desa" type="text" id="nama_desa" class="form-control form-control-alternative @error('nama_desa') is-invalid @enderror" placeholder="Masukkan Nama Desa" value="{{ old('nama_desa',$desa->nama_desa) }}">
                            @error('nama_desa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="nama_kecamatan">Nama Kecamatan</label>
                            <input name="nama_kecamatan" type="text" id="nama_kecamatan" class="form-control form-control-alternative @error('nama_kecamatan') is-invalid @enderror" placeholder="Masukkan Nama Kecamatan" value="{{ old('nama_kecamatan',$desa->nama_kecamatan) }}">
                            @error('nama_kecamatan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="nama_kabupaten">Nama Kabupaten</label>
                            <input name="nama_kabupaten" type="text" id="nama_kabupaten" class="form-control form-control-alternative @error('nama_kabupaten') is-invalid @enderror" placeholder="Masukkan Nama Kabupaten" value="{{ old('nama_kabupaten',$desa->nama_kabupaten) }}">
                            @error('nama_kabupaten')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="nama_provinsi">Nama Provinsi</label>
                            <input name="nama_provinsi" type="text" id="nama_provinsi" class="form-control form-control-alternative @error('nama_provinsi') is-invalid @enderror" placeholder="Masukkan Nama Provinsi" value="{{ old('nama_provinsi',$desa->nama_provinsi) }}">
                            @error('nama_provinsi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="kode_provinsi">Kode Provinsi</label>
                                    <input name="kode_provinsi" maxlength="2" onkeypress="return hanyaAngka(event);" type="text" id="kode_provinsi" class="form-control form-control-alternative @error('kode_provinsi') is-invalid @enderror" placeholder="Masukkan Kode Provinsi" value="{{ old('kode_provinsi',$desa->kode_provinsi) }}">
                                    @error('kode_provinsi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="kode_kabupaten">Kode Kabupaten</label>
                                    <input name="kode_kabupaten" maxlength="4" onkeypress="return hanyaAngka(event);" type="text" id="kode_kabupaten" class="form-control form-control-alternative @error('kode_kabupaten') is-invalid @enderror" placeholder="Masukkan Kode Kabupaten" value="{{ old('kode_kabupaten',$desa->kode_kabupaten) }}">
                                    @error('kode_kabupaten')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="kode_kecamatan">Kode Kecamatan</label>
                                    <input name="kode_kecamatan" maxlength="6" onkeypress="return hanyaAngka(event);" type="text" id="kode_kecamatan" class="form-control form-control-alternative @error('kode_kecamatan') is-invalid @enderror" placeholder="Masukkan Kode Kecamatan" value="{{ old('kode_kecamatan',$desa->kode_kecamatan) }}">
                                    @error('kode_kecamatan')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="form-control-label" for="kode_desa">Kode Desa</label>
                                    <input name="kode_desa" maxlength="10" onkeypress="return hanyaAngka(event);" type="text" id="kode_desa" class="form-control form-control-alternative @error('kode_desa') is-invalid @enderror" placeholder="Masukkan Kode Desa" value="{{ old('kode_desa',$desa->kode_desa) }}">
                                    @error('kode_desa')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="kodepos">Kode Pos</label>
                            <input name="kodepos" onkeypress="return hanyaAngka(event);" type="text" id="kodepos" class="form-control form-control-alternative @error('kodepos') is-invalid @enderror" placeholder="Masukkan Kode Pos" value="{{ old('kodepos',$desa->kodepos) }}">
                            @error('kodepos')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="alamat">Alamat</label>
                            <input name="alamat" type="text" id="alamat" class="form-control form-control-alternative @error('alamat') is-invalid @enderror" placeholder="Masukkan nama desa" value="{{ old('alamat',$desa->alamat) }}">
                            @error('alamat')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="telp">No. Telp</label>
                            <input name="telepon" type="text" id="telepon" class="form-control form-control-alternative @error('telepon') is-invalid @enderror" placeholder="Masukkan nomer telepon kantor" value="{{ old('telepon',$desa->telepon) }}">
                            @error('telepon')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="nama_kepala_desa">Nama Kepala Desa</label>
                            <input name="nama_kepala_desa" type="text" id="nama_kepala_desa" class="form-control form-control-alternative @error('nama_kepala_desa') is-invalid @enderror" placeholder="Masukkan nama desa" value="{{ old('nama_kepala_desa',$desa->nama_kepala_desa) }}">
                            @error('nama_kepala_desa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="alamat_kepala_desa">Alamat Kepala Desa</label>
                            <input name="alamat_kepala_desa" type="text" id="alamat_kepala_desa" class="form-control form-control-alternative @error('alamat_kepala_desa') is-invalid @enderror" placeholder="Masukkan nama desa" value="{{ old('alamat_kepala_desa',$desa->alamat_kepala_desa) }}">
                            @error('alamat_kepala_desa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label class="form-control-label" for="label_header">Header Label</label>
                            <input name="label_header" type="text" id="label_header" class="form-control form-control-alternative @error('label_header') is-invalid @enderror" placeholder="Masukkan label header" value="{{ old('label_header',$desa->label_header) }}">
                            @error('label_header')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="form-control-label" for="label_header2">Header Label 2</label>
                            <input name="label_header2" type="text" id="label_header2" class="form-control form-control-alternative @error('label_header2') is-invalid @enderror" placeholder="Masukkan label header 2" value="{{ old('label_header2',$desa->label_header2) }}">
                            @error('label_header2')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block mt-3">Simpan Perubahan</button>
</form>

<input type="file" name="logo" id="input-logo" style="display: none">
<input type="file" name="logo-header" id="input-logo-header" style="display: none">
@endsection

@push('scripts')
<script src="{{ asset('js/jquery.fancybox.js') }}"></script>

<script>
    $(document).ready(function(){
        $('#btn-ganti-logo').on('click', function () {
            $('#input-logo').click();
        });

        $('#btn-ganti-logo-header').on('click', function () {
            $('#input-logo-header').click();
        });

        $(document).on("submit","form", function () {
            $(this).children("button:submit").attr('disabled','disabled');
            $(this).children("button:submit").html(`<img height="20px" src="{{ url('/storage/loading.gif') }}" alt=""> Loading ...`);
        });

        $('#input-logo').on('change', function () {
            if (this.files && this.files[0]) {
                let formData = new FormData();
                let oFReader = new FileReader();
                formData.append("logo", this.files[0]);
                formData.append("_method", "patch");
                formData.append("_token", "{{ csrf_token() }}");
                formData.append("_status","0");
                oFReader.readAsDataURL(this.files[0]);

                $.ajax({
                    url: "{{ route('update-desa', $desa) }}",
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $('#img-logo').attr('src', "{{ url('/storage/loading.gif') }}");
                    },
                    success: function (data) {
                        if (data.error) {
                            $('#img-logo').attr('src', $("#img-logo").attr('alt'));
                        } else {
                            location.reload();
                        }
                    }
                });
            }
        });

        $('#input-logo-header').on('change', function () {
            if (this.files && this.files[0]) {
                let formData = new FormData();
                let oFReader = new FileReader();
                formData.append("logo_header", this.files[0]);
                formData.append("_method", "patch");
                formData.append("_token", "{{ csrf_token() }}");
                formData.append("_status","1");
                oFReader.readAsDataURL(this.files[0]);

                $.ajax({
                    url: "{{ route('update-desa', $desa) }}",
                    type: 'POST',
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $('#img-logo-header').attr('src', "{{ url('/storage/loading.gif') }}");
                    },
                    success: function (data) {
                        if (data.error) {
                            $('#img-logo-header').attr('src', $("#img-logo-header").attr('alt'));
                        } else {
                            location.reload();
                        }
                    }
                });
            }
        });
    });
</script>
@endpush
