@extends('layouts.app')

@section('title', 'Penduduk Mutasi')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

<style>
    .table th, .table td {
        padding: 5px;
    }
    .card .table td, .card .table th {
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
      overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
      font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg .tg-pb0m{border-color:inherit;text-align:center;vertical-align:bottom}
    .tg .tg-nrix{border-color:inherit;text-align:center;vertical-align:middle}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-za14{border-color:inherit;text-align:left;vertical-align:bottom}
    .tg .tg-baqh{border-color:inherit;text-align:center;vertical-align:middle}
    </style>
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-values-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-values-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Buku Induk Penduduk</h2>
                                <p class="mb-0 text-sm">Kelola Penduduk</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ url("kasipem") }}?page={{ request('page') }}" class="btn btn-secondary mr-2" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                                {{-- <a target="_blank" href="#addPenduduk" data-toggle="tooltip" class="mr-2 btn btn-outline-success" title="Tambah Penduduk"><i class="fas fa-plus"></i> Tambah</a>
                                <a target="_blank" href="{{ route('kasipem.print_buku_induk') }}" data-toggle="tooltip" class="mr-2 btn btn-primary" title="Cetak"><i class="fas fa-print"></i> Cetak</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('form-search')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" action="{{ URL::current() }}" method="GET">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Cari ...." type="search" name="cari" value="{{ request('cari') }}">
        </div>
    </div>
</form>
@endsection

@section('form-search-mobile')
<form class="mt-4 mb-3 d-md-none" action="{{ URL::current() }}" method="GET">
    <div class="input-group input-group-rounded input-group-merge">
        <input type="search" name="cari" class="form-control form-control-rounded form-control-prepended" placeholder="cari" aria-label="Search" value="{{ request('cari') }}">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <span class="fa fa-search"></span>
            </div>
        </div>
    </div>
</form>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card shadow">
    <div class="card-body">
        <div class="table-responsive">
            <div class="row">
                <form class="mt-4 mb-3 ml-3" action="{{ URL::current() }}" method="GET">
                    <div class="input-group input-group-rounded input-group-merge">
                        <input type="date" class="form-control form-control-rounded form-control-prepended" name="cari" id="cari" aria-label="Search" title="Filter Cetak"  placeholder="Masukkan Tanggal ..." value="{{ date('Y-m-d',strtotime(request('cari')))  }}">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-search"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <table class="tg">
                <thead>
                    <tr>
                        <td class="tg-nrix" rowspan="2">NOMOR&nbsp;&nbsp;&nbsp;URUT</td>
                        <td class="tg-nrix" rowspan="2">NO. KK</td>
                        <td class="tg-nrix" rowspan="2">NAMA LENGKAP</td>
                        <td class="tg-nrix" rowspan="2">NIK</td>
                        <td class="tg-nrix" rowspan="2">JENIS KELAMIN</td>
                        <td class="tg-nrix" rowspan="2">TEMPAT/ TANGGAL LAHIR</td>
                        <td class="tg-nrix" rowspan="2">Gol. Darah </td>
                        <td class="tg-nrix" rowspan="2">AGAMA</td>
                        <td class="tg-nrix" rowspan="2">PENDIDIKAN</td>
                        <td class="tg-nrix" rowspan="2">PEKERJAAN</td>
                        <td class="tg-nrix" rowspan="2">ALAMAT</td>
                        <td class="tg-nrix" rowspan="2">STATUS PERKAWINAN</td>
                        <td class="tg-nrix" rowspan="2">TEMPAT DAN TANGGAL&nbsp;&nbsp;&nbsp;DIKELUARKAN</td>
                        <td class="tg-nrix" rowspan="2">STATUS HUB. KELUARGA</td>
                        <td class="tg-nrix" rowspan="2">KEWARGANEGARAAN</td>
                        <td class="tg-nrix" colspan="2">ORANG&nbsp;&nbsp;&nbsp;TUA</td>
                        <td class="tg-nrix" rowspan="2">TGL MULAI TINGGAL DI&nbsp;&nbsp;&nbsp;DESA</td>
                        <td class="tg-nrix" rowspan="2">KET</td>
                    </tr>
                    <tr>
                        <td class="tg-nrix">AYAH</td>
                        <td class="tg-nrix">IBU</td>
                    </tr>
                    <tr>
                        <td class="tg-nrix">1</td>
                        <td class="tg-nrix">2</td>
                        <td class="tg-nrix">3</td>
                        <td class="tg-nrix">4</td>
                        <td class="tg-nrix">5</td>
                        <td class="tg-nrix">6</td>
                        <td class="tg-nrix">7</td>
                        <td class="tg-nrix">8</td>
                        <td class="tg-nrix">9</td>
                        <td class="tg-nrix">10</td>
                        <td class="tg-nrix">11</td>
                        <td class="tg-nrix">12</td>
                        <td class="tg-nrix">13</td>
                        <td class="tg-nrix">14</td>
                        <td class="tg-nrix">15</td>
                        <td class="tg-nrix">16</td>
                        <td class="tg-nrix">17</td>
                        <td class="tg-nrix">18</td>
                        <td class="tg-nrix">19</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                    @endphp
                    @forelse ($penduduks as $value)
                        <tr>
                            <td class="tg-nrix">{{++$i}}</td>
                            <td class="tg-nrix">{{$value->kk}}</td>
                            <td class="tg-nrix">{{$value->nama}}</td>
                            <td class="tg-nrix">{{$value->nik}}</td>
                            <td class="tg-nrix">{{@$value->jenis_kelamin==1?'L':'P'}}</td>
                            <td class="tg-nrix">{{$value->tempat_lahir}},&nbsp;&nbsp;&nbsp;{{tgl($value->tanggal_lahir)}}</td>
                            <td style="tg-nrix">{{$value->darah->golongan ?? '-' }}</td>
                            <td style="tg-nrix">{{$value->agama->nama ?? '-' }}</td>
                            <td style="tg-nrix">{{$value->pendidikan->nama ?? '-' }}</td>
                            <td style="tg-nrix">{{$value->pekerjaan->nama ?? '-' }}</td>
                            <td style="tg-nrix">{{ $value->statusPerkawinan->nama ?? '-' }}</td>
                            <td class="tg-nrix">{{dateFormatTime($value->created_at)}}</td>
                            <td style="tg-nrix">{{ $value->statusHubunganDalamKeluarga->nama ?? '-' }}</td>
                            <td style="tg-nrix">
                                @php
                                    switch ($value->kewarganegaraan) {
                                        case 1:
                                            echo "WNI";
                                            break;
                                        case 2:
                                            echo "WNA";
                                            break;
                                        case 3:
                                            echo "Dua Kewarganegaraan";
                                            break;
                                    }
                                @endphp
                            </td>
                            <td style="tg-nrix">{{ $value->nama_ayah ? $value->nama_ayah : '-' }}</td>
                            <td style="tg-nrix">{{ $value->nama_ibu ? $value->nama_ibu : '-' }}</td>
                            <td class="tg-nrix"> {{dateFormatTime($value->created_at)}} </td>
                            <td class="tg-nrix"> </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="19" align="center">Data tidak tersedia</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {{ $penduduks->links('layouts.components.pagination') }}
    </div>
</div>
@endsection
