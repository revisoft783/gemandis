@extends('layouts.app')

@section('title', 'Penduduk Mutasi')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

<style>
    .table th, .table td {
        padding: 5px;
    }
    .card .table td, .card .table th {
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Buku Induk Penduduk</h2>
                                <p class="mb-0 text-sm">Kelola Penduduk</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ url("kasipem") }}?page={{ request('page') }}" class="btn btn-secondary mr-2" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                                <a target="_blank" href="{{ route('kasipem.print_buku_induk') }}" data-toggle="tooltip" class="mr-2 btn btn-primary" title="Cetak"><i class="fas fa-print"></i> Cetak</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('form-search')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" action="{{ URL::current() }}" method="GET">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Cari ...." type="search" name="cari" value="{{ request('cari') }}">
        </div>
    </div>
</form>
@endsection

@section('form-search-mobile')
<form class="mt-4 mb-3 d-md-none" action="{{ URL::current() }}" method="GET">
    <div class="input-group input-group-rounded input-group-merge">
        <input type="search" name="cari" class="form-control form-control-rounded form-control-prepended" placeholder="cari" aria-label="Search" value="{{ request('cari') }}">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <span class="fa fa-search"></span>
            </div>
        </div>
    </div>
</form>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card shadow">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover table-sm table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">Nama Lengkap/Panggilan</th>
                        <th class="text-center">Jenis Kelamin</th>
                        <th class="text-center">Status Perkawinan</th>
                        <th class="text-center">Tempat Lahir</th>
                        <th class="text-center">Tanggal Lahir</th>
                        <th class="text-center">Agama</th>
                        <th class="text-center">Pendidikan Terakhir</th>
                        <th class="text-center">Pekerjaan</th>
                        <th class="text-center">Dapat Membaca</th>
                        <th class="text-center">Kewarganegara</th>
                        <th class="text-center">Alamat Lengkap</th>
                        <th class="text-center">Kedudukan Dlm Keluarga</th>
                        <th class="text-center">NIK</th>
                        <th class="text-center">Nomor KK</th>
                        <th class="text-center">Ket</th>
                        <th class="text-center">Operator</th>
                        <th class="text-center">Waktu Input</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($penduduk as $item)
                        <tr>
                            <td class="text-center">{{ ($penduduk->currentpage()-1) * $penduduk->perpage() + $loop->index + 1 }}</td>
                            <td>{{$item->nama}}</td>
                            <td>{{@$item->jenis_kelamin==1?'L':'P'}}</td>
                            <td>{{statusPerkawinan($item->status_perkawinan_id)}}</td>
                            <td>{{$item->tempat_lahir}}</td>
                            <td>{{tgl($item->tanggal_lahir)}}</td>
                            <td>{{$item->agama->nama}}</td>
                            <td>{{$item->pendidikan->nama}}</td>
                            <td>{{$item->pekerjaan->nama}}</td>
                            <td></td>
                            <td>{{@$item->kewarganegaraan == 1?"WNI":"WNA"}}</td>
                            <td>{{$item->alamat_sekarang}}</td>
                            <td>{{$item->statusHubunganDalamKeluarga->nama}}</td>
                            <td>{{$item->nik}}</td>
                            <td>{{$item->kk}}</td>
                            <td></td>
                            <td>{{@$item->users_created==null ?"-":$item->users_created->nama}}</td>
                            <td>{{dateFormatTime($item->created_at)}}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="18" align="center">Data tidak tersedia</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {{ $penduduk->links('layouts.components.pagination') }}
    </div>
</div>
@include('penduduk.pindah-status.edit')
@endsection

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#tanggal,#dusun,#rt,#rw,#nik,#nama").change(function () {
            $(this).parent().submit();
        });
        $('#pilihan_keluarga').on('change', function(e){
            if($(this).val() == 0) {
                $('#sendiri_pindah').hide();
                $('#all_pindah').show();
            }else{
                $('#sendiri_pindah').show();
                $('#all_pindah').hide();
            }
        })
        $('.cari').on('change', function(e){
            e.preventDefault();
            $.ajax({
                url : baseURL + '/penduduk/cari',
                type: 'GET',
                data: {
                    nik: $("#nomor_induk_penduduk").val()
                },
                before: function () {
                    $("#cari-nik").html(`<img height="20px" src="${baseURL}/storage/loading.gif" alt="">`);
                },
                success: function(response){
                    if (response.nik) {
                        $("#nomor_induk_penduduk").html(`cari`);
                        $("#nama_penduduk").val(response.nama);
                        $('#tgl_pindah').focus();
                    }
                }
            })
        })
        $('.cari_kk').on('change', function(e){
            e.preventDefault();
            $.ajax({
                url : baseURL + '/penduduk/cari-kk',
                type: 'GET',
                data: {
                    nik: $("#nomor_kk_penduduk").val()
                },
                before: function () {
                    $("#cari-nik").html(`<img height="20px" src="${baseURL}/storage/loading.gif" alt="">`);
                },
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let dataPengikut = "";
                    for(let i=0; i < data.length; i++){
                        dataPengikut += "<tr><td>" + data[i].nomor_urut_dalam_kk + "</td><td>" + data[i].nik + "</td><td>" + data[i].kk + "</td><td>" + data[i].nama + "</td></tr>";
                    }
                    $('#anggota_keluarga').html(dataPengikut)
                }
            })
        })
        $('.save').on('click', function(e){
            e.preventDefault();
            let nik = $('#nomor_induk_penduduk').val();
            let tgl_pindah = $('#tgl_pindah').val();
            let kota_tujuan = $('#kota_tujuan').val();
            let kecamatan_tujuan = $('#kecamatan_tujuan').val();
            let kelurahan_tujuan = $('#kelurahan_tujuan').val();
            let alamat_tujuan = $('#alamat_tujuan').val();
            if (kota_tujuan == undefined || kota_tujuan == "") {
                alertFail('Harap kota tujuan diisi');
                return false;
            }else if(kecamatan_tujuan == undefined || kecamatan_tujuan == "") {
                alertFail('Harap kecamatan tujuan diisi');
                return false;
            }else if(kelurahan_tujuan == undefined || kelurahan_tujuan == "") {
                alertFail('Harap kelurahan tujuan diisi');
                return false;
            }else if(alamat_tujuan == undefined || alamat_tujuan == "") {
                alertFail('Harap alamat tujuan diisi');
                return false;
            }else {
               return true;
            }
        })

        $(window).keydown(function(event){
            if( (event.keyCode == 13) && (validationFunction() == false) ) {
            event.preventDefault();
            return false;
            }
        });

        $('#provinsi_id').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kabupaten')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kota_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kota_tujuan += '<option value='+ data[i].city_id +'>' + data[i].city_name+ '</option>';
                    }
                   $("#kota_tujuan").html(kota_tujuan);
                }
            });
        })

        $('#kota_tujuan').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kecamatan')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kecamatan_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kecamatan_tujuan += '<option value='+ data[i].dis_id +'>' + data[i].dis_name+ '</option>';
                    }
                   $("#kecamatan_tujuan").html(kecamatan_tujuan);
                }
            });
        })

        $('#kecamatan_tujuan').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kelurahan')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kecamatan_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kecamatan_tujuan += '<option value='+ data[i].subdis_id +'>' + data[i].subdis_name+ '</option>';
                    }
                   $("#kelurahan_tujuan").html(kecamatan_tujuan);
                }
            });

        })

    });
    var createMutasi = function() {
        $('#modal-edit #nik').val("");
        $('#modal-edit #nama_penduduk').val("");
        $('#modal-edit #tgl_pindah').val("");
        $('#modal-edit #kota').val("");
        $('#modal-edit #kecamatan').val("");
        $('#modal-edit #kelurahan').val("");
        $('#modal-edit #alamat').val("");
        $('#modal-edit').modal("show");
    }

    function validationFunction() {
        let good= true;
        let nik = $('#nomor_induk_penduduk').val();
        let tgl_pindah = $('#tgl_pindah').val();
        let kota_tujuan = $('#kota_tujuan').val();
        let kecamatan_tujuan = $('#kecamatan_tujuan').val();
        let kelurahan_tujuan = $('#kelurahan_tujuan').val();
        let alamat_tujuan = $('#alamat_tujuan').val();
        if (kota_tujuan == undefined || kota_tujuan == "") {
            alertFail('Harap kota tujuan diisi');
            good= false;
        }else if(kecamatan_tujuan == undefined || kecamatan_tujuan == "") {
            alertFail('Harap kecamatan tujuan diisi');
            good= false;
        }else if(kelurahan_tujuan == undefined || kelurahan_tujuan == "") {
            alertFail('Harap kelurahan tujuan diisi');
            good= false;
        }else if(alamat_tujuan == undefined || alamat_tujuan == "") {
            alertFail('Harap alamat tujuan diisi');
            good= false;
        }
        if(good) {
            return true;
        }
        return false;
    }
</script>
@endpush
