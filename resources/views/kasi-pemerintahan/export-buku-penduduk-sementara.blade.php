<table class="tg">
    <thead>
      <tr>
        <th class="tg-c3ow" colspan="18"> BUKU PENDUDUK SEMENTARA&nbsp;&nbsp;&nbsp;TAHUN {{date('Y')}}</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="tg-pb0m" colspan="18">DESA&nbsp;&nbsp;&nbsp;GELANGKULON  KECAMATAN SAMPUNG</td>
      </tr>
      <tr>
        <td class="tg-pb0m" colspan="18">KABUPATEN PONOROGO</td>
      </tr>
      <tr>
        <td class="tg-pb0m"></td>
        <td class="tg-za14"></td>
        <td class="tg-za14"></td>
        <td class="tg-za14"></td>
        <td class="tg-za14"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
      </tr>
      <tr>
        <td class="tg-nrix" rowspan="2">NOMOR&nbsp;&nbsp;&nbsp;URUT</td>
        <td class="tg-nrix" rowspan="2">NAMA LENGKAP</td>
        <td class="tg-nrix" colspan="2">JENIS&nbsp;&nbsp;&nbsp;KELAMIN</td>
        <td class="tg-nrix" rowspan="2">NOMOR IDENTITAS/ TANDA&nbsp;&nbsp;&nbsp;PENGENAL</td>
        <td class="tg-nrix" rowspan="2">TEMPAT DAN TANGGAL&nbsp;&nbsp;&nbsp;LAHIR/ UMUR</td>
        <td class="tg-nrix" rowspan="2">PEKER JAAN</td>
        <td class="tg-nrix" colspan="2">KEWARGANEGARAAN</td>
        <td class="tg-nrix" rowspan="2">DATANG DARI</td>
        <td class="tg-nrix" rowspan="2">MAKSUD DAN TUJUAN&nbsp;&nbsp;&nbsp;KEDATANGAN</td>
        <td class="tg-nrix" rowspan="2">NAMA DAN ALAMAT YG&nbsp;&nbsp;&nbsp;DIDATANGI</td>
        <td class="tg-nrix" rowspan="2">DATANG TANGGAL</td>
        <td class="tg-nrix" rowspan="2">PERGI TANGGAL</td>
        <td class="tg-nrix" rowspan="2">FOTO</td>
        <td class="tg-nrix" rowspan="2">KET</td>
        <td class="tg-nrix" rowspan="2">OPERATOR</td>
        <td class="tg-nrix" rowspan="2">WAKTU PENDATAAN</td>
      </tr>
      <tr>
        <td class="tg-nrix">L</td>
        <td class="tg-nrix">P</td>
        <td class="tg-nrix">KEBANGSAAN</td>
        <td class="tg-nrix">KETURUNAN</td>
      </tr>
      <tr>
        <td class="tg-baqh">1</td>
        <td class="tg-baqh">2</td>
        <td class="tg-baqh">3</td>
        <td class="tg-baqh">4</td>
        <td class="tg-baqh">5</td>
        <td class="tg-baqh">6</td>
        <td class="tg-baqh">7</td>
        <td class="tg-baqh">8</td>
        <td class="tg-baqh">9</td>
        <td class="tg-baqh">10</td>
        <td class="tg-baqh">11</td>
        <td class="tg-baqh">12</td>
        <td class="tg-baqh">13</td>
        <td class="tg-baqh">14</td>
        <td class="tg-baqh">15</td>
        <td class="tg-baqh">16</td>
        <td class="tg-nrix">17</td>
        <td class="tg-nrix">18</td>
      </tr>
      <tr>
        <td class="tg-nrix">1</td>
        <td class="tg-nrix">FAJRIN&nbsp;&nbsp;&nbsp;NUGROHO</td>
        <td class="tg-nrix">L</td>
        <td class="tg-nrix">-</td>
        <td class="tg-nrix">35214658132848</td>
        <td class="tg-nrix">Surabaya,&nbsp;&nbsp;&nbsp;17 Oktober 1996</td>
        <td class="tg-nrix">Mahasiswa</td>
        <td class="tg-nrix">WNI</td>
        <td class="tg-nrix">-</td>
        <td class="tg-nrix">SURABAYA</td>
        <td class="tg-nrix">SEKOLAH</td>
        <td class="tg-nrix">DUSUN&nbsp;&nbsp;&nbsp;KROYO  RT001 RW003</td>
        <td class="tg-nrix">18&nbsp;&nbsp;&nbsp;Februari 2021</td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix">file&nbsp;&nbsp;&nbsp;foto </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix">KASI&nbsp;&nbsp;&nbsp;PEMERINTAHAN </td>
        <td class="tg-nrix">20-Feb-21</td>
      </tr>
      <tr>
        <td class="tg-nrix">2</td>
        <td class="tg-nrix">INDAH&nbsp;&nbsp;&nbsp;KURNIAWATI</td>
        <td class="tg-nrix">-</td>
        <td class="tg-nrix">P</td>
        <td class="tg-nrix">953116958400001</td>
        <td class="tg-nrix">Jakarta,&nbsp;&nbsp;&nbsp;11 Mei 1985</td>
        <td class="tg-nrix">Ibu&nbsp;&nbsp;&nbsp;Rumah Tangga</td>
        <td class="tg-nrix">-</td>
        <td class="tg-nrix">WNA</td>
        <td class="tg-baqh"> </td>
        <td class="tg-nrix">BEROBAT</td>
        <td class="tg-nrix">DUSUN&nbsp;&nbsp;&nbsp;DARAT RT 004 RW 002</td>
        <td class="tg-nrix">17&nbsp;&nbsp;&nbsp;April 2021</td>
        <td class="tg-nrix">-</td>
        <td class="tg-nrix">file&nbsp;&nbsp;&nbsp;foto </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix">KETUA&nbsp;&nbsp;&nbsp;RT 004 RW 002 Dusun Kroyo</td>
        <td class="tg-nrix">17-Apr-21</td>
      </tr>
      <tr>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
      </tr>
      <tr>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
      </tr>
      <tr>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-baqh"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
        <td class="tg-nrix"> </td>
      </tr>
      <tr>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
      </tr>
      <tr>
        <td class="tg-8d8j" colspan="6">Mengetahui :</td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-8d8j" colspan="5">……………………….., ………………….. 20XX</td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
      </tr>
      <tr>
        <td class="tg-8d8j" colspan="6">Kepala Desa ……………………….</td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-8d8j" colspan="5">Sekretaris Desa ………………………</td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
      </tr>
      <tr>
        <td class="tg-7zrl" colspan="6"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl" colspan="5"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
      </tr>
      <tr>
        <td class="tg-8d8j" colspan="6">…………………………………….</td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
        <td class="tg-8d8j" colspan="5">……………………………………….</td>
        <td class="tg-7zrl"></td>
        <td class="tg-7zrl"></td>
      </tr>
    </tbody>
</table>
