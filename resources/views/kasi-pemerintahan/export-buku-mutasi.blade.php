<table>
    <tr>
        <td colspan="18"><b>B.2 BUKU MUTASI PENDUDUK DESA</b> </td>
    </tr>
    <tr>
        <td colspan="18"><b>BUKU MUTASI PENDUDUK DESA  BULAN @php echo date('M') @endphp TAHUN @php echo date('Y') @endphp</b> </td>
    </tr>
</table>

<table class="table table-bordered">
    <thead>
        <tr>
            <th class="tg-nrix" rowspan="2">NOMOR URUT</th>
            <th class="tg-nrix" rowspan="2">NAMA LENGKAP/ PANGGILAN</th>
            <th class="tg-nrix" colspan="2">TEMPAT &amp; TANGGAL LAHIR</th>
            <th class="tg-nrix" rowspan="2">JENIS KELAMIN</th>
            <th class="tg-nrix" rowspan="2">KEWARGA NEGARAAN</th>
            <th class="tg-o4ah" colspan="3">PENAMBAHAN</th>
            <th class="tg-o4ah" colspan="4">PENGURANGAN</th>
            <th class="tg-o4ah" rowspan="2">KET</th>
            <th class="tg-nrix" rowspan="2">OPERATOR</th>
            <th class="tg-nrix" rowspan="2">WAKTU INPUT</th>
        </tr>
        <tr>
            <th class="tg-nrix">TEMPAT</th>
            <th class="tg-nrix">TANGGAL</th>
            <th class="tg-o4ah">DATANG DARI</th>
            <th class="tg-o4ah">TANGGAL</th>
            <th class="tg-o4ah">ALAMAT TUJUAN </th>
            <th class="tg-o4ah">PINDAH KE</th>
            <th class="tg-o4ah">TANGGAL</th>
            <th class="tg-o4ah">MENINGGAL</th>
            <th class="tg-o4ah">TANGGAL</th>
        </tr>
    </thead>
    <tbody>
        @php
            $loop=0;
        @endphp
        @foreach ($penduduks as $key => $item)
        <tr>
            <td class="text-center">{{ $key }}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->tempat_lahir}}</td>
            <td>{{tgl($item->tanggal_lahir)}}</td>
            <td>{{@$item->jenis_kelamin==1?'L':'P'}}</td>
            <td>{{@$item->kewarganegaraan == 1?"WNI":"WNA"}}</td>
            @if($item->status_penduduk_id == 3)
            <td>{{$penduduk->alamat_sebelumnya?? "-"}}</td>
            <td>{{@$item->tgl_pindah ? tgl($item->tgl_pindah):""}}</td>
            <td>{{$penduduk->alamat_sekarang ?? '-' }}</td>
            @else
            <td>-</td>
            <td>-</td>
            <td>-</td>
            @endif
            <td>{{@$item->kotaTujuan->city_name ?? ""}}  {{ @$item->kecamatanTujuan->dis_name ?? ""}} {{ @$item->kelurahanTujuan->subdis_name ?? ""}}</td>
            <td>{{$item->tgl_mutasi}}</td>
            @if($item->status_penduduk_id == 4)
            <td>{{$item->tempat_meninggal}}</td>
            <td>{{$item->tgl_meninggal}}</td>
            <td>{{$item->penyebab_meninggal}}</td>
            @else
            <td>-</td>
            <td>-</td>
            <td>-</td>
            @endif

            <td>{{@$item->users_created==null ?"-":$item->users_created->nama}}</td>
            <td>{{dateFormatTime($item->created_at)}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<table>
    <thead>
      <tr>
        <th colspan="2"></th>
        <th colspan="3">Mengetahui : </th>
        <th colspan="7"></th>
        <th colspan="4">………………..,………… @php echo date('Y') @endphp</th>
        <th colspan="2"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="2"></td>
        <td colspan="3">Kepala Desa</td>
        <td colspan="7"></td>
        <td colspan="4">Sekretaris Desa</td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="18"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3"></td>
        <td colspan="7"></td>
        <td  colspan="4"></td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3"></td>
        <td colspan="7"></td>
        <td  colspan="4"></td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3">…………………………</td>
        <td colspan="7"></td>
        <td  colspan="4">……………………………</td>
        <td colspan="2"></td>
      </tr>
    </tbody>
</table>
