<table>
    <tr>
        <td colspan="18"><b>B.3 BUKU REKAP PENDUDUK DESA</b> </td>
    </tr>
    <tr>
        <td colspan="18"><b>BUKU REKAP PENDUDUK DESA  BULAN @php echo date('M') @endphp TAHUN @php echo date('Y') @endphp</b> </td>
    </tr>
</table>
<table class="table table-hover table-sm table-striped table-bordered tg">
    <thead>
        <tr>
            <td class="tg-9wq8" rowspan="4">NOMOR URUT</td>
            <td class="tg-9wq8" rowspan="4">NAMA DUSUN/ LINGKUNGAN</td>
            <td class="tg-9wq8" colspan="7">JUMLAH PENDUDUK AWAL BULAN</td>
            <td class="tg-nrix" colspan="8">TAMBAHAN BULAN INI</td>
            <td class="tg-nrix" colspan="8">PENGURANGAN BULAN INI</td>
            <td class="tg-nrix" colspan="7" rowspan="2">JML PENDUDUK AKHIR BULAN</td>
            <td class="tg-nrix" rowspan="4">KET</td>
          </tr>
          <tr>
            <td class="tg-9wq8" colspan="2" rowspan="2">WNI</td>
            <td class="tg-9wq8" colspan="2" rowspan="2">WNA</td>
            <td class="tg-nrix" rowspan="3">JML KK</td>
            <td class="tg-nrix" rowspan="3">JML&nbsp;&nbsp;&nbsp;ANGGOTA KELUARGA</td>
            <td class="tg-nrix" rowspan="3">JML JIWA&nbsp;&nbsp;&nbsp;(7+8)</td>
            <td class="tg-nrix" colspan="4">LAHIR</td>
            <td class="tg-nrix" colspan="4">DATANG</td>
            <td class="tg-nrix" colspan="4">MENINGGAL</td>
            <td class="tg-nrix" colspan="4">PINDAH</td>
          </tr>
          <tr>
            <td class="tg-nrix" colspan="2">WNA</td>
            <td class="tg-nrix" colspan="2">WNI</td>
            <td class="tg-nrix" colspan="2">WNA</td>
            <td class="tg-nrix" colspan="2">WNI</td>
            <td class="tg-nrix" colspan="2">WNA</td>
            <td class="tg-nrix" colspan="2">WNI</td>
            <td class="tg-nrix" colspan="2">WNA</td>
            <td class="tg-nrix" colspan="2">WNI</td>
            <td class="tg-nrix" colspan="2">WNA</td>
            <td class="tg-nrix" colspan="2">WNI</td>
            <td class="tg-nrix" rowspan="2">JML KK</td>
            <td class="tg-nrix" rowspan="2">JML  ANGGOTA KELUARGA</td>
            <td class="tg-nrix" rowspan="2">JML JIWA</td>
          </tr>
          <tr>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-cly1">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
            <td class="tg-nrix">L</td>
            <td class="tg-nrix">P</td>
          </tr>
          <tr>
            <td class="tg-nrix">1</td>
            <td class="tg-nrix">2</td>
            <td class="tg-nrix">3</td>
            <td class="tg-nrix">4</td>
            <td class="tg-nrix">5</td>
            <td class="tg-nrix">6</td>
            <td class="tg-nrix">7</td>
            <td class="tg-nrix">8</td>
            <td class="tg-nrix">9</td>
            <td class="tg-nrix">10</td>
            <td class="tg-nrix">11</td>
            <td class="tg-nrix">12</td>
            <td class="tg-nrix">13</td>
            <td class="tg-nrix">14</td>
            <td class="tg-nrix">15</td>
            <td class="tg-nrix">16</td>
            <td class="tg-nrix">17</td>
            <td class="tg-nrix">18</td>
            <td class="tg-nrix">19</td>
            <td class="tg-nrix">20</td>
            <td class="tg-nrix">21</td>
            <td class="tg-nrix">22</td>
            <td class="tg-nrix">23</td>
            <td class="tg-nrix">24</td>
            <td class="tg-nrix">25</td>
            <td class="tg-nrix">26</td>
            <td class="tg-nrix">27</td>
            <td class="tg-nrix">28</td>
            <td class="tg-nrix">29</td>
            <td class="tg-nrix">30</td>
            <td class="tg-nrix">31</td>
            <td class="tg-nrix">32</td>
            <td class="tg-nrix">33</td>
          </tr>
    </thead>
    <tbody>
        @forelse ($allDusuns as $key => $item)
        <tr>
            <td class="tg-nrix">{{++$key}}</td>
            <td class="tg-nrix">@lang("Dusun") {{$item->nama}}</td>
            <td class="tg-nrix">{{$item->awal_month_wni_l}} </td>
            <td class="tg-nrix">{{$item->awal_month_wni_p}} </td>
            <td class="tg-nrix">{{$item->awal_month_wna_l}}  </td>
            <td class="tg-nrix">{{$item->awal_month_wna_p}}  </td>
            <td class="tg-nrix">{{$item->jml_kk}} </td>
            <td class="tg-nrix">{{$item->jml_anggota}} </td>
            <td class="tg-nrix">{{$item->total_pend}}</td>
            <td class="tg-nrix">{{$item->lhr_month_wni_l}}</td>
            <td class="tg-nrix">{{$item->lhr_month_wni_p}} </td>
            <td class="tg-nrix">{{$item->lhr_month_wna_l}}</td>
            <td class="tg-nrix">{{$item->lhr_month_wna_p}}</td>
            <td class="tg-nrix">{{$item->dtg_month_wna_l}} </td>
            <td class="tg-nrix">{{$item->dtg_month_wna_p}} </td>
            <td class="tg-nrix">{{$item->dtg_month_wni_l}}</td>
            <td class="tg-nrix">{{$item->dtg_month_wni_p}} </td>
            <td class="tg-nrix">{{$item->mngl_month_wna_l}} </td>
            <td class="tg-nrix">{{$item->mngl_month_wna_p}} </td>
            <td class="tg-nrix">{{$item->mngl_month_wni_l}} </td>
            <td class="tg-nrix">{{$item->mngl_month_wni_p}} </td>
            <td class="tg-nrix">{{$item->pndh_month_wna_l}} </td>
            <td class="tg-nrix">{{$item->pndh_month_wna_p}} </td>
            <td class="tg-nrix">{{$item->pndh_month_wni_l}} </td>
            <td class="tg-nrix">{{$item->pndh_month_wni_p}} </td>
            <td class="tg-nrix">{{$item->total_wna_l}} </td>
            <td class="tg-nrix">{{$item->total_wna_p}} </td>
            <td class="tg-nrix">{{$item->total_wni_l}} </td>
            <td class="tg-nrix">{{$item->total_wni_p}} </td>
            <td class="tg-nrix">{{$item->total_kk}} </td>
            <td class="tg-nrix">{{$item->total_anggota}} </td>
            <td class="tg-nrix">{{$item->total_jiwa}} </td>
            <td class="tg-nrix"> </td>
          </tr>
        @empty
            <tr>
                <td colspan="33" align="center">Data tidak tersedia</td>
            </tr>
        @endforelse
    </tbody>
</table>
<table>
    <thead>
      <tr>
        <th colspan="2"></th>
        <th colspan="3">Mengetahui : </th>
        <th colspan="7"></th>
        <th colspan="4">………………..,………… @php echo date('Y') @endphp</th>
        <th colspan="2"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="2"></td>
        <td colspan="3">Kepala Desa</td>
        <td colspan="7"></td>
        <td colspan="4">Sekretaris Desa</td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="18"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3"></td>
        <td colspan="7"></td>
        <td  colspan="4"></td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3"></td>
        <td colspan="7"></td>
        <td  colspan="4"></td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3">…………………………</td>
        <td colspan="7"></td>
        <td  colspan="4">……………………………</td>
        <td colspan="2"></td>
      </tr>
    </tbody>
</table>
