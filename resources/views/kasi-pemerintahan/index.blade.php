@extends('layouts.app')

@section('title', 'Kasi Pemerintahan')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Kasi Pemerintahan</h2>
                                <p class="mb-0 text-sm">Analisa Kasi Pemerintah</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ route("penduduk.index") }}?page={{ request('page') }}" class="btn btn-success" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content')
<div class="card shadow">
    <div class="card-body">
        <div class="row pb-8">
            <div class="col-xl-6 col-md-6 col-sm-6 pb-5">
                <div class="card card-stats shadow h-100">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <a href="{{route('kasipem.buku_induk')}}"><h5 class="card-title text-uppercase text-muted mb-0">Buku Induk Penduduk</h5></a>
                                <span class="h2 font-weight-bold mb-0"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                    <i class="fas fa-book"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 col-sm-6 pb-5">
                <div class="card card-stats shadow h-100">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <a href="{{route('kasipem.mutasi-penduduk')}}"><h5 class="card-title text-uppercase text-muted mb-0">Buku Mutasi Penduduk</h5></a>
                                <span class="h2 font-weight-bold mb-0"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                    <i class="fas fa-book"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 col-sm-6 pb-5">
                <div class="card card-stats shadow h-100">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <a href="{{route('kasipem.rekap_penduduk')}}"><h5 class="card-title text-uppercase text-muted mb-0">Buku Rekap Penduduk</h5></a>
                                <span class="h2 font-weight-bold mb-0"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                    <i class="fas fa-book"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 col-sm-6 pb-5">
                <div class="card card-stats shadow h-100">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <a href="{{route('kasipem.penduduk_sementara')}}"><h5 class="card-title text-uppercase text-muted mb-0">Buku Penduduk Sementara</h5></a>
                                <span class="h2 font-weight-bold mb-0"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                    <i class="fas fa-book"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-6 col-sm-6 pb-5">
                <div class="card card-stats shadow h-100">
                    <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <a href="{{route('kasipem.buku_ktp-kk')}}"><h5 class="card-title text-uppercase text-muted mb-0">Buku KTP dan KK Tahunan</h5></a>
                                <span class="h2 font-weight-bold mb-0"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                    <i class="fas fa-book"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
