<table>
    <tr>
        <td colspan="18">B.1 BUKU INDUK PENDUDUK </td>
    </tr>
</table>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Lengkap/Panggilan</th>
            <th>Jenis Kelamin</th>
            <th>Status Perkawinan</th>
            <th>Tempat Lahir</th>
            <th>Tanggal Lahir</th>
            <th>Agama</th>
            <th>Pendidikan Terakhir</th>
            <th>Pekerjaan</th>
            <th>Dapat Membaca</th>
            <th>Kewarganegara</th>
            <th>Alamat Lengkap</th>
            <th>Kedudukan Dlm Keluarga</th>
            <th>NIK</th>
            <th>Nomor KK</th>
            <th>Ket</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($penduduks as $key => $item)
        <tr>
            <td>{{  $key }}</td>
            <td>{{$item->nama}}</td>
            <td>{{@$item->jenis_kelamin==1?'L':'P'}}</td>
            <td>{{statusPerkawinan($item->status_perkawinan_id)}}</td>
            <td>{{$item->tempat_lahir}}</td>
            <td>{{tgl($item->tanggal_lahir)}}</td>
            <td>{{@$item->agama->nama ?? ""}}</td>
            <td>{{@$item->pendidikan->nama ?? ""}}</td>
            <td>{{@$item->pekerjaan->nama ?? ""}}</td>
            <td></td>
            <td>{{@$item->kewarganegaraan == 1?"WNI":"WNA"}}</td>
            <td>{{$item->alamat_sekarang}}</td>
            <td>{{@$item->statusHubunganDalamKeluarga == null ? $item->statusHubunganDalamKeluarga->nama:""}}</td>
            <td>{{$item->nik}}</td>
            <td>{{$item->kk}}</td>
            <td></td>
        </tr>
        @endforeach
    </tbody>
</table>

<table>
    <thead>
      <tr>
        <th colspan="2"></th>
        <th colspan="3">Mengetahui : </th>
        <th colspan="7"></th>
        <th colspan="4">………………..,………… @php echo date('Y') @endphp</th>
        <th colspan="2"></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="2"></td>
        <td colspan="3">Kepala Desa</td>
        <td colspan="7"></td>
        <td colspan="4">Sekretaris Desa</td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="18"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3"></td>
        <td colspan="7"></td>
        <td  colspan="4"></td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3"></td>
        <td colspan="7"></td>
        <td  colspan="4"></td>
        <td colspan="2"></td>
      </tr>
      <tr>
        <td colspan="2"></td>
        <td  colspan="3">…………………………</td>
        <td colspan="7"></td>
        <td  colspan="4">……………………………</td>
        <td colspan="2"></td>
      </tr>
    </tbody>
</table>
