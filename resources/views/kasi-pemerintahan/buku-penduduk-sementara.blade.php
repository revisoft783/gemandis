@extends('layouts.app')

@section('title', 'Penduduk Mutasi')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

<style>
    .table th, .table td {
        padding: 5px;
    }
    .card .table td, .card .table th {
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
      overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
      font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg .tg-pb0m{border-color:inherit;text-align:center;vertical-align:bottom}
    .tg .tg-nrix{border-color:inherit;text-align:center;vertical-align:middle}
    .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    .tg .tg-za14{border-color:inherit;text-align:left;vertical-align:bottom}
    .tg .tg-baqh{border-color:inherit;text-align:center;vertical-align:middle}
    </style>
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-values-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-values-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Buku Induk Penduduk</h2>
                                <p class="mb-0 text-sm">Kelola Penduduk</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ url("kasipem") }}?page={{ request('page') }}" class="btn btn-secondary mr-2" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                                {{-- <a target="_blank" href="#addPenduduk" data-toggle="tooltip" class="mr-2 btn btn-outline-success" title="Tambah Penduduk"><i class="fas fa-plus"></i> Tambah</a>
                                <a target="_blank" href="{{ route('kasipem.print_buku_induk') }}" data-toggle="tooltip" class="mr-2 btn btn-primary" title="Cetak"><i class="fas fa-print"></i> Cetak</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('form-search')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" action="{{ URL::current() }}" method="GET">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Cari ...." type="search" name="cari" value="{{ request('cari') }}">
        </div>
    </div>
</form>
@endsection

@section('form-search-mobile')
<form class="mt-4 mb-3 d-md-none" action="{{ URL::current() }}" method="GET">
    <div class="input-group input-group-rounded input-group-merge">
        <input type="search" name="cari" class="form-control form-control-rounded form-control-prepended" placeholder="cari" aria-label="Search" value="{{ request('cari') }}">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <span class="fa fa-search"></span>
            </div>
        </div>
    </div>
</form>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card shadow">
    <div class="card-body">
        <div class="table-responsive">
            <div class="row">
                <form class="mt-4 mb-3 ml-3" action="{{ URL::current() }}" method="GET">
                    <div class="input-group input-group-rounded input-group-merge">
                        <input type="date" class="form-control form-control-rounded form-control-prepended" name="cari" id="cari" aria-label="Search" title="Filter Cetak"  placeholder="Masukkan Tanggal ..." value="{{ date('Y-m-d',strtotime(request('cari')))  }}">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-search"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <table class="tg">
                <thead>
                    <tr>
                        <td class="tg-nrix" rowspan="2">NOMOR&nbsp;&nbsp;&nbsp;URUT</td>
                        <td class="tg-nrix" rowspan="2">NAMA LENGKAP</td>
                        <td class="tg-nrix" colspan="2">JENIS&nbsp;&nbsp;&nbsp;KELAMIN</td>
                        <td class="tg-nrix" rowspan="2">NOMOR IDENTITAS/ TANDA&nbsp;&nbsp;&nbsp;PENGENAL</td>
                        <td class="tg-nrix" rowspan="2">TEMPAT DAN TANGGAL&nbsp;&nbsp;&nbsp;LAHIR/ UMUR</td>
                        <td class="tg-nrix" rowspan="2">PEKER JAAN</td>
                        <td class="tg-nrix" colspan="2">KEWARGANEGARAAN</td>
                        <td class="tg-nrix" rowspan="2">DATANG DARI</td>
                        <td class="tg-nrix" rowspan="2">MAKSUD DAN TUJUAN&nbsp;&nbsp;&nbsp;KEDATANGAN</td>
                        <td class="tg-nrix" rowspan="2">NAMA DAN ALAMAT YG&nbsp;&nbsp;&nbsp;DIDATANGI</td>
                        <td class="tg-nrix" rowspan="2">DATANG TANGGAL</td>
                        <td class="tg-nrix" rowspan="2">PERGI TANGGAL</td>
                        <td class="tg-nrix" rowspan="2">FOTO</td>
                        <td class="tg-nrix" rowspan="2">KET</td>
                        <td class="tg-nrix" rowspan="2">OPERATOR</td>
                        <td class="tg-nrix" rowspan="2">WAKTU PENDATAAN</td>
                    </tr>
                    <tr>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">KEBANGSAAN</td>
                        <td class="tg-nrix">KETURUNAN</td>
                    </tr>
                    <tr>
                        <td class="tg-baqh">1</td>
                        <td class="tg-baqh">2</td>
                        <td class="tg-baqh">3</td>
                        <td class="tg-baqh">4</td>
                        <td class="tg-baqh">5</td>
                        <td class="tg-baqh">6</td>
                        <td class="tg-baqh">7</td>
                        <td class="tg-baqh">8</td>
                        <td class="tg-baqh">9</td>
                        <td class="tg-baqh">10</td>
                        <td class="tg-baqh">11</td>
                        <td class="tg-baqh">12</td>
                        <td class="tg-baqh">13</td>
                        <td class="tg-baqh">14</td>
                        <td class="tg-baqh">15</td>
                        <td class="tg-baqh">16</td>
                        <td class="tg-baqh">17</td>
                        <td class="tg-baqh">18</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i=0;
                    @endphp
                    @forelse ($penduduks as $value)
                        <tr>
                            <td class="tg-nrix">{{++$i}}</td>
                            <td class="tg-nrix">{{$value->nama}}</td>
                            <td class="tg-nrix">{{@$value->jenis_kelamin==1?'L':'-'}}</td>
                            <td class="tg-nrix">{{@$value->jenis_kelamin==2?'P':'-'}}</td>
                            <td class="tg-nrix">{{$value->nik}}</td>
                            <td class="tg-nrix">{{$value->tempat_lahir}},&nbsp;&nbsp;&nbsp;{{tgl($value->tanggal_lahir)}}</td>
                            <td class="tg-nrix">{{$value->pekerjaan->nama}}</td>
                            <td class="tg-nrix">{{@$value->kewarganegaraan == 1?"WNI":"-"}}</td>
                            <td class="tg-nrix">{{@$value->kewarganegaraan == 2?"WNA":"-"}}</td>
                            <td class="tg-nrix">{{$value->kota_dari}}</td>
                            <td class="tg-nrix">SEKOLAH</td>
                            <td class="tg-nrix">{{$value->alamat_sekarang}}</td>
                            <td class="tg-nrix">{{@$value->tgl_mutasi??""}}</td>
                            <td class="tg-nrix">{{@$value->tgl_pindah??""}} </td>
                            <td class="tg-nrix">file&nbsp;&nbsp;&nbsp;foto </td>
                            <td class="tg-nrix"> </td>
                            <td class="tg-nrix">{{@$value->users_created==null ?"-":$value->users_created->nama}} </td>
                            <td class="tg-nrix">{{dateFormatTime($value->created_at)}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="18" align="center">Data tidak tersedia</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {{ $penduduks->links('layouts.components.pagination') }}
    </div>
</div>
@endsection
