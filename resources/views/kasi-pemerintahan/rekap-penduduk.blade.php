@extends('layouts.app')

@section('title', 'Penduduk Mutasi')

@section('styles')
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />

<style>
    .table th, .table td {
        padding: 5px;
    }
    .card .table th {
        padding-left: 5px;
        padding-right: 5px;
        vertical-align:middle;
        text-align: center
    }

    .card .table td {
        padding-left: 5px;
        padding-right: 5px;
    }
</style>
<style type="text/css">
    .tg  {border-collapse:collapse;border-spacing:0;}
    .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
      overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
      font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
    .tg .tg-cly1{text-align:left;vertical-align:middle}
    .tg .tg-9wq8{border-color:inherit;text-align:center;vertical-align:middle}
    .tg .tg-tads{font-weight:bold;text-align:center;text-decoration:underline;vertical-align:middle}
    .tg .tg-nrix{text-align:center;vertical-align:middle}
    .tg .tg-7zrl{text-align:left;vertical-align:bottom}
    .tg .tg-8d8j{text-align:center;vertical-align:bottom}
    .tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
@endsection

@section('content-header')
<div class="header pb-8 pt-5 pt-lg-8 d-flex align-items-center" style="background-image: url({{ asset('/img/cover-bg-profil.jpg') }}); background-size: cover; background-position: center top;">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col">
                <div class="card shadow h-100">
                    <div class="card-header border-0">
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                            <div class="mb-3">
                                <h2 class="mb-0">Buku Rekap Penduduk</h2>
                                <p class="mb-0 text-sm">Kelola Penduduk</p>
                            </div>
                            <div class="mb-3">
                                <a href="{{ url("kasipem") }}?page={{ request('page') }}" class="btn btn-secondary mr-2" title="Kembali"><i class="fas fa-arrow-left"></i> Kembali</a>
                                <a target="_blank" href="{{ route('kasipem.print_rekap_penduduk') }}" data-toggle="tooltip" class="mr-2 btn btn-primary" title="Cetak"><i class="fas fa-print"></i> Cetak</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('form-search')
<form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto" action="{{ URL::current() }}" method="GET">
    <div class="form-group mb-0">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
            </div>
            <input class="form-control" placeholder="Cari ...." type="search" name="cari" value="{{ request('cari') }}">
        </div>
    </div>
</form>
@endsection

@section('form-search-mobile')
<form class="mt-4 mb-3 d-md-none" action="{{ URL::current() }}" method="GET">
    <div class="input-group input-group-rounded input-group-merge">
        <input type="search" name="cari" class="form-control form-control-rounded form-control-prepended" placeholder="cari" aria-label="Search" value="{{ request('cari') }}">
        <div class="input-group-prepend">
            <div class="input-group-text">
                <span class="fa fa-search"></span>
            </div>
        </div>
    </div>
</form>
@endsection

@section('content')
@include('layouts.components.alert')
<div class="card shadow">
    <div class="card-body">
        <div class="table-responsive">
            <div class="row">
                <form class="mt-4 mb-3 ml-3" action="{{ URL::current() }}" method="GET">
                    <div class="input-group input-group-rounded input-group-merge">
                        <input type="date" class="form-control form-control-rounded form-control-prepended" name="cari" id="cari" aria-label="Search" title="Filter Cetak"  placeholder="Masukkan Tanggal ..." value="{{ date('Y-m-d',strtotime(request('cari')))  }}">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-search"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-hover table-sm table-striped table-bordered tg">
                <thead>
                    <tr>
                        <td class="tg-9wq8" rowspan="4">NOMOR URUT</td>
                        <td class="tg-9wq8" rowspan="4">NAMA DUSUN/ LINGKUNGAN</td>
                        <td class="tg-9wq8" colspan="7">JUMLAH PENDUDUK AWAL BULAN</td>
                        <td class="tg-nrix" colspan="8">TAMBAHAN BULAN INI</td>
                        <td class="tg-nrix" colspan="8">PENGURANGAN BULAN INI</td>
                        <td class="tg-nrix" colspan="7" rowspan="2">JML PENDUDUK AKHIR BULAN</td>
                        <td class="tg-nrix" rowspan="4">KET</td>
                      </tr>
                      <tr>
                        <td class="tg-9wq8" colspan="2" rowspan="2">WNI</td>
                        <td class="tg-9wq8" colspan="2" rowspan="2">WNA</td>
                        <td class="tg-nrix" rowspan="3">JML KK</td>
                        <td class="tg-nrix" rowspan="3">JML&nbsp;&nbsp;&nbsp;ANGGOTA KELUARGA</td>
                        <td class="tg-nrix" rowspan="3">JML JIWA&nbsp;&nbsp;&nbsp;(7+8)</td>
                        <td class="tg-nrix" colspan="4">LAHIR</td>
                        <td class="tg-nrix" colspan="4">DATANG</td>
                        <td class="tg-nrix" colspan="4">MENINGGAL</td>
                        <td class="tg-nrix" colspan="4">PINDAH</td>
                      </tr>
                      <tr>
                        <td class="tg-nrix" colspan="2">WNA</td>
                        <td class="tg-nrix" colspan="2">WNI</td>
                        <td class="tg-nrix" colspan="2">WNA</td>
                        <td class="tg-nrix" colspan="2">WNI</td>
                        <td class="tg-nrix" colspan="2">WNA</td>
                        <td class="tg-nrix" colspan="2">WNI</td>
                        <td class="tg-nrix" colspan="2">WNA</td>
                        <td class="tg-nrix" colspan="2">WNI</td>
                        <td class="tg-nrix" colspan="2">WNA</td>
                        <td class="tg-nrix" colspan="2">WNI</td>
                        <td class="tg-nrix" rowspan="2">JML KK</td>
                        <td class="tg-nrix" rowspan="2">JML  ANGGOTA KELUARGA</td>
                        <td class="tg-nrix" rowspan="2">JML JIWA</td>
                      </tr>
                      <tr>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-cly1">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                        <td class="tg-nrix">L</td>
                        <td class="tg-nrix">P</td>
                      </tr>
                      <tr>
                        <td class="tg-nrix">1</td>
                        <td class="tg-nrix">2</td>
                        <td class="tg-nrix">3</td>
                        <td class="tg-nrix">4</td>
                        <td class="tg-nrix">5</td>
                        <td class="tg-nrix">6</td>
                        <td class="tg-nrix">7</td>
                        <td class="tg-nrix">8</td>
                        <td class="tg-nrix">9</td>
                        <td class="tg-nrix">10</td>
                        <td class="tg-nrix">11</td>
                        <td class="tg-nrix">12</td>
                        <td class="tg-nrix">13</td>
                        <td class="tg-nrix">14</td>
                        <td class="tg-nrix">15</td>
                        <td class="tg-nrix">16</td>
                        <td class="tg-nrix">17</td>
                        <td class="tg-nrix">18</td>
                        <td class="tg-nrix">19</td>
                        <td class="tg-nrix">20</td>
                        <td class="tg-nrix">21</td>
                        <td class="tg-nrix">22</td>
                        <td class="tg-nrix">23</td>
                        <td class="tg-nrix">24</td>
                        <td class="tg-nrix">25</td>
                        <td class="tg-nrix">26</td>
                        <td class="tg-nrix">27</td>
                        <td class="tg-nrix">28</td>
                        <td class="tg-nrix">29</td>
                        <td class="tg-nrix">30</td>
                        <td class="tg-nrix">31</td>
                        <td class="tg-nrix">32</td>
                        <td class="tg-nrix">33</td>
                      </tr>
                </thead>
                <tbody>
                    @forelse ($allDusuns as $key => $item)
                    <tr>
                        <td class="tg-nrix">{{++$key}}</td>
                        <td class="tg-nrix">@lang("Dusun") {{$item->nama}}</td>
                        <td class="tg-nrix">{{$item->awal_month_wni_l}} </td>
                        <td class="tg-nrix">{{$item->awal_month_wni_p}} </td>
                        <td class="tg-nrix">{{$item->awal_month_wna_l}}  </td>
                        <td class="tg-nrix">{{$item->awal_month_wna_p}}  </td>
                        <td class="tg-nrix">{{$item->jml_kk}} </td>
                        <td class="tg-nrix">{{$item->jml_anggota}} </td>
                        <td class="tg-nrix">{{$item->total_pend}}</td>
                        <td class="tg-nrix">{{$item->lhr_month_wni_l}}</td>
                        <td class="tg-nrix">{{$item->lhr_month_wni_p}} </td>
                        <td class="tg-nrix">{{$item->lhr_month_wna_l}}</td>
                        <td class="tg-nrix">{{$item->lhr_month_wna_p}}</td>
                        <td class="tg-nrix">{{$item->dtg_month_wna_l}} </td>
                        <td class="tg-nrix">{{$item->dtg_month_wna_p}} </td>
                        <td class="tg-nrix">{{$item->dtg_month_wni_l}}</td>
                        <td class="tg-nrix">{{$item->dtg_month_wni_p}} </td>
                        <td class="tg-nrix">{{$item->mngl_month_wna_l}} </td>
                        <td class="tg-nrix">{{$item->mngl_month_wna_p}} </td>
                        <td class="tg-nrix">{{$item->mngl_month_wni_l}} </td>
                        <td class="tg-nrix">{{$item->mngl_month_wni_p}} </td>
                        <td class="tg-nrix">{{$item->pndh_month_wna_l}} </td>
                        <td class="tg-nrix">{{$item->pndh_month_wna_p}} </td>
                        <td class="tg-nrix">{{$item->pndh_month_wni_l}} </td>
                        <td class="tg-nrix">{{$item->pndh_month_wni_p}} </td>
                        <td class="tg-nrix">{{$item->total_wna_l}} </td>
                        <td class="tg-nrix">{{$item->total_wna_p}} </td>
                        <td class="tg-nrix">{{$item->total_wni_l}} </td>
                        <td class="tg-nrix">{{$item->total_wni_p}} </td>
                        <td class="tg-nrix">{{$item->total_kk}} </td>
                        <td class="tg-nrix">{{$item->total_anggota}} </td>
                        <td class="tg-nrix">{{$item->total_jiwa}} </td>
                        <td class="tg-nrix"> </td>
                      </tr>
                    @empty
                        <tr>
                            <td colspan="33" align="center">Data tidak tersedia</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('penduduk.pindah-status.edit')
@endsection

@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#tanggal,#dusun,#rt,#rw,#nik,#nama").change(function () {
            $(this).parent().submit();
        });
        $('#pilihan_keluarga').on('change', function(e){
            if($(this).val() == 0) {
                $('#sendiri_pindah').hide();
                $('#all_pindah').show();
            }else{
                $('#sendiri_pindah').show();
                $('#all_pindah').hide();
            }
        })
        $('.cari').on('change', function(e){
            e.preventDefault();
            $.ajax({
                url : baseURL + '/penduduk/cari',
                type: 'GET',
                data: {
                    nik: $("#nomor_induk_penduduk").val()
                },
                before: function () {
                    $("#cari-nik").html(`<img height="20px" src="${baseURL}/storage/loading.gif" alt="">`);
                },
                success: function(response){
                    if (response.nik) {
                        $("#nomor_induk_penduduk").html(`cari`);
                        $("#nama_penduduk").val(response.nama);
                        $('#tgl_pindah').focus();
                    }
                }
            })
        })
        $('.cari_kk').on('change', function(e){
            e.preventDefault();
            $.ajax({
                url : baseURL + '/penduduk/cari-kk',
                type: 'GET',
                data: {
                    nik: $("#nomor_kk_penduduk").val()
                },
                before: function () {
                    $("#cari-nik").html(`<img height="20px" src="${baseURL}/storage/loading.gif" alt="">`);
                },
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let dataPengikut = "";
                    for(let i=0; i < data.length; i++){
                        dataPengikut += "<tr><td>" + data[i].nomor_urut_dalam_kk + "</td><td>" + data[i].nik + "</td><td>" + data[i].kk + "</td><td>" + data[i].nama + "</td></tr>";
                    }
                    $('#anggota_keluarga').html(dataPengikut)
                }
            })
        })
        $('.save').on('click', function(e){
            e.preventDefault();
            let nik = $('#nomor_induk_penduduk').val();
            let tgl_pindah = $('#tgl_pindah').val();
            let kota_tujuan = $('#kota_tujuan').val();
            let kecamatan_tujuan = $('#kecamatan_tujuan').val();
            let kelurahan_tujuan = $('#kelurahan_tujuan').val();
            let alamat_tujuan = $('#alamat_tujuan').val();
            if (kota_tujuan == undefined || kota_tujuan == "") {
                alertFail('Harap kota tujuan diisi');
                return false;
            }else if(kecamatan_tujuan == undefined || kecamatan_tujuan == "") {
                alertFail('Harap kecamatan tujuan diisi');
                return false;
            }else if(kelurahan_tujuan == undefined || kelurahan_tujuan == "") {
                alertFail('Harap kelurahan tujuan diisi');
                return false;
            }else if(alamat_tujuan == undefined || alamat_tujuan == "") {
                alertFail('Harap alamat tujuan diisi');
                return false;
            }else {
               return true;
            }
        })

        $(window).keydown(function(event){
            if( (event.keyCode == 13) && (validationFunction() == false) ) {
            event.preventDefault();
            return false;
            }
        });

        $('#provinsi_id').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kabupaten')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kota_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kota_tujuan += '<option value='+ data[i].city_id +'>' + data[i].city_name+ '</option>';
                    }
                   $("#kota_tujuan").html(kota_tujuan);
                }
            });
        })

        $('#kota_tujuan').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kecamatan')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kecamatan_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kecamatan_tujuan += '<option value='+ data[i].dis_id +'>' + data[i].dis_name+ '</option>';
                    }
                   $("#kecamatan_tujuan").html(kecamatan_tujuan);
                }
            });
        })

        $('#kecamatan_tujuan').on('change', function(){
            let id = $(this).val();
            $.ajax({
                type: "get",
                dataType: "html",
                url: "{{url('kelurahan')}}/"+id,
                success: function(response){
                    data = JSON.parse(response)
                    console.log(data);
                    let kecamatan_tujuan = "";
                    for(let i=0; i < data.length; i++){
                        kecamatan_tujuan += '<option value='+ data[i].subdis_id +'>' + data[i].subdis_name+ '</option>';
                    }
                   $("#kelurahan_tujuan").html(kecamatan_tujuan);
                }
            });

        })

    });
    var createMutasi = function() {
        $('#modal-edit #nik').val("");
        $('#modal-edit #nama_penduduk').val("");
        $('#modal-edit #tgl_pindah').val("");
        $('#modal-edit #kota').val("");
        $('#modal-edit #kecamatan').val("");
        $('#modal-edit #kelurahan').val("");
        $('#modal-edit #alamat').val("");
        $('#modal-edit').modal("show");
    }

    function validationFunction() {
        let good= true;
        let nik = $('#nomor_induk_penduduk').val();
        let tgl_pindah = $('#tgl_pindah').val();
        let kota_tujuan = $('#kota_tujuan').val();
        let kecamatan_tujuan = $('#kecamatan_tujuan').val();
        let kelurahan_tujuan = $('#kelurahan_tujuan').val();
        let alamat_tujuan = $('#alamat_tujuan').val();
        if (kota_tujuan == undefined || kota_tujuan == "") {
            alertFail('Harap kota tujuan diisi');
            good= false;
        }else if(kecamatan_tujuan == undefined || kecamatan_tujuan == "") {
            alertFail('Harap kecamatan tujuan diisi');
            good= false;
        }else if(kelurahan_tujuan == undefined || kelurahan_tujuan == "") {
            alertFail('Harap kelurahan tujuan diisi');
            good= false;
        }else if(alamat_tujuan == undefined || alamat_tujuan == "") {
            alertFail('Harap alamat tujuan diisi');
            good= false;
        }
        if(good) {
            return true;
        }
        return false;
    }
</script>
@endpush
