<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;

use PhpOffice\PhpSpreadsheet\Cell\Cell;

class RekapPendudukExport implements WithCustomValueBinder,FromView,ShouldAutoSize
{
    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        return true;
    }

    public function view(): View
    {
        $collection = " SELECT
        x.*,
        ((
                awal_month_wna_l + lhr_month_wna_l + dtg_month_wna_l
            ) - ( mngl_month_wna_l + pndh_month_wna_l )) AS total_wna_l,
        ((
                awal_month_wna_p + lhr_month_wna_p + dtg_month_wna_p
            ) - ( mngl_month_wna_p + pndh_month_wna_p )) AS total_wna_p,
        ((
                awal_month_wni_l + lhr_month_wni_l + dtg_month_wni_l
            ) - ( mngl_month_wni_l + pndh_month_wni_l )) AS total_wni_l,
        ((
                awal_month_wni_p + lhr_month_wni_p + dtg_month_wni_p
            ) - ( mngl_month_wni_p + pndh_month_wni_p )) AS total_wni_p,
            (jml_kk + jml_bln_kk) AS total_kk,
            (jml_anggota + jml_bln_anggota) as total_anggota,
            ((jml_kk + jml_bln_kk) +  (jml_anggota + jml_bln_anggota)) As total_jiwa
    FROM
        (
        SELECT
            id,
            nama,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
            ) AS awal_month_wni_l,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                ) AS awal_month_wni_p,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                ) AS awal_month_wna_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                ) AS awal_month_wna_p,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND detail_dusun_id = dusun.id
                AND status_hubungan_dalam_keluarga_id = 1
                ) AS jml_kk,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND detail_dusun_id = dusun.id
                AND status_hubungan_dalam_keluarga_id <> 1
            ) AS jml_anggota,
            ((
                SELECT
                    count( 1 )
                FROM
                    penduduk
                WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND detail_dusun_id = dusun.id
                    AND status_hubungan_dalam_keluarga_id = 1
                    )+(
                SELECT
                    count( 1 )
                FROM
                    penduduk
                WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND detail_dusun_id = dusun.id
                    AND status_hubungan_dalam_keluarga_id <> 1
                )) AS total_pend,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                ) AS lhr_month_wni_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                ) AS lhr_month_wni_p,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                ) AS lhr_month_wna_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
            ) AS lhr_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 3
            ) AS dtg_month_wna_l,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 3
            ) AS dtg_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 3
            ) AS dtg_month_wni_l,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 3
                ) AS dtg_month_wni_p,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 4
                ) AS mngl_month_wni_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 4
            ) AS mngl_month_wni_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 4
            ) AS mngl_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 4
                ) AS mngl_month_wna_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 5
                ) AS pndh_month_wni_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 5
            ) AS pndh_month_wni_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 5
            ) AS pndh_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 5
            ) AS pndh_month_wna_l,(
        SELECT count( 1 ) FROM penduduk WHERE
			created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
			AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
			AND detail_dusun_id = dusun.id
			AND status_hubungan_dalam_keluarga_id = 1
			) AS jml_bln_kk,(
		SELECT count( 1 ) FROM penduduk WHERE
			created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
			AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
			AND detail_dusun_id = dusun.id
			AND status_hubungan_dalam_keluarga_id <> 1
		) AS jml_bln_anggota
        FROM
        dusun
        ) AS x";
        $vCollPenerimaanSts = DB::select($collection);
        $vCollPenerimaan = collect(DB::select($collection));
        $allDusuns = $vCollPenerimaan;
        $allDusuns->all();
        return view('kasi-pemerintahan.export-rekap-penduduk', compact('allDusuns'));
    }
}
