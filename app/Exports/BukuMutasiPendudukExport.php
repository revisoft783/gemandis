<?php

namespace App\Exports;

use App\Penduduk;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\Cell;

class BukuMutasiPendudukExport implements WithCustomValueBinder,FromView,ShouldAutoSize
{
    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        return true;
    }

    public function view(): View
    {
        $penduduks = Penduduk::with(['kotaTujuan','kecamatanTujuan','kelurahanTujuan'])->whereIn('status_penduduk_id',[3,4,5])->orderBy('nik','Asc')->get();
        return view('kasi-pemerintahan.export-buku-mutasi', compact('penduduks'));
    }
}
