<?php

namespace App\Providers;

use App\CetakSurat;
use App\DetailCetak;
use App\Observers\CetakSuratObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Observe
     * Model => Observer
     */
    private $observers = [
        CetakSurat::class      => CetakSuratObserver::class,
    ];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->observers as $model => $observer) {
            $model::observe($observer);
        }
    }
}
