<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind('App\Services\ProvinceService','App\Services\Impl\ProvinceServiceImpl');
        $this->app->bind('App\Services\NegaraService','App\Services\Impl\NegaraServiceImpl');
    }
}
