<?php

namespace App\Providers;

use App\Pekerjaan;
use App\Pendidikan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $pendidikan =  Pendidikan::all();
        $pekerjaan =  Pekerjaan::all();
        View::share('pendidikan',$pendidikan);
        View::share('pekerjaan',$pekerjaan);
    }
}
