<?php

namespace App\F1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DafdukHeader extends Model
{
    use HasFactory;
    protected $table = "dafduk_headers";

}
