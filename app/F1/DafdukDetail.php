<?php

namespace App\F1;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DafdukDetail extends Model
{
    use HasFactory;
    protected $table = "dafduk_details";

    public function headers()
    {
        return $this->hasBelongTo('App\F1\DafdukHeader','id_hdr','id');
    }

    public function pekerjaan()
    {
        return $this->belongsTo('App\Pekerjaan');
    }

    public function pendidikan()
    {
        return $this->belongsTo('App\Pendidikan');
    }
}
