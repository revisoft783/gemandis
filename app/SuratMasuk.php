<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuratMasuk extends Model
{
    protected $table = "surat_masuk";
    protected $guarded = [];

    public function kode_surat()
    {
        return $this->hasMany('App\KodeSurat');
    }

    public function detail()
    {
        return $this->belongsTo('App\Models\SuratMasukDetail','id','surat_masuk_id');
    }
}
