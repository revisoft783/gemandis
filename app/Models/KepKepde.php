<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class KepKepde
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property string|null $nomor_kepdes
 * @property Carbon|null $tgl_kepdes
 * @property string|null $tentang
 * @property string|null $uraian
 * @property string|null $nomor_lapor
 * @property Carbon|null $tgl_lapor
 * @property string|null $keterangan
 * @property string|null $document_pdf
 * @property string|null $document_word
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class KepKepde extends Model
{
	protected $table = 'kep_kepdes';

	protected $casts = [
		'no_urut' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl_kepdes',
		'tgl_lapor'
	];

	protected $fillable = [
		'no_urut',
		'nomor_kepdes',
		'tgl_kepdes',
		'tentang',
		'uraian',
		'nomor_lapor',
		'tgl_lapor',
		'keterangan',
		'document_pdf',
		'document_word',
		'created_by',
		'updated_by'
	];
}
