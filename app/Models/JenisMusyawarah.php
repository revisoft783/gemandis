<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JenisMusyawarah
 * 
 * @property int $id
 * @property string|null $nama
 * 
 * @property Collection|Musyawarah[] $musyawarahs
 *
 * @package App\Models
 */
class JenisMusyawarah extends Model
{
	protected $table = 'jenis_musyawarah';
	public $timestamps = false;

	protected $fillable = [
		'nama'
	];

	public function musyawarahs()
	{
		return $this->hasMany(Musyawarah::class, 'jenis_musyawarah');
	}
}
