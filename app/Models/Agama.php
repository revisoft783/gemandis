<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Agama
 * 
 * @property int $id
 * @property string $nama
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|PemerintahanDesa[] $pemerintahan_desas
 * @property Collection|Penduduk[] $penduduks
 *
 * @package App\Models
 */
class Agama extends Model
{
	protected $table = 'agama';

	protected $fillable = [
		'nama'
	];

	public function pemerintahan_desas()
	{
		return $this->hasMany(PemerintahanDesa::class);
	}

	public function penduduks()
	{
		return $this->hasMany(Penduduk::class);
	}
}
