<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class KtpReader
 *
 * @property int $id
 * @property string|null $nik
 * @property string|null $nama
 * @property string|null $ttl
 * @property Carbon|null $tgllhr
 * @property string|null $gdr
 * @property string|null $goldar
 * @property string|null $almt
 * @property string|null $rt
 * @property string|null $rw
 * @property string|null $kec
 * @property string|null $kel
 * @property string|null $agm
 * @property string|null $mrtl
 * @property string|null $pkj
 * @property string|null $prv
 * @property string|null $kab
 * @property string|null $kwgn
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class KtpReader extends Model
{
	protected $table = 'ktp_reader';

	protected $casts = [
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgllhr'
	];

	protected $fillable = [
		'nik',
		'nama',
		'ttl',
		'tgllhr',
		'gdr',
		'goldar',
		'almt',
		'rt',
		'rw',
		'kec',
		'kel',
		'agm',
		'mrtl',
		'pkj',
		'prv',
		'kab',
		'kwgn',
        'tag_card_id',
		'created_by',
		'updated_by'
	];
}
