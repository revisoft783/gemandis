<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Negara
 * 
 * @property int $id_country
 * @property string $negara_name
 * @property string $code1
 * @property string $code2
 * @property string $flag
 *
 * @package App\Models
 */
class Negara extends Model
{
	protected $table = 'negara';
	protected $primaryKey = 'id_country';
	public $timestamps = false;

	protected $fillable = [
		'negara_name',
		'code1',
		'code2',
		'flag'
	];
}
