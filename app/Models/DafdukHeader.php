<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DafdukHeader
 * 
 * @property int $id
 * @property string|null $trx
 * @property string|null $no_kk
 * @property string|null $nama_kk
 * @property string|null $alamat_kk
 * @property string|null $email_kk
 * @property int|null $kd_profinsi
 * @property int|null $kd_kabupaten
 * @property int|null $kd_kelurahan
 * @property int|null $kd_kampung
 * @property string|null $alamat
 * @property string|null $kota
 * @property string|null $negara
 * @property string|null $kode_pos_wn
 * @property string|null $telp_wn
 * @property int|null $kd_negara
 * @property int|null $kd_kbri
 * @property string|null $jml_anggota
 * @property string|null $user_input
 * @property string|null $user_update
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|DafdukDetail[] $dafduk_details
 *
 * @package App\Models
 */
class DafdukHeader extends Model
{
	protected $table = 'dafduk_headers';

	protected $casts = [
		'kd_profinsi' => 'int',
		'kd_kabupaten' => 'int',
		'kd_kelurahan' => 'int',
		'kd_kampung' => 'int',
		'kd_negara' => 'int',
		'kd_kbri' => 'int'
	];

	protected $fillable = [
		'trx',
		'no_kk',
		'nama_kk',
		'alamat_kk',
		'email_kk',
		'kd_profinsi',
		'kd_kabupaten',
		'kd_kelurahan',
		'kd_kampung',
		'alamat',
		'kota',
		'negara',
		'kode_pos_wn',
		'telp_wn',
		'kd_negara',
		'kd_kbri',
		'jml_anggota',
		'user_input',
		'user_update'
	];

	public function dafduk_details()
	{
		return $this->hasMany(DafdukDetail::class, 'id_hdr');
	}
}
