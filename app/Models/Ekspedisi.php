<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ekspedisi
 * 
 * @property int $id
 * @property int $id_agenda
 * @property string|null $penerima
 * @property string|null $ttd_penerima
 * @property string|null $foto_penerima
 * @property string|null $keterangan
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 * @property AgendaSurat $agenda_surat
 *
 * @package App\Models
 */
class Ekspedisi extends Model
{
	protected $table = 'ekspedisi';

	protected $casts = [
		'id_agenda' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'id_agenda',
		'penerima',
		'ttd_penerima',
		'foto_penerima',
		'keterangan',
		'created_by',
		'updated_by'
	];

	public function agenda_surat()
	{
		return $this->belongsTo(AgendaSurat::class, 'id_agenda');
	}
}
