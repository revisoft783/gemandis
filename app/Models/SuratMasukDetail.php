<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SuratMasukDetail
 * 
 * @property int $id
 * @property int|null $surat_masuk_id
 * @property string|null $berkas
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class SuratMasukDetail extends Model
{
	protected $table = 'surat_masuk_detail';

	protected $casts = [
		'surat_masuk_id' => 'int'
	];

	protected $fillable = [
		'surat_masuk_id',
		'berkas'
	];
}
