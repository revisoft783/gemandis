<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Analisi
 * 
 * @property int $id
 * @property string $nama
 * @property int $subjek
 * @property bool $status_analisis
 * @property int|null $bilangan_pembagi
 * @property string $deskripsi
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property HasilKlasifikasi $hasil_klasifikasi
 * @property Collection|Indikator[] $indikators
 * @property Collection|Kategori[] $kategoris
 * @property Collection|Klasifikasi[] $klasifikasis
 * @property Collection|Periode[] $periodes
 *
 * @package App\Models
 */
class Analisi extends Model
{
	protected $table = 'analisis';

	protected $casts = [
		'subjek' => 'int',
		'status_analisis' => 'bool',
		'bilangan_pembagi' => 'int'
	];

	protected $fillable = [
		'nama',
		'subjek',
		'status_analisis',
		'bilangan_pembagi',
		'deskripsi'
	];

	public function hasil_klasifikasi()
	{
		return $this->hasOne(HasilKlasifikasi::class, 'analisis_id');
	}

	public function indikators()
	{
		return $this->hasMany(Indikator::class, 'analisis_id');
	}

	public function kategoris()
	{
		return $this->hasMany(Kategori::class, 'analisis_id');
	}

	public function klasifikasis()
	{
		return $this->hasMany(Klasifikasi::class, 'analisis_id');
	}

	public function periodes()
	{
		return $this->hasMany(Periode::class, 'analisis_id');
	}
}
