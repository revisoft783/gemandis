<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AgendaSurat
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property Carbon|null $tgl_surat_kirim
 * @property string|null $no_surat
 * @property Carbon|null $tgl_surat_keluar
 * @property string|null $penerima
 * @property string|null $isi_singkat
 * @property string|null $keterangan
 * @property string|null $word
 * @property string|null $pdf
 * @property int|null $status
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Ekspedisi[] $ekspedisis
 *
 * @package App\Models
 */
class AgendaSurat extends Model
{
	protected $table = 'agenda_surat';

	protected $casts = [
		'no_urut' => 'int',
		'status' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl_surat_kirim',
		'tgl_surat_keluar'
	];

	protected $fillable = [
		'no_urut',
		'tgl_surat_kirim',
		'no_surat',
		'tgl_surat_keluar',
		'penerima',
		'isi_singkat',
		'keterangan',
		'word',
		'pdf',
		'status',
		'created_by',
		'updated_by'
	];

	public function ekspedisis()
	{
		return $this->hasMany(Ekspedisi::class, 'id_agenda');
	}
}
