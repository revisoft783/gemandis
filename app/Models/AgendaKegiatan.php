<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AgendaKegiatan
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property Carbon|null $tgl
 * @property int|null $bidang_id
 * @property int|null $jenis_kegiatan
 * @property string|null $uraian_kegiatan
 * @property string|null $peserta
 * @property Carbon|null $tgl_kegiatan
 * @property string|null $catatan_kegiatan
 * @property string|null $laporan_kegiatan
 * @property string|null $documentasi
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 * @property Bidang|null $bidang
 *
 * @package App\Models
 */
class AgendaKegiatan extends Model
{
	protected $table = 'agenda_kegiatan';

	protected $casts = [
		'no_urut' => 'int',
		'bidang_id' => 'int',
		'jenis_kegiatan' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl',
		'tgl_kegiatan'
	];

	protected $fillable = [
		'no_urut',
		'tgl',
		'bidang_id',
		'jenis_kegiatan',
		'uraian_kegiatan',
		'peserta',
		'tgl_kegiatan',
		'catatan_kegiatan',
		'laporan_kegiatan',
		'documentasi',
		'created_by',
		'updated_by'
	];

	public function bidang()
	{
		return $this->belongsTo(Bidang::class);
	}
}
