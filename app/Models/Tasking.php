<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tasking
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property Carbon|null $tgl_pelimpahan
 * @property string|null $uraian
 * @property Carbon|null $tgl_penyelesaian
 * @property string|null $tujuan
 * @property int $bidang_id
 * @property int|null $disposisi_id
 * @property string|null $document_word
 * @property string|null $document_pdf
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 * @property Bidang $bidang
 *
 * @package App\Models
 */
class Tasking extends Model
{
	protected $table = 'tasking';

	protected $casts = [
		'no_urut' => 'int',
		'bidang_id' => 'int',
		'disposisi_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl_pelimpahan',
		'tgl_penyelesaian'
	];

	protected $fillable = [
		'no_urut',
		'tgl_pelimpahan',
		'uraian',
		'tgl_penyelesaian',
		'tujuan',
		'bidang_id',
		'disposisi_id',
		'document_word',
		'document_pdf',
		'created_by',
		'updated_by'
	];

	public function bidang()
	{
		return $this->belongsTo(Bidang::class);
	}
}
