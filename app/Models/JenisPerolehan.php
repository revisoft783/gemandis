<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JenisPerolehan
 * 
 * @property int $id
 * @property string|null $nama
 * 
 * @property Collection|InventarisDesa[] $inventaris_desas
 *
 * @package App\Models
 */
class JenisPerolehan extends Model
{
	protected $table = 'jenis_perolehan';
	public $timestamps = false;

	protected $fillable = [
		'nama'
	];

	public function inventaris_desas()
	{
		return $this->hasMany(InventarisDesa::class, 'asal_perolehan');
	}
}
