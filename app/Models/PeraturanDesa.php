<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PeraturanDesa
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property int|null $jns_peraturan
 * @property string|null $nomor_penetapan
 * @property Carbon|null $tgl_penetapan
 * @property string|null $tentang
 * @property string|null $uraian
 * @property Carbon|null $tgl_kesepakatan
 * @property string|null $nomor_dilaporkan
 * @property Carbon|null $tgl_dilaporkan
 * @property string|null $nomor_udld
 * @property Carbon|null $tgl_udld
 * @property string|null $nomor_udbd
 * @property Carbon|null $tgl_udbd
 * @property string|null $keterangan
 * @property string|null $document_word
 * @property string|null $document_pdf
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class PeraturanDesa extends Model
{
	protected $table = 'peraturan_desa';

	protected $casts = [
		'no_urut' => 'int',
		'jns_peraturan' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl_penetapan',
		'tgl_kesepakatan',
		'tgl_dilaporkan',
		'tgl_udld',
		'tgl_udbd'
	];

	protected $fillable = [
		'no_urut',
		'jns_peraturan',
		'nomor_penetapan',
		'tgl_penetapan',
		'tentang',
		'uraian',
		'tgl_kesepakatan',
		'nomor_dilaporkan',
		'tgl_dilaporkan',
		'nomor_udld',
		'tgl_udld',
		'nomor_udbd',
		'tgl_udbd',
		'keterangan',
		'document_word',
		'document_pdf',
		'created_by',
		'updated_by'
	];
}
