<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AkseptorKb
 * 
 * @property int $id
 * @property string $nama
 * @property bool $sex
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Penduduk[] $penduduks
 *
 * @package App\Models
 */
class AkseptorKb extends Model
{
	protected $table = 'akseptor_kb';

	protected $casts = [
		'sex' => 'bool'
	];

	protected $fillable = [
		'nama',
		'sex'
	];

	public function penduduks()
	{
		return $this->hasMany(Penduduk::class);
	}
}
