<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Musyawarah
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property Carbon|null $tgl
 * @property int|null $jenis_musyawarah
 * @property string|null $narasumber
 * @property string|null $peserta
 * @property string|null $isi_singkat
 * @property Carbon|null $tgl_undangan
 * @property string|null $perihal
 * @property string|null $peserta_undangan
 * @property string|null $pdf_undangan
 * @property string|null $foto
 * @property string|null $notulen_word
 * @property string|null $notulen_pdf
 * @property string|null $daftar_hadir_word
 * @property string|null $daftar_hadir_pdf
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 *
 * @package App\Models
 */
class Musyawarah extends Model
{
	protected $table = 'musyawarah';

	protected $casts = [
		'no_urut' => 'int',
		'jenis_musyawarah' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl',
		'tgl_undangan'
	];

	protected $fillable = [
		'no_urut',
		'tgl',
		'jenis_musyawarah',
		'narasumber',
		'peserta',
		'isi_singkat',
		'tgl_undangan',
		'perihal',
		'peserta_undangan',
		'pdf_undangan',
		'foto',
		'notulen_word',
		'notulen_pdf',
		'daftar_hadir_word',
		'daftar_hadir_pdf',
		'created_by',
		'updated_by'
	];

	public function jenis_musyawarah()
	{
		return $this->belongsTo(JenisMusyawarah::class, 'jenis_musyawarah');
	}
}
