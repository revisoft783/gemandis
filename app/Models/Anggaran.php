<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Anggaran
 * 
 * @property int $id
 * @property string|null $nama
 * 
 * @property InventarisDesa $inventaris_desa
 * @property Collection|InventarisDesa[] $inventaris_desas
 *
 * @package App\Models
 */
class Anggaran extends Model
{
	protected $table = 'anggaran';
	public $timestamps = false;

	protected $fillable = [
		'nama'
	];

	public function inventaris_desa()
	{
		return $this->belongsTo(InventarisDesa::class, 'id', 'anggaran_id');
	}

	public function inventaris_desas()
	{
		return $this->hasMany(InventarisDesa::class);
	}
}
