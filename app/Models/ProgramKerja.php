<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProgramKerja
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property Carbon|null $tgl
 * @property Carbon|null $year
 * @property int|null $bidang
 * @property string|null $program_kerja
 * @property string|null $uraian_program
 * @property string|null $sasaran
 * @property string|null $indikator
 * @property string|null $tujuan
 * @property string|null $sasaran_kebijakan
 * @property string|null $capaian_program
 * @property string|null $keterangan
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 *
 * @package App\Models
 */
class ProgramKerja extends Model
{
	protected $table = 'program_kerja';

	protected $casts = [
		'no_urut' => 'int',
		'bidang' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl',
		'year'
	];

	protected $fillable = [
		'no_urut',
		'tgl',
		'year',
		'bidang',
		'program_kerja',
		'uraian_program',
		'sasaran',
		'indikator',
		'tujuan',
		'sasaran_kebijakan',
		'capaian_program',
		'keterangan',
		'created_by',
		'updated_by'
	];

	public function bidang()
	{
		return $this->belongsTo(Bidang::class, 'bidang');
	}
}
