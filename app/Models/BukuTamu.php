<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BukuTamu
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property Carbon|null $tgl_tamu
 * @property Carbon|null $years
 * @property int|null $jns_tamu
 * @property string|null $nama
 * @property string|null $nik
 * @property string|null $alamat
 * @property string|null $instansi
 * @property int $tujuan_id
 * @property string|null $keperluan
 * @property Carbon|null $jam_datang
 * @property string|null $foto
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 * @property Bidang $bidang
 *
 * @package App\Models
 */
class BukuTamu extends Model
{
	protected $table = 'buku_tamu';

	protected $casts = [
		'no_urut' => 'int',
		'jns_tamu' => 'int',
		'tujuan_id' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl_tamu',
		'years',
		'jam_datang'
	];

	protected $fillable = [
		'no_urut',
		'tgl_tamu',
		'years',
		'jns_tamu',
		'nama',
		'nik',
		'alamat',
		'instansi',
		'tujuan_id',
		'keperluan',
		'jam_datang',
		'foto',
		'created_by',
		'updated_by'
	];

	public function bidang()
	{
		return $this->belongsTo(Bidang::class, 'tujuan_id');
	}
}
