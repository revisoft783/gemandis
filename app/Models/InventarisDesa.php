<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InventarisDesa
 * 
 * @property int $id
 * @property int|null $no_urut
 * @property string|null $no_barcode
 * @property string|null $jenis_barang
 * @property Carbon|null $tgl_perolehan
 * @property int|null $asal_perolehan
 * @property int|null $anggaran_id
 * @property string|null $foto
 * @property int|null $keadaan_barang_bangunan
 * @property string|null $word
 * @property string|null $pdf
 * @property int|null $created_by
 * @property Carbon|null $created_at
 * @property int|null $updated_by
 * @property Carbon|null $updated_at
 * 
 * @property JenisPerolehan|null $jenis_perolehan
 * @property Anggaran $anggaran
 *
 * @package App\Models
 */
class InventarisDesa extends Model
{
	protected $table = 'inventaris_desa';

	protected $casts = [
		'no_urut' => 'int',
		'asal_perolehan' => 'int',
		'anggaran_id' => 'int',
		'keadaan_barang_bangunan' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $dates = [
		'tgl_perolehan'
	];

	protected $fillable = [
		'no_urut',
		'no_barcode',
		'jenis_barang',
		'tgl_perolehan',
		'asal_perolehan',
		'anggaran_id',
		'foto',
		'keadaan_barang_bangunan',
		'word',
		'pdf',
		'created_by',
		'updated_by'
	];

	public function jenis_perolehan()
	{
		return $this->belongsTo(JenisPerolehan::class, 'asal_perolehan');
	}

	public function anggaran()
	{
		return $this->hasOne(Anggaran::class, 'id', 'anggaran_id');
	}
}
