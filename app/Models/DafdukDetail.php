<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Agama;
use App\Darah;
use App\Pekerjaan;
use App\Pendidikan;
use App\StatusHubunganDalamKeluarga;
use App\StatusPerkawinan;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DafdukDetail
 * 
 * @property int $id
 * @property int|null $id_hdr
 * @property string|null $nama_lenkap
 * @property string|null $gelar_dpn
 * @property string|null $gelar_blkg
 * @property string|null $nmr_paspor
 * @property Carbon|null $tgl_expired_paspor
 * @property string|null $nama_sponsor
 * @property int|null $tipe_sponsor
 * @property string|null $alamat_sponsor
 * @property int|null $jns_kelamin
 * @property string|null $tempat_lahir
 * @property Carbon|null $tgl_lahir
 * @property string|null $kewarganegaraan
 * @property string|null $nomor_sk
 * @property int|null $akta_lahir
 * @property string|null $nomor_akta_lahir
 * @property int|null $gol_darah
 * @property int|null $agama
 * @property string|null $nama_organisasi_yme
 * @property int|null $status_perkawinan
 * @property int|null $akta_perkawinan
 * @property string|null $nomor_akta_perkawinan
 * @property Carbon|null $tgl_perkawinan
 * @property int|null $akta_cerai
 * @property string|null $nomor_akta_cerai
 * @property Carbon|null $tgl_cerai
 * @property int|null $status_keluarga
 * @property int|null $kelainan_fisik_mental
 * @property int|null $penyandang_cacat
 * @property int|null $pendidikan_id
 * @property int|null $pekerjaan_id
 * @property string|null $nomor_itas_itap
 * @property string|null $tempat_terbit_itas_itap
 * @property Carbon|null $tgl_itas_itap
 * @property Carbon|null $tgl_itas_itap_expired
 * @property string|null $tempat_datang_pertama
 * @property Carbon|null $tgl_kedatangan_pertama
 * @property string|null $nik_ibu
 * @property string|null $nama_ibu
 * @property string|null $nik_ayah
 * @property string|null $nama_ayah
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property DafdukHeader|null $dafduk_header
 * @property Pekerjaan|null $pekerjaan
 * @property Pendidikan|null $pendidikan
 *
 * @package App\Models
 */
class DafdukDetail extends Model
{
	protected $table = 'dafduk_details';

	protected $casts = [
		'id_hdr' => 'int',
		'tipe_sponsor' => 'int',
		'jns_kelamin' => 'int',
		'akta_lahir' => 'int',
		'gol_darah' => 'int',
		'agama' => 'int',
		'status_perkawinan' => 'int',
		'akta_perkawinan' => 'int',
		'akta_cerai' => 'int',
		'status_keluarga' => 'int',
		'kelainan_fisik_mental' => 'int',
		'penyandang_cacat' => 'int',
		'pendidikan_id' => 'int',
		'pekerjaan_id' => 'int'
	];

	protected $dates = [
		'tgl_expired_paspor',
		'tgl_lahir',
		'tgl_perkawinan',
		'tgl_cerai',
		'tgl_itas_itap',
		'tgl_itas_itap_expired',
		'tgl_kedatangan_pertama'
	];

	protected $fillable = [
		'id_hdr',
		'nama_lenkap',
		'gelar_dpn',
		'gelar_blkg',
		'nmr_paspor',
		'tgl_expired_paspor',
		'nama_sponsor',
		'tipe_sponsor',
		'alamat_sponsor',
		'jns_kelamin',
		'tempat_lahir',
		'tgl_lahir',
		'kewarganegaraan',
		'nomor_sk',
		'akta_lahir',
		'nomor_akta_lahir',
		'gol_darah',
		'agama',
		'nama_organisasi_yme',
		'status_perkawinan',
		'akta_perkawinan',
		'nomor_akta_perkawinan',
		'tgl_perkawinan',
		'akta_cerai',
		'nomor_akta_cerai',
		'tgl_cerai',
		'status_keluarga',
		'kelainan_fisik_mental',
		'penyandang_cacat',
		'pendidikan_id',
		'pekerjaan_id',
		'nomor_itas_itap',
		'tempat_terbit_itas_itap',
		'tgl_itas_itap',
		'tgl_itas_itap_expired',
		'tempat_datang_pertama',
		'tgl_kedatangan_pertama',
		'nik_ibu',
		'nama_ibu',
		'nik_ayah',
		'nama_ayah'
	];

	public function dafduk_header()
	{
		return $this->belongsTo(DafdukHeader::class, 'id_hdr');
	}

	public function pekerjaan()
	{
		return $this->belongsTo(Pekerjaan::class);
	}

	public function pendidikan()
	{
		return $this->belongsTo(Pendidikan::class);
	}

    /**
     * @return string
     */
    public function scopeGolDarah()
    {
        /**
         * @author Afes Oktavianus
         * @since 26-10-2021
         * @version Feature/2.2.2 - Depo & WD in coin/cryto
         * @todo Get use method in Deposit user
         */
        return Darah::find($this->gol_darah)->golongan;
    }

    /**
     * @return string
     */
    public function scopeKewarganegaraan()
    {        
        return $this->kewarganegaraan == "1" ? 'WNI' : ($this->kewarganegaraan == "2" ? 'WNA' : "Dua Kewarganegaraan");
    }

    public function scopeAgama()
	{
		return Agama::find($this->agama)->nama;
	}

    /**
     * @return string
     */
    public function scopeStatusPerkawinan()
    {        
        return StatusPerkawinan::find($this->status_perkawinan)->nama;
    }

    /**
     * @return string
     */
    public function scopeStatusHubungan()
    {        
        return StatusHubunganDalamKeluarga::find($this->status_keluarga)->nama;
    }
}
