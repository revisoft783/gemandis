<?php

namespace App\Services\Impl;

use App\Models\Negara;
use App\Services\NegaraService;

class NegaraServiceImpl extends BaseServiceImpl implements NegaraService
{
    public function __construct(Negara $model) {
        parent::__construct($model);
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Negara::class;
    }

    public static function getNegaraAllStatic()
    {
        return Negara::select('negara_name','id_country')->get();
    }
}