<?php

namespace App\Services\Impl;

use App\Models\City;
use App\Models\District;
use App\Models\Province;
use App\Models\Subdistrict;
use App\Services\ProvinceService;

class ProvinceServiceImpl extends BaseServiceImpl implements ProvinceService
{
    public function __construct(Province $model) {
        parent::__construct($model);
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Province::class;
    }

    public static function getprovinsiAllStatic()
    {
        return Province::select('prov_name','prov_id')->get();
    }

    public function getCities($id)
    {
        return City::select('city_name','city_id')->whereProvId($id)->get();
    }

    public function getDistrict($id)
    {
        return District::select('dis_name','dis_id')->whereCityId($id)->get();
    }

    public function getSubDistrict($id)
    {
        return Subdistrict::select('subdis_name','subdis_id')->whereDisId($id)->get();
    }
}
