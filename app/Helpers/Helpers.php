<?php

use Illuminate\Support\Carbon;

if (! function_exists('tgl')) {
    function tgl($tanggal)
    {
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}
if (! function_exists('bulan')) {
    function bulan($bulan)
    {
        $data = array (
            '01'    =>  'Januari',
            '02'    =>  'Februari',
            '03'    =>  'Maret',
            '04'    =>  'April',
            '05'    =>  'Mei',
            '06'    =>  'Juni',
            '07'    =>  'Juli',
            '08'    =>  'Agustus',
            '09'    =>  'September',
            '10'    =>  'Oktober',
            '11'    =>  'November',
            '12'    =>  'Desember'
        );
        return $data[$bulan];
    }
}

function getTrx($length = 12)
{
    $characters = 'ABCDEFGHJKMNOPQRSTUVWXYZ123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getProvincies()
{
    $provinsi = ['' => 'Silahkan pilih provinsi...'];
    $provinsis = \App\Services\Impl\ProvinceServiceImpl::getprovinsiAllStatic()->pluck('prov_name', 'prov_id')->toArray();
    $provinsi += $provinsis;
    return $provinsi;
}

function getNegara()
{
    $provinsi = ['' => 'Silahkan pilih negara...'];
    $provinsis = \App\Services\Impl\NegaraServiceImpl::getNegaraAllStatic()->pluck('negara_name', 'id_country')->toArray();
    $provinsi += $provinsis;
    return $provinsi;
}

function statusPerkawinan($id)
{
    if($id == 1){
        $status = 'BK';
    }else if($id==2){
        $status = 'K';
    }else if($id==3){
        $status = 'CH';
    }else{
        $status = 'CM';
    }
    return $status;
}

if (! function_exists('dateFormatTime')) {
    function dateFormatTime($date,$format=NULL)
    {
        if ($format){
            $result = date($format,strtotime($date));
        }else{
            $result = date('d-m-Y H:i:s',strtotime($date));
        }
        return $result;
    }
}
