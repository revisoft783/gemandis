<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendOTPEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $nama;
    protected $otp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nama, $otp)
    {
        $this->nama = $nama;
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $nama = $this->nama;
        $otp = $this->otp;
        return $this->from('demo@gemandis.com')
                    ->view('email.send-otp')->with(['nama'=>$nama,'otp'=>$otp]);
    }
}
