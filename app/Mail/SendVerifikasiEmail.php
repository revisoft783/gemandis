<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendVerifikasiEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $nama;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nama)
    {
        //
        $this->user =  $nama;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('demo@gemandis.com')
                    ->view('email.send-email')->with(['nama'=>$this->nama]);
    }
}
