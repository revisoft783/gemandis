<?php

namespace App\Observers;

use App\CetakSurat as CetakSurat;
use App\DetailCetak;
use App\Models\SuratMasukDetail;
use App\Penduduk;
use App\Surat;
use App\SuratMasuk;
use GuzzleHttp\Promise\Create;

use function GuzzleHttp\json_encode;

class CetakSuratObserver
{
    /**
     * Handle the CetakSurat "created" event.
     *
     * @param  \App\Models\CetakSurat  $cetakSurat
     * @return void
     */
    public function created(CetakSurat $cetakSurat)
    {

    }

    /**
     * Handle the CetakSurat "updated" event.
     *
     * @param  \App\Models\CetakSurat  $cetakSurat
     * @return void
     */
    public function updated(CetakSurat $cetakSurat)
    {
        $surat = Surat::find($cetakSurat->surat_id);
        $berkasFile = DetailCetak::whereCetakSuratId($cetakSurat->id)->whereNotNull('berkas')->get();
        $berkas=[];
        foreach ($berkasFile as $value) {
            array_push($berkas,'public/surat-masuk/'.$value->berkas);
        }
        $berkasFiles =json_encode($berkas);
        $masuk = new SuratMasuk();
        $masuk->nomor_urut = $cetakSurat->id;
        $masuk->tanggal_penerimaan = date('Y-m-d');
        $masuk->nomor_surat = $cetakSurat->nomor;
        $masuk->kode_surat_id = $surat->kode_surat;
        $masuk->tanggal_surat = date('Y-m-d',strtotime($cetakSurat->created_at));
        $masuk->pengirim = Penduduk::whereNik($cetakSurat->nik)->first()->nama;
        $masuk->isi_singkat_atau_perihal = $surat->nama;
        $masuk->bidang_id = $surat->bidang_id;
        $masuk->save();


        foreach (json_decode($berkasFiles) as $value) {
            $data = [
                'surat_masuk_id' => $masuk->id,
                'berkas'=> $value
            ];
            SuratMasukDetail::create($data);
        }

    }

    /**
     * Handle the CetakSurat "deleted" event.
     *
     * @param  \App\Models\CetakSurat  $cetakSurat
     * @return void
     */
    public function deleted(CetakSurat $cetakSurat)
    {
        //
    }

    /**
     * Handle the CetakSurat "restored" event.
     *
     * @param  \App\Models\CetakSurat  $cetakSurat
     * @return void
     */
    public function restored(CetakSurat $cetakSurat)
    {
        //
    }

    /**
     * Handle the CetakSurat "force deleted" event.
     *
     * @param  \App\Models\CetakSurat  $cetakSurat
     * @return void
     */
    public function forceDeleted(CetakSurat $cetakSurat)
    {
        //
    }
}
