<?php

namespace App\Http\Controllers;

use App\Exports\BukuIndukPendudukExport;
use App\Exports\BukuMutasiPendudukExport;
use App\Exports\RekapPendudukExport;
use App\Penduduk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class KasiPemerintahanController extends Controller
{
    // Start Kasi Pemerintah
    public function index(Request $request)
    {
        return view('kasi-pemerintahan.index');
    }

    public function buku_induk(Request $request)
    {
        $penduduk = Penduduk::latest()->paginate(20);
        return view('kasi-pemerintahan.buku-induk',compact('penduduk'));
    }

    public function exportBukuInduk() {
        return Excel::download(new BukuIndukPendudukExport, 'Buku Induk Penduduk.xlsx');
    }

    public function buku_mutasi_penduduk(Request $request)
    {
        $penduduk = Penduduk::with(['kotaTujuan','kecamatanTujuan','kelurahanTujuan'])->whereIn('status_penduduk_id',[3,4,5])->latest()->paginate(20);
        return view('kasi-pemerintahan.mutasi-penduduk',compact('penduduk'));
    }

    public function exportBukuMutasi() {
        return Excel::download(new BukuMutasiPendudukExport, 'Buku Mutasi Penduduk.xlsx');
    }

    public function buku_rekap_penduduk(Request $request)
    {
        $collection = " SELECT
        x.*,
        ((
                awal_month_wna_l + lhr_month_wna_l + dtg_month_wna_l
            ) - ( mngl_month_wna_l + pndh_month_wna_l )) AS total_wna_l,
        ((
                awal_month_wna_p + lhr_month_wna_p + dtg_month_wna_p
            ) - ( mngl_month_wna_p + pndh_month_wna_p )) AS total_wna_p,
        ((
                awal_month_wni_l + lhr_month_wni_l + dtg_month_wni_l
            ) - ( mngl_month_wni_l + pndh_month_wni_l )) AS total_wni_l,
        ((
                awal_month_wni_p + lhr_month_wni_p + dtg_month_wni_p
            ) - ( mngl_month_wni_p + pndh_month_wni_p )) AS total_wni_p,
            (jml_kk + jml_bln_kk) AS total_kk,
            (jml_anggota + jml_bln_anggota) as total_anggota,
            ((jml_kk + jml_bln_kk) +  (jml_anggota + jml_bln_anggota)) As total_jiwa
        FROM ( SELECT id, nama, (
                SELECT count( 1 ) FROM penduduk WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND jenis_kelamin = 1
                    AND detail_dusun_id = dusun.id
                    AND kewarganegaraan = 1
                ) AS awal_month_wni_l, (
                SELECT count( 1 ) FROM penduduk WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND jenis_kelamin = 2
                    AND detail_dusun_id = dusun.id
                    AND kewarganegaraan = 1
                ) AS awal_month_wni_p,(
                SELECT count( 1 ) FROM penduduk WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND jenis_kelamin = 1
                    AND detail_dusun_id = dusun.id
                    AND kewarganegaraan = 2
                ) AS awal_month_wna_l,(
                SELECT count( 1 ) FROM penduduk WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND jenis_kelamin = 2
                    AND detail_dusun_id = dusun.id
                    AND kewarganegaraan = 2
                ) AS awal_month_wna_p,(
                SELECT count( 1 ) FROM penduduk WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND detail_dusun_id = dusun.id
                    AND status_hubungan_dalam_keluarga_id = 1
                ) AS jml_kk,(
                SELECT count( 1 ) FROM penduduk WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND detail_dusun_id = dusun.id
                    AND status_hubungan_dalam_keluarga_id <> 1
                ) AS jml_anggota,
            ((
                SELECT
                    count( 1 )
                FROM
                    penduduk
                WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND detail_dusun_id = dusun.id
                    AND status_hubungan_dalam_keluarga_id = 1
                    )+(
                SELECT
                    count( 1 )
                FROM
                    penduduk
                WHERE
                    date_format( created_at, '%Y-%m-%d' ) < DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                    AND detail_dusun_id = dusun.id
                    AND status_hubungan_dalam_keluarga_id <> 1
                )) AS total_pend,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                ) AS lhr_month_wni_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                ) AS lhr_month_wni_p,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                ) AS lhr_month_wna_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
            ) AS lhr_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 3
            ) AS dtg_month_wna_l,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 3
            ) AS dtg_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 3
            ) AS dtg_month_wni_l,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 3
                ) AS dtg_month_wni_p,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 4
                ) AS mngl_month_wni_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 4
            ) AS mngl_month_wni_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 4
            ) AS mngl_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 4
                ) AS mngl_month_wna_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 5
                ) AS pndh_month_wni_l,(
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 1
                AND status_penduduk_id = 5
            ) AS pndh_month_wni_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 2
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 5
            ) AS pndh_month_wna_p,
            (
            SELECT
                count( 1 )
            FROM
                penduduk
            WHERE
                created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
                AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
                AND jenis_kelamin = 1
                AND detail_dusun_id = dusun.id
                AND kewarganegaraan = 2
                AND status_penduduk_id = 5
            ) AS pndh_month_wna_l,(
        SELECT count( 1 ) FROM penduduk WHERE
			created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
			AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
			AND detail_dusun_id = dusun.id
			AND status_hubungan_dalam_keluarga_id = 1
			) AS jml_bln_kk,(
		SELECT count( 1 ) FROM penduduk WHERE
			created_at BETWEEN DATE_FORMAT( DATE( now()), '%Y-%m-01' )
			AND DATE_FORMAT( DATE( now()), '%Y-%m-%d' )
			AND detail_dusun_id = dusun.id
			AND status_hubungan_dalam_keluarga_id <> 1
		) AS jml_bln_anggota
        FROM
        dusun
        ) AS x";
        $vCollPenerimaanSts = DB::select($collection);
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[3])->latest()->paginate(20);

        $vCollPenerimaan = collect(DB::select($collection));
        $allDusuns = $vCollPenerimaan;
        $allDusuns->all();
        //$penduduk->appends(request()->input())->links();
        return view('kasi-pemerintahan.rekap-penduduk',compact('allDusuns'));
    }

    public function exportRekapPenduduk() {
        return Excel::download(new RekapPendudukExport, 'Rekap Penduduk.xlsx');
    }

    public function buku_penduduk_sementara(Request $request) {
        $penduduks = Penduduk::with(['kotaTujuan','kecamatanTujuan','kelurahanTujuan'])->where('status_penduduk_id',2)->latest()->paginate(20);
        return view('kasi-pemerintahan.buku-penduduk-sementara', compact('penduduks'));
    }

    public function buku_kk_nik(Request $request) {
        $penduduks = Penduduk::latest()->paginate(20);
        return view('kasi-pemerintahan.buku-ktp-kk', compact('penduduks'));
    }
}
