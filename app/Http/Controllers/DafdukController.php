<?php

namespace App\Http\Controllers;

use App\Models\DafdukDetail;
use App\Models\DafdukHeader;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\District;
use App\Models\Negara;
use App\Models\Province;
use App\Models\Subdistrict;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DafdukController extends Controller
{
    //
    public function index(Request $request)
    {
        # code...
        $dafduk = DafdukHeader::paginate(10);
        if ($request->cari) {
            $dafduk = DafdukHeader::where('nama_program','like',"%{$request->cari}%")
                            ->orWhere('keterangan','like',"%{$request->cari}%")
                            ->paginate(10);
        }

        $dafduk->appends($request->only('cari'));
        return view('F1.dafduk.index', compact('dafduk'));

    }

    public function create()
    {
        # code...
        DafdukHeader::whereNull('trx')->delete();
        $dafduk = new DafdukHeader();
        $dafduk->create();
        $id_dafduk = DafdukHeader::latest()->first()->id;
        return view('F1.dafduk.create', compact('id_dafduk'));
    }

    public function edit($id)
    {
        # code...
        $dafduk = DafdukHeader::find($id);
        $dafdukDetails = DafdukDetail::with(['pendidikan','pekerjaan'])->whereIdHdr($id)->get();
        $id_dafduk = $dafduk->id;
        $provincies = Province::all();
        $cities = City::whereProvId($dafduk->kd_profinsi)->get();
        $districts = District::whereCityId($dafduk->kd_kabupaten)->get();
        $subDistricts = Subdistrict::whereDisId($dafduk->kd_kelurahan)->get();
        $negaras = Negara::all();
        return view('F1.dafduk.edit', compact('id_dafduk','dafduk','dafdukDetails','provincies','cities','districts','subDistricts','negaras'));
    }

    public function store()
    {
        # code...
    }

    public function update(Request $request)
    {
        $dataAll = $request->all();
        $dataAll += [
            'trx' => "".getTrx()."",
        ];
        try{
            DB::beginTransaction();
            DafdukHeader::find($request->id)->update($dataAll);
            DB::commit();
            return response()->json(['status'=>true, "message"=>"Silahkan menambahkan data anggota"]);
        }catch(\Exception $ex) {
            Log::debug($ex->getMessage());
            DB::rollback();
            return response()->json(['status'=>false, "message"=>$ex->getMessage()]);
        }
    }

    public function storeDetail(Request $request)
    {
        # code...
        if($request['jenis-penduduk'] == 1){
            $validate = Validator::make($request->all(), [
                'nama_lenkap'        => 'required',
                'gelar_dpn'        => 'required',
                'gelar_blkg'     => 'required',
                'nmr_paspor'       => 'required',
                'tgl_expired_paspor'   => 'nullable',
                'jns_kelamin'      => 'required',
                'tempat_lahir'       => 'required',
                'tgl_lahir'      => 'nullable',
                'kewarganegaraan'    => 'required',
                // 'akta_lahir'         => 'nullable',
                //'nomor_akta_lahir'      => 'nullable',
                'gol_darah'          => 'required',
                'agama'              => 'required',
                'nama_organisasi_yme'=> 'required',
                'status_perkawinan'  => 'nullable',
                // 'akta_perkawinan'    => 'nullable',
                // 'nomor_akta_perkawinan' => 'nullable',
                // 'tgl_perkawinan' => 'nullable',
                // 'akta_cerai' => 'nullable',
                // 'nomor_akta_cerai' => 'nullable',
                // 'tgl_cerai' => 'nullable',
                'status_keluarga' => 'required',
                // 'kelainan_fisik_mental' => 'nullable',
                // 'penyandang_cacat' => 'nullable',
                'pendidikan_id' => 'required',
                'pekerjaan_id' => 'required',
                'nik_ibu' => 'required',
                'nama_ibu' => 'required',
                'nik_ayah' => 'required',
                'nama_ayah' => 'required',
            ]);
        }elseif($request['jenis-penduduk' == 2]){
            $validate = Validator::make($request->all(), [
                'nama_lenkap'        => 'required',
                'gelar-depan'        => 'required',
                'gelar-belakang'     => 'required',
                'nomor-paspor'       => 'required',
                'tanggal-passport'   => 'required',
                'jenis_kelamin'      => 'required',
                'tempat_lahir'       => 'required',
                'tanggal_lahir'      => 'required',
                'kewarganegaraan'    => 'required',
                'akta-lahir'         => 'required',
                'no-akta-lahir'      => 'required',
                'gol-darah'          => 'required',
                'agama'              => 'required',
                'nama_organisasi_yme'=> 'required',
                'status_perkawinan'  => 'required',
                'akta-perkawinan'    => 'required',
                'nomor_akta_perkawinan' => 'required',
                'tanggal_perkawinan' => 'required',
                'akta-cerai' => 'required',
                'nomor_akta_cerai' => 'required',
                'tanggal_cerai' => 'required',
                'status_keluarga' => 'required',
                'kelainan_fisik_mental' => 'required',
                'penyandang_cacat' => 'required',
                'pendidikan_id' => 'required',
                'pekerjaan_id' => 'required',
                'nik_ibu' => 'required',
                'nama_ibu' => 'required',
                'nik_ayah' => 'required',
                'nama_ayah' => 'required',
               'nama-sponsor' => 'required',
                'tipe-sponsor' => 'required',
                'alamat-sponsor' => 'required',
                'nomor_itas_itap' => 'required',
                'tempat_terbit_itas_itap' => 'required',
                'tgl_itas_itap' => 'required',
                'tgl_itas_itap_expired' => 'required',
                'tempat_datang_pertama' => 'required',
                'tgl_kedatangan_pertama' => 'required',
            ]);
        }else{
            $validate = Validator::make($request->all(), [
                'nama_lenkap'        => 'required',
                'gelar-depan'        => 'required',
                'gelar-belakang'     => 'required',
                'nomor-paspor'       => 'required',
                'tanggal-passport'   => 'required',
                'jenis_kelamin'      => 'required',
                'tempat_lahir'       => 'required',
                'tanggal_lahir'      => 'required',
                'kewarganegaraan'    => 'required',
                'akta-lahir'         => 'required',
                'no-akta-lahir'      => 'required',
                'gol-darah'          => 'required',
                'agama'              => 'required',
                'nama_organisasi_yme'=> 'required',
                'status_perkawinan'  => 'required',
                'akta-perkawinan'    => 'required',
                'nomor_akta_perkawinan' => 'required',
                'tanggal_perkawinan' => 'required',
                'akta-cerai' => 'required',
                'nomor_akta_cerai' => 'required',
                'tanggal_cerai' => 'required',
                'status_keluarga' => 'required',
                'kelainan_fisik_mental' => 'required',
                'penyandang_cacat' => 'required',
                'pendidikan_id' => 'required',
                'pekerjaan_id' => 'required',
                'nik_ibu' => 'required',
                'nama_ibu' => 'required',
                'nik_ayah' => 'required',
                'nama_ayah' => 'required',
               'nama-sponsor' => 'required',
                'tipe-sponsor' => 'required',
                'alamat-sponsor' => 'required',
                'nomor_itas_itap' => 'required',
                'tempat_terbit_itas_itap' => 'required',
                'tgl_itas_itap' => 'required',
                'tgl_itas_itap_expired' => 'required',
                'tempat_datang_pertama' => 'required',
                'tgl_kedatangan_pertama' => 'required',
                'nomor-sk'=>'required'
            ]);
        }
        if ($validate->fails()) {
            DafdukDetail::whereIdHdr($request->id_dafduk)->delete();
            return redirect()->route('buat-dafduk-edit',[$request->id_dafduk])->with('erros',$validate->errors());
        }
        $data = $request->all();
        $data += [
            'id_hdr' => $request->id_dafduk
        ];

        DafdukDetail::create($data);
        return redirect()->route('buat-dafduk-edit',[$request->id_dafduk])->with('success',"Detail Anggota berhasil ditambahkan");
    }

    public function deleted()
    {
        # code...
    }

    public function preview_dafduk($id)
    {
        $dafduk = DafdukHeader::find($id);
        $dafdukDetails = DafdukDetail::with(['pendidikan','pekerjaan'])->whereIdHdr($id)->get();
        return view('F1.dafduk.preview',compact('dafduk','dafdukDetails'));
    }
}
