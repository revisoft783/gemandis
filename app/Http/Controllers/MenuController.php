<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Submenu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menu.index', ['menu' => Menu::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.create');
    }

    public function create_submenu()
    {
        return view('menu.submenu.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->validate([
            'nama' => ['required','string','max:50']
        ]);

        $data['show_droid'] = $request->show_droid == "on" ? 1:0;

        $menu = Menu::create($data);
        return redirect()->route('menu.index',$menu)->with('success','Menu berhasil ditambahkan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_submenu(Request $request)
    {
        //
        $data = $request->validate([
            'nama' => ['required','string','max:50'],
            'url' => ['required','string'],
            'icon'=> ['required','string'],
            'warna'=> ['required','string'],
            'menu_id'=> ['required'],
        ]);

        Submenu::create($data);
        return redirect()->route('menu.edit',$request->menu_id)->with('success','Sub Menu berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        return view('menu.edit', compact('menu'));
    }

    public function show_submenu(Submenu $submenu)
    {
        return view('menu.edit', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        return view('menu.edit', compact('menu'));
    }

    public function edit_submenu($id)
    {
        $data = Submenu::findOrFail($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_submenu(Request $request)
    {
        //
        $data = $request->validate([
            'nama' => ['required','string','max:50'],
            'url' => ['required','string'],
            'icon'=> ['required','string'],
            'warna'=> ['required','string'],
            'menu_id'=> ['required'],
        ]);

        Submenu::find($request->submenu_id)->update($data);
        return redirect()->route('menu.edit',$request->menu_id)->with('success','Sub Menu berhasil diubah');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $data = $request->validate([
            'nama' => ['required','string','max:50'],
            'url'  => ['required','string','min:1']
        ]);

        $menu = Menu::find($request->id)->update($data);
        return redirect()->route('menu.index',$menu)->with('success','Menu berhasil ditambahkan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        //
        $menu->delete();
        return back()->with('success', 'Menu ' . $menu->nama . ' berhasil dihapus');
    }

    public function destroy_submenu(Submenu $submenu)
    {
        //
        $submenu->delete();
        return back()->with('success', 'Submenu ' . $submenu->nama . ' berhasil dihapus');
    }
}
