<?php

namespace App\Http\Controllers;

use App\Agama;
use App\AkseptorKb;
use App\Asuransi;
use App\Darah;
use App\Desa;
use App\Dusun;
use App\Exports\PendudukExport;
use App\Http\Requests\PendudukRequest;
use App\Imports\PendudukImport;
use App\Imports\PendudukOpenSIDImport;
use App\Imports\PendudukProdeskelImport;
use App\JenisCacat;
use App\JenisKelahiran;
use App\Models\KtpReader;
use App\Pekerjaan;
use App\Pendidikan;
use App\Penduduk;
use App\PenolongKelahiran;
use App\SakitMenahun;
use App\StatusHubunganDalamKeluarga;
use App\StatusPenduduk;
use App\StatusPerkawinan;
use App\StatusRekam;
use App\TempatDilahirkan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class PendudukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[4,5])->latest()->paginate(10);

        if ($request->cari) {
            if ($request->cari == "Laki-laki") {
                $penduduk = Penduduk::where('jenis_kelamin',1)->latest()->paginate(10);
            } elseif ($request->cari == "Perempuan") {
                $penduduk = Penduduk::where('jenis_kelamin',2)->latest()->paginate(10);
            } elseif ($request->cari == "WNI") {
                $penduduk = Penduduk::where('kewarganegaraan',1)->latest()->paginate(10);
            } elseif ($request->cari == "WNA") {
                $penduduk = Penduduk::where('kewarganegaraan',2)->latest()->paginate(10);
            } elseif ($request->cari == "Dua Kewarganegaraan") {
                $penduduk = Penduduk::where('kewarganegaraan',3)->latest()->paginate(10);
            } else {
                $penduduk = Penduduk::where(function ($penduduk) use ($request) {
                    $penduduk->where('nik', 'like', "%$request->cari%");
                    $penduduk->orWhere('kk', 'like', "%$request->cari%");
                    $penduduk->orWhere('nama', 'like', "%$request->cari%");
                    $penduduk->orWhere('tempat_lahir', 'like', "%$request->cari%");
                    $penduduk->orWhere('tanggal_lahir', 'like', "%$request->cari%");
                    $penduduk->orWhere('nomor_paspor', 'like', "%$request->cari%");
                    $penduduk->orWhere('nomor_kitas_atau_kitap', 'like', "%$request->cari%");
                    $penduduk->orWhere('nik_ayah', 'like', "%$request->cari%");
                    $penduduk->orWhere('nama_ayah', 'like', "%$request->cari%");
                    $penduduk->orWhere('nik_ibu', 'like', "%$request->cari%");
                    $penduduk->orWhere('nama_ibu', 'like', "%$request->cari%");
                    $penduduk->orWhere('alamat_sekarang', 'like', "%$request->cari%");
                    $penduduk->orWhereHas('statusHubunganDalamKeluarga', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('pendidikan', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('pekerjaan', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('agama', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('darah', function ($status) use ($request) {
                        $status->where('golongan', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('statusPerkawinan', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhere('tag_card_id', "%$request->cari%");
                })->whereNotIn('status_penduduk_id',[4,5])->latest()->paginate(10);
            }
        }

        $penduduk->appends(request()->input())->links();
        return view('penduduk.index', compact('penduduk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penduduk.create', $this->parsing());
    }

    private function parsing($penduduk = [])
    {
        return [
            'agama'                         => Agama::all(),
            'darah'                         => Darah::all(),
            'dusun'                         => Dusun::all(),
            'pekerjaan'                     => Pekerjaan::all(),
            'pendidikan'                    => Pendidikan::all(),
            'pendidikan'                    => Pendidikan::all(),
            'status_hubungan_dalam_keluarga'=> StatusHubunganDalamKeluarga::all(),
            'status_perkawinan'             => StatusPerkawinan::all(),
            'akseptor_kb'                   => AkseptorKb::all(),
            'asuransi'                      => Asuransi::all(),
            'jenis_cacat'                   => JenisCacat::all(),
            'jenis_kelahiran'               => JenisKelahiran::all(),
            'penolong_kelahiran'            => PenolongKelahiran::all(),
            'sakit_menahun'                 => SakitMenahun::all(),
            'status_penduduk'               => StatusPenduduk::all(),
            'status_rekam'                  => StatusRekam::all(),
            'tempat_dilahirkan'             => TempatDilahirkan::all(),
            'penduduk'                      => $penduduk,
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PendudukRequest $request)
    {
        $data = $request->validated();
        if ($request->foto) {
            $data['foto'] = $request->foto->store('public/gallery');
        }
        Penduduk::create($data);
        return redirect()->route('penduduk.index')->with('success','Penduduk berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function show($nik)
    {
        $penduduk = Penduduk::where('nik',$nik)->orWhere('tag_card_id',$nik)->firstOrFail();
        return view('penduduk.show', compact('penduduk'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function detail(Penduduk $penduduk)
    {
        if ($penduduk) {
            return response()->json([
                'success'   => true,
                'data'      => $penduduk
            ]);
        } else {
            return response()->json([
                'success'   => false
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function edit(Penduduk $penduduk)
    {
        return view('penduduk.edit', $this->parsing($penduduk));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function update(PendudukRequest $request, Penduduk $penduduk)
    {
        $data = $request->validated();
        if ($request->foto) {
            if ($penduduk->foto) {
                File::delete(storage_path('app/' . $penduduk->foto));
            }
            $data['foto'] = $request->foto->store('public/gallery');
        }

        $penduduk->update($data);
        return redirect()->back()->with('success','Penduduk berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penduduk $penduduk)
    {
        if ($penduduk->foto) {
            File::delete(storage_path('app/' . $penduduk->foto));
        }
        $penduduk->delete();
        return redirect()->back()->with('success','Penduduk berhasil dihapus');
    }

    public function destroys(Request $request)
    {
        foreach ($request->id as $value) {
            $penduduk = Penduduk::find($value);
            if ($penduduk->foto) {
                File::delete(storage_path('app/' . $penduduk->foto));
            }
            $penduduk->delete();
        }

        return response()->json([
            'message' => 'Penduduk berhasil dihapus'
        ]);
    }

    public function export()
    {
        return Excel::download(new PendudukExport, 'penduduk.xlsx');
    }

    public function import(Request $request)
    {
        $request->validate([
            'file_excel_penduduk' => ['required','file','max:2048']
        ]);

        Excel::import(new PendudukImport, $request->file('file_excel_penduduk'));
        return back();
    }

    public function import_prodeskel(Request $request)
    {
        $request->validate([
            'file_excel_penduduk_prodeskel' => ['required','file','max:2048']
        ]);

        Excel::import(new PendudukProdeskelImport, $request->file('file_excel_penduduk_prodeskel'));
        return back();
    }

    public function import_opensid(Request $request)
    {
        $request->validate([
            'file_excel_penduduk_opensid' => ['required','file','max:2048']
        ]);

        Excel::import(new PendudukOpenSIDImport, $request->file('file_excel_penduduk_opensid'));
        return back()->with('success', 'Data penduduk dari OPENSID berhasil diimport');
    }

    public function printAll()
    {
        $desa = Desa::find(1);
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[4,5])->latest()->get()->groupBy('kk');
        return view('penduduk.print', compact('penduduk','desa'));
    }

    public function printAllKeluarga()
    {
        $desa = Desa::find(1);
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[4,5])->latest()->whereHas('statusHubunganDalamKeluarga', function ($status) {$status->where('nama','Kepala Keluarga');})->get();
        return view('penduduk.keluarga.print-all', compact('penduduk','desa'));
    }

    public function printKeluarga($kk)
    {
        $index=1;
        $desa = Desa::find(1);
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[4,5])->where('kk', $kk)->orderBy('nik')->get();
        return view('penduduk.keluarga.print', compact('penduduk','desa','index'));
    }

    public function printCalonPemilih(Request $request)
    {
        $dusun = $request->dusun;
        $rw = $request->rw;
        $rt = $request->rt;
        $desa = Desa::find(1);
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[4,5])->latest()->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)));
		$penduduk->WhereHas('detailDusun', function ($detail) use ($request) {
			$detail->whereHas('dusun', function ($dusun) use ($request) {
				if ($request->dusun) {
					$dusun->where('nama', 'like', "%$request->dusun%");
				}
				if ($request->rw) {
					$dusun->where('rw', 'like', "%$request->rw%");
				}
				if ($request->rt) {
					$dusun->where('rt', 'like', "%$request->rt%");
				}
			});
		});

		$penduduk = $penduduk->whereIn('status_perkawinan_id',array([2,3,4]))->get();
        return view('penduduk.calon-pemilih.print', compact('penduduk','desa','dusun','rw','rt'));
    }

    public function keluarga(Request $request)
    {
        $penduduk = Penduduk::latest()->whereHas('statusHubunganDalamKeluarga', function ($status) {$status->where('nama','Kepala Keluarga');})->paginate(10);

        if ($request->cari) {
            if ($request->cari == "Laki-laki") {
                $penduduk = Penduduk::where('jenis_kelamin',1)->whereHas('statusHubunganDalamKeluarga', function ($status) {$status->where('nama','Kepala Keluarga');})->latest()->paginate(10);
            } elseif ($request->cari == "Perempuan") {
                $penduduk = Penduduk::where('jenis_kelamin',2)->whereHas('statusHubunganDalamKeluarga', function ($status) {$status->where('nama','Kepala Keluarga');})->latest()->paginate(10);
            } else {
                $penduduk = Penduduk::where(function ($penduduk) use ($request) {
                    $penduduk->where('nik', 'like', "%$request->cari%");
                    $penduduk->orWhere('kk', 'like', "%$request->cari%");
                    $penduduk->orWhere('nama', 'like', "%$request->cari%");
                    $penduduk->orWhere('alamat_sekarang', 'like', "%$request->cari%");
                    $penduduk->orWhereHas('detailDusun', function ($detail) use ($request) {
                        $detail->whereHas('dusun', function ($dusun) use ($request) {
                            $dusun->where('nama', 'like', "%$request->cari%");
                        });
                        $detail->orWhere('rt', 'like', "%$request->cari%");
                        $detail->orWhere('rw', 'like', "%$request->cari%");
                    });
                })->whereHas('statusHubunganDalamKeluarga', function ($status) {$status->where('nama','Kepala Keluarga');})->latest()->paginate(10);
            }
        }

        $penduduk->appends(request()->input())->links();
        return view('penduduk.keluarga.index', compact('penduduk'));
    }

    public function detailKeluarga($kk)
    {
        $penduduk = Penduduk::where('kk', $kk)->get();
        return view('penduduk.keluarga.show', compact('penduduk'));
    }

    public function calonPemilih(Request $request)
    {
        if (!$request->tanggal) {
           return redirect('/calon-pemilih?tanggal='.date('d-m-Y'));
        }
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[4,5])->latest()->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))
        ->whereIn('status_perkawinan_id',array([2,3,4]))->paginate(20);

        if ($request->cari) {
            if ($request->cari == "Laki-laki") {
                $penduduk = Penduduk::where('jenis_kelamin',1)->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
            } elseif ($request->cari == "Perempuan") {
                $penduduk = Penduduk::where('jenis_kelamin',2)->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
            } else {
                $penduduk = Penduduk::where(function ($penduduk) use ($request) {
                    $penduduk->where('nik', 'like', "%$request->cari%");
                    $penduduk->orWhere('kk', 'like', "%$request->cari%");
                    $penduduk->orWhere('nama', 'like', "%$request->cari%");
                    $penduduk->orWhere('alamat_sekarang', 'like', "%$request->cari%");
                    $penduduk->orWhereHas('detailDusun', function ($detail) use ($request) {
                        $detail->whereHas('dusun', function ($dusun) use ($request) {
                            $dusun->where('nama', 'like', "%$request->cari%");
                        });
                        $detail->orWhere('rt', 'like', "%$request->cari%");
                        $detail->orWhere('rw', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('pendidikan', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('pekerjaan', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                })->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
            }
        }else{
			//return $penduduk->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->paginate(20);
		}

        $penduduk->appends(request()->input())->links();
        return view('penduduk.calon-pemilih.index', compact('penduduk'));
    }

    public function cari(Request $request)
    {
        $nik = $request->nik;
        // $nik  = explode("=",explode("?", $uri)[1])[1];
        $status = $request->status;
        $desa = Desa::find(1);
        $penduduk = Penduduk::where('nik', $nik)->orWhere('tag_card_id',$nik);
        if($status == 1) {
            $penduduk=  $penduduk->whereNotIn('status_penduduk_id',[4,5]);
        }
        $penduduk = $penduduk->first();
        if (!$penduduk) {
            return response()->json(['status'=>false,'message'=>'Penduduk tidak ditemukan']);
        }

        $data = $penduduk->toArray();
        $data['nama']                           = $penduduk->nama ?? null;
        $data['nik']                            = $penduduk->nik ?? null;
        $data['nama_ayah']                      = $penduduk->nama_ayah ?? null;
        $data['nik_ayah']                       = $penduduk->nik_ayah ?? null;
        $data['nama_ibu']                       = $penduduk->nama_ibu ?? null;
        $data['nik_ibu']                        = $penduduk->nik_ibu ?? null;
        $data['agama']                          = $penduduk->agama->nama ?? null;
        $data['akseptor_kb']                    = $penduduk->akseptorKb->nama ?? null;
        $data['alamat']                         = $penduduk->alamat_sekarang;
        $data['asuransi']                       = $penduduk->asuransi->nama ?? null;
        $data['darah']                          = $penduduk->darah->nama ?? null;
        $data['rt']                             = $penduduk->detail_dusun_id ? $penduduk->detailDusun->rt .'/'.$penduduk->detailDusun->rt : '';
        $data['rw']                             = $penduduk->detail_dusun_id ? $penduduk->detailDusun->rt .'/'.$penduduk->detailDusun->rw : '';
        $data['dusun']                          = $penduduk->detail_dusun_id ? $penduduk->detailDusun->rt .'/'.$penduduk->detailDusun->dusun : '';
        $data['jenis_cacat']                    = $penduduk->jenisCacat->nama ?? null;
        $data['jenis_kelahiran']                = $penduduk->jenisKelahiran->nama ?? null;
        $data['jenis_kelamin']                  = $penduduk->jenis_kelamin == 1 ? "Laki-laki" : "Perempuan";
        $data['kewarganegaraan']                = $penduduk->kewarganegaraan == 1 ? "WNI" : ($penduduk->kewarganegaraan == 2 ? $penduduk->kewarganegaraan = "WNA" : $penduduk->kewarganegaraan = "Dua Kewarganagaraan");
        $data['pekerjaan']                      = $penduduk->pekerjaan->nama ?? null;
        $data['pendidikan']                     = $penduduk->pendidikan->nama ?? null;
        $data['penolong_kelahiran']             = $penduduk->penolongKelahiran->nama ?? null;
        $data['sakit_menahun']                  = $penduduk->sakitMenahun->nama ?? null;
        $data['status_hubungan_dalam_keluarga'] = $penduduk->statusHubunganDalamKeluarga->nama ?? null;
        $data['status_penduduk']                = $penduduk->statusPenduduk->nama ?? null;
        $data['status_perkawinan']              = $penduduk->statusPerkawinan->nama ?? null;
        $data['status_rekam']                   = $penduduk->statusRekam->nama ?? null;
        $data['tanggal_berakhir_paspor']        = date('d-m-Y', strtotime($penduduk->tgl_berakhir_paspor));
        $data['tanggal_perceraian']             = date('d-m-Y', strtotime($penduduk->tanggal_perceraian));
        $data['tanggal_perkawinan']             = date('d-m-Y', strtotime($penduduk->tanggal_perkawinan));
        $data['tempat,_tanggal_lahir']          = $penduduk->tempat_lahir .', '. date('d-m-Y', strtotime($penduduk->tanggal_lahir));
        $data['tempat_lahir']                   = $penduduk->tempat_lahir;
        $data['tanggal_lahir']                  = date('d-m-Y', strtotime($penduduk->tanggal_lahir));
        $data['tempat_dilahirkan']              = $penduduk->tempatDilahirkan->nama ?? null;
        $data['umur']                           = Carbon::parse($penduduk->tanggal_lahir)->diff(Carbon::now())->format('%y Tahun');
        /**
         * @since 03-10-2021
         * @version bug/2110032145-add-alamat-with-rt-rw
         * @author Afes Oktavianus
         */
        $dusun      = $penduduk->detail_dusun_id ? $penduduk->detailDusun->dusun->nama : '';
        $rt         = $penduduk->detail_dusun_id ? $penduduk->detailDusun->rt : '';
        $rw         = $penduduk->detail_dusun_id ? $penduduk->detailDusun->rw : '';
        // $data['alamat,_rt_rw_kec_kab']          = $dusun.', RT/RW : '. $rt.'/'.$rw.', Kec. '.$desa->nama_kecamatan.', Kab. '.$desa->nama_kabupaten;
        $data['alamat,_rt_rw_kec_kab']          = 'RT : '. $rt.' RW : '.$rw.' '.$dusun.', Kec. '.$desa->nama_kecamatan.', Kab. '.$desa->nama_kabupaten;
        $data['status'] = true;
        unset(
            $data['akseptor_kb_id'],
            $data['alamat_sekarang'],
            $data['agama_id'],
            $data['asuransi_id'],
            $data['created_at'],
            $data['darah_id'],
            $data['detail_dusun_id'],
            $data['foto'],
            $data['id'],
            $data['jenis_cacat_id'],
            $data['jenis_kelahiran_id'],
            $data['pekerjaan_id'],
            $data['pendidikan_id'],
            $data['penolong_kelahiran_id'],
            $data['sakit_menahun_id'],
            $data['status_hubungan_dalam_keluarga_id'],
            $data['status_penduduk_id'],
            $data['status_perkawinan_id'],
            $data['status_rekam_id'],
            $data['tempat_dilahirkan_id'],
            $data['tgl_berakhir_paspor'],
            $data['updated_at']
        );

        return response()->json($data);
    }

    public function akseptor_kb($sex)
    {
        $akseptor_kb = AkseptorKb::where('sex', $sex)->get();
        return response()->json($akseptor_kb->toArray());
    }

    public function changeStatus()
    {
        return view('penduduk.change_status', $this->parsing());
    }

    public function meninggal(Request $request)
    {
        if (!$request->tanggal) {
            return redirect('/meninggal-status?tanggal='.date('d-m-Y'));
         }
         $penduduk = Penduduk::latest()->with(['users_updated'])->whereStatusPendudukId(4)->paginate(20);

         if ($request->cari) {
             if ($request->cari == "Laki-laki") {
                 $penduduk = Penduduk::where('jenis_kelamin',1)->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
             } elseif ($request->cari == "Perempuan") {
                 $penduduk = Penduduk::where('jenis_kelamin',2)->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
             } else {
                 $penduduk = Penduduk::where(function ($penduduk) use ($request) {
                     $penduduk->where('nik', 'like', "%$request->cari%");
                     $penduduk->orWhere('kk', 'like', "%$request->cari%");
                     $penduduk->orWhere('nama', 'like', "%$request->cari%");
                     $penduduk->orWhere('alamat_sekarang', 'like', "%$request->cari%");
                     $penduduk->orWhereHas('detailDusun', function ($detail) use ($request) {
                         $detail->whereHas('dusun', function ($dusun) use ($request) {
                             $dusun->where('nama', 'like', "%$request->cari%");
                         });
                         $detail->orWhere('rt', 'like', "%$request->cari%");
                         $detail->orWhere('rw', 'like', "%$request->cari%");
                     });
                     $penduduk->orWhereHas('pendidikan', function ($status) use ($request) {
                         $status->where('nama', 'like', "%$request->cari%");
                     });
                     $penduduk->orWhereHas('pekerjaan', function ($status) use ($request) {
                         $status->where('nama', 'like', "%$request->cari%");
                     });
                 })->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
             }
         }else{
             //return $penduduk->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->paginate(20);
         }

         $penduduk->appends(request()->input())->links();
        return view('penduduk.meninggal-status.index', compact('penduduk'));
    }

    public function mutasiPenduduk(Request $request)
    {
        if (!$request->tanggal) {
            return redirect('/mutasi-status?tanggal='.date('d-m-Y'));
        }
        $penduduk = Penduduk::with(['users_updated'])->latest()->whereStatusPendudukId(5)->paginate(20);

        if ($request->cari) {
            if ($request->cari == "Laki-laki") {
                $penduduk = Penduduk::where('jenis_kelamin',1)->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
            } elseif ($request->cari == "Perempuan") {
                $penduduk = Penduduk::where('jenis_kelamin',2)->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
            } else {
                $penduduk = Penduduk::where(function ($penduduk) use ($request) {
                    $penduduk->where('nik', 'like', "%$request->cari%");
                    $penduduk->orWhere('kk', 'like', "%$request->cari%");
                    $penduduk->orWhere('nama', 'like', "%$request->cari%");
                    $penduduk->orWhere('alamat_sekarang', 'like', "%$request->cari%");
                    $penduduk->orWhereHas('detailDusun', function ($detail) use ($request) {
                        $detail->whereHas('dusun', function ($dusun) use ($request) {
                            $dusun->where('nama', 'like', "%$request->cari%");
                        });
                        $detail->orWhere('rt', 'like', "%$request->cari%");
                        $detail->orWhere('rw', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('pendidikan', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                    $penduduk->orWhereHas('pekerjaan', function ($status) use ($request) {
                        $status->where('nama', 'like', "%$request->cari%");
                    });
                })->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->latest()->paginate(20);
            }
        }else{
            //return $penduduk->where('tanggal_lahir','<', (date('Y', strtotime($request->tanggal)) - 17) . '-' . date('m-d', strtotime($request->tanggal)))->paginate(20);
        }

         $penduduk->appends(request()->input())->links();
        return view('penduduk.pindah-status.index', compact('penduduk'));
    }

    public function update_mutasi(Request $request)
    {
        try {
            $berkas_file_kk=$berkas_file="";
            $validator = Validator::make($request->all(), [
                'nomor_induk_penduduk'      => ['required','string'],
                'tgl_pindah'                => ['required'],
                'kota_tujuan'               => ['required'],
                'kecamatan_tujuan'          => ['required'],
                'kelurahan_tujuan'          => ['required'],
                'alamat_tujuan'             => ['required'],
                'berkas_file'                   => ['required','file','max:2048'],
                'berkas_file_kk'                => ['required','file','max:2048'],
            ],[
                'tgl_pindah.required' => 'tanggal wajib diisi',
                'kota_tujuan.required' => 'kota wajib diisi',
                'kecamatan_tujuan.required' => 'kecamatan wajib diisi',
                'kelurahan_tujuan.required' => 'kelurahan wajib diisi',
                'alamat_tujuan.required' => 'alamat wajib diisi',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->with('error',$validator->errors()->all());
            }
            if ($request->berkas_file) {
                $berkas_file = $request->berkas_file->storeAs('public/berkas-penduduk', $request->berkas_file->getClientOriginalName());
            }
            if ($request->berkas_file_kk) {
                $berkas_file_ktp = $request->berkas_file_kk->storeAs('public/berkas-penduduk', $request->berkas_file_kk->getClientOriginalName());
            }
            if($request->pilihan_keluarga == "0"){
                $penduduks = Penduduk::where("kk",$request->nomor_induk_penduduk)->get();
                foreach ($penduduks as $key => $value) {
                    $penduduk = Penduduk::whereNik("$value->nik")->firstOrFail();
                    # code...
                    $penduduk->update([
                        'tgl_pindah'        => $request->tgl_pindah,
                        'kota_tujuan'       => $request->kota_tujuan,
                        'kecamatan_tujuan'  => $request->kecamatan_tujuan,
                        'kelurahan_tujuan'  => $request->kelurahan_tujuan,
                        'alamat_tujuan'     => $request->alamat_tujuan,
                        'status_penduduk_id'=> 5,
                        'updated_by'        => auth()->user()->id,
                        'tgl_mutasi'        => date('Y-m-d'),
                        'berkas_file'       => $berkas_file,
                        'berkas_file_kk'    => $berkas_file_ktp,
                    ]);
                }
            }else{
                $penduduk = Penduduk::where("nik",$request->nomor_induk_penduduk)->firstOrFail();
                $penduduk->update([
                    'tgl_pindah'        => $request->tgl_pindah,
                    'kota_tujuan'       => $request->kota_tujuan,
                    'kecamatan_tujuan'  => $request->kecamatan_tujuan,
                    'kelurahan_tujuan'  => $request->kelurahan_tujuan,
                    'alamat_tujuan'     => $request->alamat_tujuan,
                    'status_penduduk_id'=> 5,
                    'updated_by'        => auth()->user()->id,
                    'tgl_mutasi'        => date('Y-m-d'),
                    'berkas_file'       => $berkas_file,
                    'berkas_file_kk'    => $berkas_file_ktp,
                ]);
            }


            // return response()->json(['status'=>true, 'message'=>'Penduduk berhasil dimutasi meninggal']);
           return redirect()->back()->with('success','Penduduk berhasil diperbarui');
        } catch (\Throwable $th) {
            throw $th;
        }
        return redirect()->back()->with('error','Penduduk tidak berhasil diperbarui');
    }

    public function update_mutasi_meninggal(Request $request)
    {
        try {
            $berkas_file_ktp=$berkas_file_kk=$berkas_file="";
            $validator = Validator::make($request->all(), [
                'nomor_induk_penduduk'          => ['required','string'],
                'tgl_meninggal'                 => ['required'],
                'jam_meninggal'                 => ['required'],
                'tempat_meninggal'              => ['required'],
                'penyebab_meninggal'            => ['required'],
                'berkas_file'                   => ['required','file','max:2048'],
                'berkas_file_kk'                => ['required','file','max:2048'],
                'berkas_file_ktp'               => ['required','file','max:2048'],
            ],[
                'tgl_meninggal.required' => 'tanggal wajib diisi',
                'jam_meninggal.required' => 'kota wajib diisi',
                'tempat_meninggal.required' => 'kecamatan wajib diisi',
                'penyebab_meninggal.required' => 'kelurahan wajib diisi',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success'   => false,
                    'message'   => $validator->errors()->all()
                ]);
            }
            if ($request->berkas_file) {
                $berkas_file = $request->berkas_file->storeAs('public/berkas-penduduk', $request->berkas_file->getClientOriginalName());
            }
            if ($request->berkas_file_kk) {
                $berkas_file_ktp = $request->berkas_file_kk->storeAs('public/berkas-penduduk', $request->berkas_file_kk->getClientOriginalName());
            }
            if ($request->berkas_file_ktp) {
                $berkas_file_kk = $request->berkas_file_ktp->storeAs('public/berkas-penduduk', $request->berkas_file_ktp->getClientOriginalName());
            }

            $penduduk = Penduduk::whereNik("$request->nomor_induk_penduduk")->firstOrFail();
            $penduduk->update([
                'tgl_meninggal'     => $request->tgl_meninggal,
                'jam_meninggal'     => $request->jam_meninggal,
                'tempat_meninggal'  => $request->tempat_meninggal,
                'penyebab_meninggal'=> $request->penyebab_meninggal,
                'status_penduduk_id'=> 4,
                'updated_by'        => auth()->user()->id,
                'tgl_mutasi'        => date('Y-m-d'),
                'berkas_file'       => $berkas_file,
                'berkas_file_ktp'   => $berkas_file_ktp,
                'berkas_file_kk'    => $berkas_file_kk,
            ]);
            // return response()->json(['status'=>true, 'message'=>'Penduduk berhasil dimutasi meninggal']);
           return redirect()->back()->with('success','Penduduk berhasil diperbarui');
        } catch (\Throwable $th) {
            throw $th;
        }
        return redirect()->back()->with('error','Penduduk tidak berhasil diperbarui');
    }

    public function print_all_mutasi(Request $request)
    {
        $dusun = $request->dusun;
        $rw = $request->rw;
        $rt = $request->rt;
        $desa = Desa::find(1);
        $penduduk = Penduduk::latest();
		$penduduk->WhereHas('detailDusun', function ($detail) use ($request) {
			$detail->whereHas('dusun', function ($dusun) use ($request) {
				if ($request->dusun) {
					$dusun->where('nama', 'like', "%$request->dusun%");
				}
				if ($request->rw) {
					$dusun->where('rw', 'like', "%$request->rw%");
				}
				if ($request->rt) {
					$dusun->where('rt', 'like', "%$request->rt%");
				}
			});
		});

		$penduduk = $penduduk->where('status_penduduk_id',5)->whereIn('status_perkawinan_id',array([2,3,4]))->get();
        return view('penduduk.pindah-status.print', compact('penduduk','desa','dusun','rw','rt'));
    }

    public function print_all_mutasi_meninggal(Request $request)
    {
        $dusun = $request->dusun;
        $rw = $request->rw;
        $rt = $request->rt;
        $desa = Desa::find(1);
        $penduduk = Penduduk::latest();
		$penduduk->WhereHas('detailDusun', function ($detail) use ($request) {
			$detail->whereHas('dusun', function ($dusun) use ($request) {
				if ($request->dusun) {
					$dusun->where('nama', 'like', "%$request->dusun%");
				}
				if ($request->rw) {
					$dusun->where('rw', 'like', "%$request->rw%");
				}
				if ($request->rt) {
					$dusun->where('rt', 'like', "%$request->rt%");
				}
			});
		});

		$penduduk = $penduduk->where('status_penduduk_id',4)->whereIn('status_perkawinan_id',array([2,3,4]))->get();
        return view('penduduk.meninggal-status.print', compact('penduduk','desa','dusun','rw','rt'));
    }

    public function cari_kk(Request $request)
    {
        $nik = $request->nik;
        // $nik  = explode("=",explode("?", $uri)[1])[1];
        $status = $request->status;
        $desa = Desa::find(1);
        $penduduk = Penduduk::with(['statusHubunganDalamKeluarga'])->where('kk', $nik);
        if($status == 1) {
            $penduduk=  $penduduk->whereNotIn('status_penduduk_id',[4,5]);
        }
        $penduduk = $penduduk->get();
        if (!$penduduk) {
            return null;
        }


        return response()->json($penduduk->toJson());
    }

    public function utility(Request $request)
    {
        $penduduk = KtpReader::latest()->paginate(10);
        if ($request->cari) {
            if ($request->cari == "WNI") {
                $penduduk = KtpReader::where('kwgn',$request->cari)->latest()->paginate(10);
            } elseif ($request->cari == "WNA") {
                $penduduk = KtpReader::where('kwgn',$request->cari)->latest()->paginate(10);
            } elseif ($request->cari == "Dua Kewarganegaraan") {
                $penduduk = KtpReader::where('kwgn',$request->cari)->latest()->paginate(10);
            } else {
                $penduduk = KtpReader::where(function ($penduduk) use ($request) {
                    $penduduk->where('nik', 'like', "%$request->cari%");
                    $penduduk->orWhere('nama', 'like', "%$request->cari%");
                    $penduduk->orWhere('ttl', 'like', "%$request->cari%");
                    $penduduk->orWhere('tgllhr', 'like', "%$request->cari%");
                    $penduduk->orWhere('tag_card_id', 'like', "%$request->cari%");
                })->latest()->paginate(10);
            }
        }
        return view('utility.index',compact('penduduk'));
    }

    public function showUtility(Request $request)
    {
        $penduduk = KtpReader::where('nik',$request->nik)->whereOr('tag_card_id',$request->tag_card_id)->firstOrFail();
        return view('utility.edit', compact('penduduk'));
    }

    public function update_ktp_reader(Request $request)
    {
        try {
            $berkas_file_ktp=$berkas_file_kk=$berkas_file="";
            $validator = Validator::make($request->all(), [
                'tag_card_id'          => ['required'],
            ],[
                'tag_card_id.required' => 'tag card wajib diisi',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success'   => false,
                    'message'   => $validator->errors()->all()
                ]);
            }

            $penduduk = KtpReader::whereNik("$request->nik")->firstOrFail();
            $penduduk->update([
                'tag_card_id'     => $request->tag_card_id
            ]);
            // return response()->json(['status'=>true, 'message'=>'Penduduk berhasil dimutasi meninggal']);
           return redirect(route('utility'))->with('success','Penduduk berhasil diperbarui');
        } catch (\Throwable $th) {
            throw $th;
        }
        return redirect()->back()->with('error','Penduduk tidak berhasil diperbarui');
    }

    public function synchronize(Request $request)
    {
        try {
            $ktpReader = KtpReader::whereNik("$request->nik")->firstOrFail();
            $penduduk = Penduduk::whereNik("$request->nik")->first();
            $data = [
                $penduduk->nik => $ktpReader->nik,
                $penduduk->nama=> $ktpReader->nama,
                $penduduk->tempat_lahir => $ktpReader->ttl,
                $penduduk->tanggal_lahir=> $ktpReader->tgllhr,
                $penduduk->tag_card_id = $ktpReader->tag_card_id
            ];
            $penduduk->updateOrCreate(['nik'=>$ktpReader->nik],$data);
            return redirect()->back()->with('success','Penduduk berhasil diperbarui');
        } catch (\Throwable $th) {
            throw $th;
        }
        return redirect()->back()->with('error','Penduduk tidak berhasil diperbarui');
    }
}
