<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\KtpReader;
use App\Penduduk;
use Illuminate\Http\Request;

class ManagePendudukController extends BaseController
{
    public function index()
    {
        $penduduk = Penduduk::whereNotIn('status_penduduk_id',[4,5])->paginate(15);
        return $this->sendResponse($penduduk,"Data Penduduk");
    }

    public function store(Request $request)
    {
       $data = $request->all();
       Penduduk::create($data);
       return $this->sendResponse('200 OK','Data Penduduk berhasil ditambahkan');
    }

    public function index_pindah()
    {
        $penduduk = Penduduk::where('status_penduduk_id',5)->paginate(15);
        return $this->sendResponse($penduduk, 'Data daftar penduduk Pindah');
    }

    public function index_meninggal()
    {
        $penduduk = Penduduk::where('status_penduduk_id',4)->paginate(15);
        return $this->sendResponse($penduduk, 'Data daftar penduduk Meninggal');
    }

    public function storeKtp(Request $request)
    {
        $data = [
            'nik' => $request->nik,
            'nama' => $request->nama,
            'ttl' => $request->tempat_lahir,
            'tgllhr' => $request->tanggal_lahir,
            'gdr' => $request->gender,
            'goldar' => $request->golongan_darah,
            'almt' => $request->alamat,
            'rt' => $request->rt,
            'rw' => $request->rw,
            'kec' => $request->kecamatan,
            'kel' => $request->kelurahan,
            'agm' => $request->agama,
            'mrtl' => $request->marital_statud,
            'pkj' => $request->pekerjaan,
            'prv' => $request->provinsi,
            'kab' => $request->kabupaten,
            'kwgn' => $request->kewarganegaraan,
       ];
       KtpReader::firstOrCreate($data);
       return $this->sendResponse('200 OK','Data Penduduk berhasil ditambahkan');
    }
}
