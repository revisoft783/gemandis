<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Mail\SendOTPEmail;
use App\Mail\SendVerifikasiEmail;
use App\PemerintahanDesa;
use App\Penduduk;
use App\Peran;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Seshac\Otp\Otp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ManageUserController extends BaseController
{

    /**
     * @author Afes Oktavianus
     * @since 27-01-2022
     * @todo Login user to use gemandis droid
     * @version feature/GEM-3-add-login-page
     */
    public function signin(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $authUser = Auth::user();
            $success['token'] =  $authUser->createToken('MyAuthApp')->plainTextToken;
            $success['name'] =  $authUser->nama;

            return $this->sendResponse($success, 'User signed in');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']);
        }
    }


    public function FindNIKPenduduk(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'nik' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Error validation', $validator->errors());
        }
        $penduduk = Penduduk::where('penduduk.nik',$request->nik)
                    ->first();
        $pemerintahanDesa = PemerintahanDesa::whereNik($request->nik)->select('jabatan')->first();
        $success['penduduk'] = $penduduk;
        $success['jabatan'] = $pemerintahanDesa;

        return $this->sendResponse($success,'Successfully get data');
    }

    /**
     * @author Afes Oktavianus
     * @since 24-01-2022
     * @todo register people before use gemandis droid
     * @version feature/GEM-3-add-login-page
     */
    public function RegisterPenduduk(Request $request)
    {
        try {
            DB::beginTransaction();
            $request->validate([
                'nik' => 'required|string',
                'email' => 'required|string|email|unique:users'
            ]);

            $penduduk = Penduduk::where('penduduk.nik',"$request->nik")
                        ->first();
            if ($penduduk == null) {
                return $this->sendResponse('failed','Failed created user! NIK tidak ditemukan');
            }
            $jabatan = Penduduk::join('pemerintahan_desa','penduduk.nik','=','pemerintahan_desa.nik')
                ->where('penduduk.nik',"$request->nik")->select('penduduk.*','pemerintahan_desa.jabatan AS nama_jab')
                ->first();

            $peran = 99;

            if ($jabatan != null) {
                $akses  = Peran::where(DB::raw('lower(nama)'),Str::lower($jabatan->nama_jab))->first();
            }
            if($akses != null) {
                $peran = $akses->id;
            }
            $identifier = Str::random(12);
            $otp = Otp::generate($identifier);
            if($peran == 99) {
                $user = new User([
                    'nama' => $penduduk->nama,
                    'email' => $request->email,
                    'password' => Hash::make("Gemandis123"),
                    'otp' => $otp->token
                ]);
            }else{
                $user = new User([
                    'nama' => $penduduk->nama,
                    'email' => $request->email,
                    'password' => Hash::make("Gemandis123"),
                    'peran_id' => $peran,
                    'otp' => $otp->token
                ]);
            }
            $user->save();
            $penduduk->update([
                'nomor_telepon' => $request->no_telp
            ]);

            Mail::to($request->email)->send(new SendOTPEmail($penduduk->nama,$otp->token));

            $success['token'] =  $user->createToken('MyAuthApp')->plainTextToken;
            $success['name'] =  $user->nama;
            $success['message'] = 'Cek email untuk verifikasi OTP';
            DB::commit();
            return $this->sendResponse($success, 'User created successfully.');
        }catch(Exception $ex) {
            DB::rollBack();
            return $this->sendError('Error', "User sudah terdaftar");
            throw $ex;
        }
    }

    /**
     * @author Afes Oktavianus
     * @since 27-01-2022
     * @todo verifikasi people before use gemandis droid
     * @version feature/GEM-3-add-login-page
     */
    public function Verifikasi(Request $request)
    {
        $request->validate([
            'otp' => 'required|string',
            'email' => 'required|string'
        ]);

        $user = User::where('email',$request->email)->where('otp',$request->otp)->where('actived',0)->first();

        if($user == null) {
            return $this->sendResponse('failed','User sudah pernah activasi!');
        }else{
            $user->update(['actived'=>1]);

            Mail::to($request->email)->send(new SendVerifikasiEmail($user->nama));
        }
        $success['token'] = $user->createToken('MyAuthApp')->plainTextToken;
        $success['message']='User sudah activasi!, check email untuk mengetahui password aplikasi';
        return $this->sendResponse($success, 'Updated verifikasi');
    }

}
