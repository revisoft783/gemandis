<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    //
    public function cities($id)
    {
        $cities = \App\Services\Impl\ProvinceServiceImpl::getCities($id);
        return response()->json($cities);
    }

    public function district($id)
    {
        $districts = \App\Services\Impl\ProvinceServiceImpl::getDistrict($id);
        return response()->json($districts);
    }

    public function sub_district($id)
    {
        $subDistricts = \App\Services\Impl\ProvinceServiceImpl::getSubDistrict($id);
        return response()->json($subDistricts);
    }
}
