<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', 'auth', 'peran']], function () {

    Route::prefix('kasipem')->group(function () {
        Route::get('/', 'KasiPemerintahanController@KasiPemerintahan')->name('kasipem');
        Route::get('/buku-induk', 'KasiPemerintahanController@buku_induk')->name('kasipem.buku_induk');
        Route::get('/print/buku-induk', 'KasiPemerintahanController@exportBukuInduk')->name('kasipem.print_buku_induk');
        Route::get('/mutasi-penduduk','KasiPemerintahanController@buku_mutasi_penduduk')->name('kasipem.mutasi-penduduk');
        Route::get('/print/buku-mutasi', 'KasiPemerintahanController@exportBukuMutasi')->name('kasipem.print_buku_mutasi');
        Route::get('/buku-rekap-penduduk','KasiPemerintahanController@buku_rekap_penduduk')->name('kasipem.rekap_penduduk');
        Route::get('/print/rekap-penduduk', 'KasiPemerintahanController@exportRekapPenduduk')->name('kasipem.print_rekap_penduduk');
        Route::get('/buku-penduduk-sementara','KasiPemerintahanController@buku_penduduk_sementara')->name('kasipem.penduduk_sementara');
        Route::get('/buku-ktp-kk','KasiPemerintahanController@buku_kk_nik')->name('kasipem.buku_ktp-kk');
    });
    Route::resource('kasipem', 'KasiPemerintahanController')->except('show');

});
