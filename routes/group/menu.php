<?php

use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['web', 'auth', 'peran']], function () {

    Route::prefix('menu')->group(function () {
        Route::get('/', 'MenuController@index')->name('menu.index');
        Route::get('/create', 'MenuController@create')->name('menu.create');
        Route::post('/store', 'MenuController@store')->name('menu-store');
        Route::delete('/delete/{menu}', 'MenuController@destroy')->name('menu-destroy');

        Route::get('/sub-menu/create', 'MenuController@create_submenu')->name('sub-menu.create');
        Route::get('/sub-menu/edit/{id?}', 'MenuController@edit_submenu')->name('sub-menu.edit');
        Route::post('/sub-menu/store', 'MenuController@store_submenu')->name('sub-menu.store');
        Route::post('/sub-menu/update', 'MenuController@update_submenu')->name('sub-menu.update');
        Route::delete('/sub-menu/delete/{sub_menu}', 'MenuController@destroy_submenu')->name('sub-menu.destroy');
    });
    Route::resource('menu', 'MenuController')->except('show');

});

