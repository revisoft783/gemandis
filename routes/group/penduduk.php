<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/penduduk/cari','PendudukController@cari')->name('penduduk.cari');
Route::get('/penduduk/cari-kk','PendudukController@cari_kk')->name('penduduk.cari_kk');
Route::get('/statistik-penduduk', 'GrafikController@index')->name('statistik-penduduk');
Route::get('/statistik-penduduk/show', 'GrafikController@show')->name('statistik-penduduk.show');
Route::get('/change', 'PendudukController@changeStatus')->name('penduduk.change');
Route::get('utility', 'PendudukController@utility')->name('utility');
Route::group(['middleware' => ['web', 'auth', 'peran']], function () {

    Route::prefix('penduduk')->group(function () {
        Route::get('/akseptor-kb/{sex}', 'PendudukController@akseptor_kb')->name('penduduk.akseptor-kb');
        Route::get('/create', 'PendudukController@create')->name('penduduk.create');
        Route::get('/cetak', 'PendudukController@printAll')->name('penduduk.print_all');
        Route::get('/detail/{penduduk}', 'PendudukController@detail')->name('penduduk.detail');
        Route::get('/{nik}', 'PendudukController@show')->name('penduduk.show');
        Route::delete('/hapus', 'PendudukController@destroys')->name('penduduk.destroys');
    });
    Route::resource('penduduk', 'PendudukController')->except('create','show','change');

    Route::prefix('keluarga-penduduk')->group(function () {
        Route::get('/', 'PendudukController@keluarga')->name('penduduk.keluarga');
        Route::get('/{kk}/cetak', 'PendudukController@printKeluarga')->name('penduduk.keluarga.print');
        Route::get('/cetak', 'PendudukController@printAllKeluarga')->name('penduduk.print_all_keluarga');
        Route::get('/{kk}', 'PendudukController@detailKeluarga')->name('penduduk.keluarga.show');
    });

    Route::prefix('calon-pemilih')->group(function () {
        Route::get('/', 'PendudukController@calonPemilih')->name('penduduk.calon_pemilih');
        Route::get('/cetak', 'PendudukController@printCalonPemilih')->name('penduduk.print_calon_pemilih');
    });

    Route::prefix('meninggal-status')->group(function () {
        Route::get('/', 'PendudukController@meninggal')->name('penduduk.meninggal');
        Route::get('/cetak', 'PendudukController@print_all_mutasi_meninggal')->name('penduduk.print_all_mutasi_meninggal');
        Route::post('/update', 'PendudukController@update_mutasi_meninggal')->name('penduduk.update_mutasi_meninggal');
    });

    Route::prefix('mutasi-status')->group(function () {
        Route::get('/', 'PendudukController@mutasiPenduduk')->name('penduduk.mutasi');
        Route::get('/cetak', 'PendudukController@print_all_mutasi')->name('penduduk.print_all_mutasi');
        Route::post('/update', 'PendudukController@update_mutasi')->name('penduduk.update_mutasi');
    });

    Route::prefix('utility')->group(function(){
        Route::get('/edit', 'PendudukController@showUtility')->name('utility.show.ktp');
        Route::post('/update', 'PendudukController@update_ktp_reader')->name('utility.store.ktp');
        Route::post('/sync', 'PendudukController@update_ktp_reader')->name('utility.sync.ktp');
    });
});
