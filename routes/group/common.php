<?php

use App\Models\City;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

Route::get('/kabupaten/{id}','CommonController@cities')->name('kabupaten');
Route::get('/kecamatan/{id}','CommonController@district')->name('kecamatan');
Route::get('/kelurahan/{id}','CommonController@sub_district')->name('kelurahan');

// AJAX ADMINISTRATIVES DATA
Route::get('/information/create/ajax-city',function(Request $request)
{
    $province_id = $request->province_id;
    $regency_id = $request->regency_id;
    $regencies = City::where('prov_id','=',$province_id)->orderBy('name')->get();
    $listRegencies = '';
    foreach ($regencies as $regency) {
        $selected = $regency->id == $regency_id ? ' selected' : '';
        $listRegencies .= '<option value="'.$regency->id.'"'.$selected.'>'.$regency->name.'</option>';
    }
    return $listRegencies;
});
