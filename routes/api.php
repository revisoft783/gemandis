<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('nik', 'Api\v1\ManageUserController@FindNIKPenduduk');
Route::post('register', 'Api\v1\ManageUserController@RegisterPenduduk');
Route::post('login', 'Api\v1\ManageUserController@signin');
Route::post('activasi', 'Api\v1\ManageUserController@Verifikasi');

Route::middleware('auth:sanctum')->group( function () {
    Route::prefix('v1/penduduk')->group(function() {
        Route::get("/",'Api\v1\ManagePendudukController@index');
        Route::get("/mutasi-pindah",'Api\v1\ManagePendudukController@index_pindah');
        Route::get("/mutasi-meninggal",'Api\v1\ManagePendudukController@index_meninggal');
        Route::post("/store",'Api\v1\ManagePendudukController@store');
        Route::post('nik', 'Api\v1\ManageUserController@FindNIKPenduduk');
        // Route::post("/store_ktp",'Api\v1\ManagePendudukController@storeKtp');
    });

    Route::prefix('v1/gallery')->group(function() {
        Route::get("/",'Api\v1\GalleryController@index');
    });

    Route::prefix('v1/dropdown')->group(function() {
        Route::get("/",'Api\HelperController@index');
        Route::get("/provinces",'Api\HelperController@provinceDDL');
        Route::get("/cities",'Api\HelperController@cityDDL');
        Route::get("/district",'Api\HelperController@districtDDL');
    });
});

Route::prefix('v1/penduduk')->group(function() {
    Route::post("/store_ktp",'Api\v1\ManagePendudukController@storeKtp');
});

// Test
Route::get('data', 'Api\HelperController@index');
